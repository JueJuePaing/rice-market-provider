module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins : [
    [
      "module:babel-plugin-module-resolver",
      {
        root : ["./src"],
        "alias" : {
          "apis" : "./src/apis",
          "assets" : "./src/assets",
          "components" : "./src/components",
          "data" : "./src/data",
          "routes" : "./src/routes",
          "screens" : "./src/screens",
          "utils" : "./src/utils",
          "hook" : "./src/hook",
          "context" : "./src/context",
          "navigation" : "./src/navigation"
        }
      }
    ],
    "react-native-reanimated/plugin"
  ]
};

