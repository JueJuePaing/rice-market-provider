import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import KnowledgeScreen from "screens/KnowledgeScreen";
import KnowledgeDetailScreen from "screens/KnowledgeDetail";

const KnowledgeStack = () => {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator screenOptions={{ headerShown : false, animationEnabled : false }}>
      <Stack.Screen name="KnowledgeScreen" component={KnowledgeScreen} />
      <Stack.Screen name="KnowledgeDetail" component={KnowledgeDetailScreen} />
    </Stack.Navigator>
  );
};
export default KnowledgeStack;
