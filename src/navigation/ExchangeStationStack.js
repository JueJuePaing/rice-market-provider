import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import PackageScreen from "screens/PackageScreen";
import CommodityDepotScreen from 'screens/CommodityDepotScreen';
import DepotRiceDetailScreen from 'screens/CommodityDepotScreen/DepotRiceDetailScreen';

const ExchangeStationStack = () => {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator screenOptions={{ headerShown : false, animationEnabled : false }}>
      <Stack.Screen
        name={ "CommodityDepot" }
        component={ CommodityDepotScreen } />
      <Stack.Screen
        name={ "Package" }
        component={ PackageScreen } />
      <Stack.Screen
        name={ "DepotRiceDetail" }
        component={ DepotRiceDetailScreen } />
    </Stack.Navigator>
  );
};
export default ExchangeStationStack;
