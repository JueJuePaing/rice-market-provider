import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import WarehouseScreen from 'screens/WarehouseScreen';
import WarehouseRiceDetailScreen from 'screens/WarehouseScreen/WarehouseRiceDetailScreen';
import WarehouseRiceListScreen from 'screens/WarehouseScreen/WarehouseRiceList';
import PackageScreen from "screens/PackageScreen"
import UserDetailScreen from 'screens/UserDetailScreen';
import ReviewsScreen from 'screens/ReviewsScreen';

const WarehouseStack = () => {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator screenOptions={{ headerShown : false, animationEnabled : false }}>
      <Stack.Screen
        name={ "WarehouseSc" }
        component={ WarehouseScreen } />
      <Stack.Screen
        name={ "RiceList" }
        component={ WarehouseRiceListScreen } />
      <Stack.Screen
        name={ "WarehouseRiceDetail" }
        component={ WarehouseRiceDetailScreen } />
      <Stack.Screen
        name={ "Package" }
        component={ PackageScreen } />
      <Stack.Screen
        name={ "UserDetail" }
        component={ UserDetailScreen } />
      <Stack.Screen
        name={ "Reviews" }
        component={ ReviewsScreen } />
    </Stack.Navigator>
  );
};
export default WarehouseStack;
