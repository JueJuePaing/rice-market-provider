import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import PackageScreen from "screens/PackageScreen";
import ProfileScreen from 'screens/ProfileScreen';
import ChangePasswordScreen from 'screens/ChangePasswordScreen';
import ForgetPasswordScreen from 'screens/ForgetPasswordScreen';
import ProfileDashboardScreen from 'screens/ProfileDashboard';
import ReviewsScreen from 'screens/ReviewsScreen';
import RiceTypeConfigurationScreen from 'screens/RiceTypeConfigurationScreen';
import RicePricesScreen from 'screens/RicePricesScreen';
import AddRicePriceScreen from 'screens/AddRicePriceScreen';
import UpdateProfile from 'screens/UpdateProfile';
import PackageHistory from "screens/PackageHistory";

const ProfileStack = () => {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator screenOptions={{ headerShown : false, animationEnabled : false }}>
      <Stack.Screen
        name={ "Profile" }
        component={ ProfileScreen } />
      <Stack.Screen
        name={ "UpdateProfile" }
        component={ UpdateProfile } />
      <Stack.Screen
        name={ "ChangePassword" }
        component={ ChangePasswordScreen } />
      <Stack.Screen
        name={ "ForgetPassword" }
        component={ ForgetPasswordScreen } />
      <Stack.Screen
        name={ "ProfileDashboard" }
        component={ ProfileDashboardScreen } />
      <Stack.Screen
        name={ "Reviews" }
        component={ ReviewsScreen } />
      <Stack.Screen
        name={ "RiceTypeConfiguration" }
        component={ RiceTypeConfigurationScreen } />
      <Stack.Screen
        name={ "RicePrice" }
        component={ RicePricesScreen } />
      <Stack.Screen
        name={ "AddRicePrice" }
        component={ AddRicePriceScreen } />
      <Stack.Screen
        name={ "Package" }
        component={ PackageScreen } />
      <Stack.Screen
        name={ "PackageHistory" }
        component={ PackageHistory } />
    </Stack.Navigator>
  );
};
export default ProfileStack;
