import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import RiceMillScreen from 'screens/RiceMillScreen';
import RiceListScreen from 'screens/RiceListScreen';
import RiceDetailScreen from 'screens/RiceDetailScreen';
import PackageScreen from "screens/PackageScreen"
import UserDetailScreen from 'screens/UserDetailScreen';
import ReviewsScreen from 'screens/ReviewsScreen';

const RiceMillStack = () => {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator screenOptions={{ headerShown : false, animationEnabled : false }}>
      <Stack.Screen
        name={ "RiceMill" }
        component={ RiceMillScreen } />
      <Stack.Screen
        name={ "RiceList" }
        component={ RiceListScreen } />
      <Stack.Screen
        name={ "RiceDetail" }
        component={ RiceDetailScreen } />
      <Stack.Screen
        name={ "Package" }
        component={ PackageScreen } />
      <Stack.Screen
        name={ "UserDetail" }
        component={ UserDetailScreen } />
      <Stack.Screen
        name={ "Reviews" }
        component={ ReviewsScreen } />
    </Stack.Navigator>
  );
};
export default RiceMillStack;
