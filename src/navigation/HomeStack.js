import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from 'screens/HomeScreen';
import PackageScreen from "screens/PackageScreen";

const HomeStack = () => {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator screenOptions={{ headerShown : false, animationEnabled : false }}>
      <Stack.Screen name="HomeScreen" component={HomeScreen} />
      <Stack.Screen name="Package" component={PackageScreen} />
    </Stack.Navigator>
  );
};
export default HomeStack;
