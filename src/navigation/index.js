import React from "react";
import { 
  StyleSheet,
  Text,
  View
} from 'react-native';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import HomeStack from "navigation/HomeStack";
import WarehouseStack from "navigation/WarehouseStack";
import RiceMillStack from "navigation/RiceMillStack";
import ExchangeStationStack from "navigation/ExchangeStationStack";
import ProfileStack from "navigation/ProfileStack";
import Colors from "assets/colors";
import {useLocal} from '../hook/useLocal';

import RiceWarehouse from 'assets/icons/RiceWarehouse';
import RiceMill from 'assets/icons/RiceMill';
import Depot from 'assets/icons/Depot';
import Profile from 'assets/icons/Profile';
import Home from 'assets/icons/Home';
import { heightPercentageToDP as hp } from "react-native-responsive-screen";

const Tab = createBottomTabNavigator();

const renderTabIcon = (
  route,
  focused,
  color
) => {
  const local = useLocal();
  const buttonStyle = focused
    ? styles.buttonTabBarActive
    : styles.buttonTabBarInActive;
  return (
    <View style={{alignItems: 'center'}}>
      <View style={buttonStyle}>
      {
        route.name === "HomeStack" ? 
          <Home color={focused ? "#fff" : color} /> :
          route.name === "WarehouseStack" ? 
            <RiceWarehouse color={focused ? "#fff" : color} /> :
            route.name === "RiceMillStack" ? 
              <RiceMill color={focused ? "#fff" : color} /> :
              route.name === "ExchangeStationStack" ? 
                <Depot color={focused ? "#fff" : color} /> :
                  <Profile color={focused ? "#fff" : color} />
      }
      </View>
      {
        route.name === "HomeStack" ? 
          <Text style={[styles.menuName, {color: focused ? Colors.light_green : 'gray', fontWeight : focused ? '500' : null}]}>{local.home}</Text> :
          route.name === "WarehouseStack" ? 
            <Text style={[styles.menuName, {color: focused ? Colors.light_green : 'gray', fontWeight : focused ? '500' : null}]}>{local.warehouse}</Text> :
            route.name === "RiceMillStack" ? 
              <Text style={[styles.menuName, {color: focused ? Colors.light_green : 'gray', fontWeight : focused ? '500' : null}]}>{local.riceMill}</Text> :
              route.name === "ExchangeStationStack" ? 
                <Text style={[styles.menuName, {color: focused ? Colors.light_green : 'gray', fontWeight : focused ? '500' : null}]}>{local.exchangeStation}</Text>:
                  <Text style={[styles.menuName, {color: focused ? Colors.light_green : 'gray', fontWeight : focused ? '500' : null}]}>{local.profile}</Text>
      }
    </View>
  );
};

const styles = StyleSheet.create({
  buttonTabBarActive : {
    paddingHorizontal : 10,
    paddingVertical : 6,
    backgroundColor : Colors.light_green,
    borderRadius : 10
  },
  buttonTabBarInActive : {
    paddingHorizontal : 10,
    paddingVertical : 6,
    borderRadius : 10
  },
  expandedButton : {
    flex : 1,
    justifyContent : "center",
    alignItems : "center"
  },
  menuName : {
    fontSize : hp(1.5),
    paddingTop : 2
  }
});
const AppFollow = () => {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon : ({
          focused, color, size 
        }) =>
          renderTabIcon(route, focused, color, size),
        keyboardHidesTabBar : true,
        headerShown : false,
        tabBarShowLabel : false,
        tabBarItemStyle : { flex : 1, width : "100%" },
        tabBarStyle : {height : 65 }
      })}
      >
      <Tab.Screen name="HomeStack" component={HomeStack} />
      <Tab.Screen name="WarehouseStack" component={WarehouseStack} />
      <Tab.Screen name="RiceMillStack" component={RiceMillStack} />
      <Tab.Screen name="ExchangeStationStack" component={ExchangeStationStack} />
      <Tab.Screen name="ProfileStack" component={ProfileStack} />
    </Tab.Navigator>
  );
};
export default AppFollow;
