import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import ChatScreen from "screens/ChatScreen";
import ChatInfoScreen from "screens/ChatScreen/ChatInfo";
import ChatDetailScreen from "screens/ChatScreen/ChatDetail";

const ChatStack = () => {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator screenOptions={{ headerShown : false, animationEnabled : false }}>
      <Stack.Screen name="ChatScreen" component={ChatScreen} />
      <Stack.Screen name="ChatInfoScreen" component={ChatInfoScreen} />
      <Stack.Screen name="ChatDetailScreen" component={ChatDetailScreen} />
    </Stack.Navigator>
  );
};
export default ChatStack;
