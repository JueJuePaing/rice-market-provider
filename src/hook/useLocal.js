import React, {useContext} from 'react';
import { Context } from 'context/Provider';
import en from '../local/en';
import mm from '../local/mm';

export const useLocal = () => {
  const {lang} = useContext(Context);
  if (lang === 'en') {
    return en;
  } else {
    return mm;
  }
};
