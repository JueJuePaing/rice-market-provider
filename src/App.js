import React, { useEffect } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import { SafeAreaProvider } from "react-native-safe-area-context";
import {
  DefaultTheme, Provider as PaperProvider
} from "react-native-paper";
import { LogBox, DeviceEventEmitter } from "react-native";
import Pushy from 'pushy-react-native';

import { Provider } from "context/Provider";
import AppFollow from "navigation/index";

import SplashScreen from "screens/LoginScreen/SplashScreen";
import LoginScreen from "screens/LoginScreen";
import RegisterScreen from "screens/RegisterScreen";
import ForgetPasswordScreen from 'screens/ForgetPasswordScreen';
import OTPScreen from "screens/OTPScreen";
import ChangePasswordScreen from 'screens/ChangePasswordScreen';
import TermsConditions from 'screens/TermsConditions';

import ChatStack from "navigation/ChatStack";
import KnowledgeStack from "navigation/KnowledgeStack";
import NotificationScreen from "screens/NotificationScreen";
import SystemNotificationScreen from "screens/NotificationScreen/SystemNotification";


const Stack = createStackNavigator();

const theme = {
  ...DefaultTheme,
  colors : {
    ...DefaultTheme.colors,
    background : "#fff",
    primary : "#87ad2b",
    accent : "#f2bf43"
  }
};

Pushy.setNotificationListener(async data => {

  console.log("Pushy setNotificationListener >> ", JSON.stringify(data));

  let notificationText = data.message ? data.message : 'Welcome to Rices Market Provider';
  let notificationTitle = data.title ? data.title : 'Rices Market Provider';

  Pushy.notify(notificationTitle, notificationText, data);
  // // Clear iOS badge count
  Pushy.setBadge && Pushy.setBadge(0);

  if (data.type === "message") {
    DeviceEventEmitter.emit('noti.chat_new_message', data);
  }
});

function App() {

  useEffect(() =>{
    LogBox.ignoreLogs(["Warning: ..."]); // Ignore log notification by message
    LogBox.ignoreAllLogs();//Ignore all log notifications
  }, []);

  useEffect(() => {
    if (Platform.OS === 'android') {
      Pushy.toggleFCM(true); // undefined function in IOS
      Pushy.setNotificationIcon('ic_launcher'); // undefined function in IOS
    } else {
      Pushy.toggleInAppBanner(true);
    }

    Pushy.listen();

    // Register the user for push notifications
    Pushy.register()
      .then(async deviceToken => {
        console.log("Pushy register >> ", deviceToken);
      })
      .catch(err => {
        // Handle registration errors
        console.error('Pushy Register Errors : ' + err);
      });

    Pushy.setNotificationClickListener(async data => {

      
      // Emit event for noti
      if (data.type === "message") {
        if (data.friend_id && data.channel_id) {
          const chat = {
            channel_id : data.channel_id,
            friend_name: data.title,
            friend_id : data.friend_id
          }
          setTimeout(() => {
            DeviceEventEmitter.emit('noti.new_message', chat);
          }, 500);
        }
      }

      else if (data.type === "exchange-sation-price" && data.exchange_station_id) {
        setTimeout(() => {
          DeviceEventEmitter.emit('noti.new_exchange_station_price', data.exchange_station_id);
        }, 500);
      } 

      else if (data.type === "system-notification" && data.data) {
        setTimeout(() => {
          DeviceEventEmitter.emit('noti.new_system_notification', data.data);
        }, 500);
      } 

      else if (data.type === "konwledge" && data.data) {
        setTimeout(() => {
          DeviceEventEmitter.emit('noti.new_knowledge', data.data);
        }, 500);
      }
    });

  }, []);

  return (
    <PaperProvider theme={theme}>
      <Provider>
        <SafeAreaProvider>
          <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerShown : false }}>
              <Stack.Screen name="Splash" component={SplashScreen} />
              <Stack.Screen name="Login" component={LoginScreen} />
              <Stack.Screen name="ForgetPassword" component={ForgetPasswordScreen} />
              <Stack.Screen name="TermsAndConditions" component={ TermsConditions } />
              <Stack.Screen name="Register" component={RegisterScreen} />
              <Stack.Screen name="OTP" component={OTPScreen} />
              <Stack.Screen name="ChangePassword" component={ChangePasswordScreen} />
              <Stack.Screen name="App" component={AppFollow} />
              <Stack.Screen name="ChatStack" component={ChatStack} />
              <Stack.Screen name="KnowledgeStack" component={KnowledgeStack} />
              <Stack.Screen name="NotificationScreen" component={NotificationScreen} />
              <Stack.Screen name="SystemNotification" component={SystemNotificationScreen} />
            </Stack.Navigator>
          </NavigationContainer>
        </SafeAreaProvider>
      </Provider>
    </PaperProvider>
  );
}

export default App;
