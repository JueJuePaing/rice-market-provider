import React from "react";
import {
    View, 
    TouchableOpacity
} from "react-native";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import {Rating as styles} from "assets/style";

const SMALL_SIZE = hp(2);
const MEDIUM_SIZE = hp(2.7);
const LARGE_SIZE = hp(4);

const Rating = ({rating, size, rateHandler}) => {
  return <View style={styles.ratingContent}>
     {Array.from({ length: rating }, (_, i) => (
      <TouchableOpacity
        style={[styles.star, {
          marginRight: size === "large" ? wp(3) : wp(1)
        }]}
        activeOpacity={0.8}>
        <FontAwesome
            name="star"
            size={size === "small" ? SMALL_SIZE : size === "medium" ? MEDIUM_SIZE : LARGE_SIZE}
            color="#d6a609" />
      </TouchableOpacity>
    ))}
    {Array.from({ length: 5 - rating }, (_, i) => (
      <TouchableOpacity
        style={[styles.star, {
          marginRight: size === "large" ? wp(3) : wp(1)
        }]}
        activeOpacity={0.8}>
        <FontAwesome
            name="star-o"
            size={size === "small" ? SMALL_SIZE : size === "medium" ? MEDIUM_SIZE : LARGE_SIZE}
            color="#d6a609" />
      </TouchableOpacity>
    ))}
  </View>
};

export default Rating;
