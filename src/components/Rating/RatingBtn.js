import React from "react";
import {
    View, 
    TouchableOpacity
} from "react-native";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import {Rating as styles} from "assets/style";

const LARGE_SIZE = hp(4);

const RatingBtn = ({rating, rateHandler}) => {
  const rate = rating ? rating : 0;
  return <View style={styles.ratingContent}>
     {Array.from({ length: rate }, (_, i) => (
      <TouchableOpacity
        onPress={()=> rateHandler(i+1)}
        style={[styles.star, {
          marginRight: wp(3)
        }]}
        activeOpacity={0.8}>
        <FontAwesome
            name="star"
            size={LARGE_SIZE}
            color="#d6a609" />
      </TouchableOpacity>
    ))}
    {Array.from({ length: 5 - rate }, (_, i) => (
      <TouchableOpacity
        onPress={()=> rateHandler(rate+i+1)}
        style={[styles.star, {
          marginRight: wp(3)
        }]}
        activeOpacity={0.8}>
        <FontAwesome
            name="star-o"
            size={LARGE_SIZE}
            color="#d6a609" />
      </TouchableOpacity>
    ))}
  </View>
};

export default RatingBtn;
