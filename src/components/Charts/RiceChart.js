import React, {
  useState
} from "react";
import { 
  Dimensions, 
  ScrollView,
  View
} from "react-native";
import { LineChart } from "react-native-chart-kit";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import Svg, {Rect, Text} from "react-native-svg";
import Colors from "assets/colors";

const windowWidth = Dimensions.get("window").width;

const RiceChart = ({
  dateLabels,
  low,
  medium,
  high,
  lowData,
  mediumData,
  highData
}) => {

  let [tooltipPos, setTooltipPos] = useState({ x: 0, y: 0, visible: false, value: 0 })


  const targetLength = dateLabels.length;

  const paddedLowData = Array.from({ length: targetLength }, (_, index) => lowData?.[index] || 0);
  const paddedMediumData = Array.from({ length: targetLength }, (_, index) => mediumData?.[index] || 0);
  const paddedHighData = Array.from({ length: targetLength }, (_, index) => highData?.[index] || 0);
  const tempData = Array(targetLength).fill(0);

  const chartData = {
    labels : dateLabels,
    datasets : []
  };

  if (low) {
    chartData.datasets.push({
      data : paddedLowData ?? tempData,
      color : (opacity = 1) => `rgba(219, 198, 114, ${opacity})`, // optional color function
      strokeWidth : 2
    })
  } 
  if (medium) {
    chartData.datasets.push({
      data : paddedMediumData ?? tempData,
      color : (opacity = 1) => `rgba(217, 135, 91, ${opacity})`, // optional color function
      strokeWidth : 2
    })
  }
  if (high) {
    chartData.datasets.push({
      data : paddedHighData ?? tempData,
      color : (opacity = 1) => `rgba(135, 173, 43, ${opacity})`, // optional color function
      strokeWidth : 2 
    })
  }

  if (!low && !medium && !high) {
    chartData.datasets.push({
      data : tempData,
      color : (opacity = 1) => `rgba(135, 173, 43, ${opacity})`, // optional color function
      strokeWidth : 2 
    })
  }

  if (chartData?.datasets?.length > 0) {
    return (
      <View style={{
        alignItems : 'center'
      }}>
        <ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}>
          <LineChart
            data={chartData}
            width={dateLabels.length > 20 ? windowWidth * Math.ceil(dateLabels.length / 20) : windowWidth - 20} // from react-native
            height={hp(35)}
            yAxisLabel="Ks "
            yAxisSuffix=""
            chartConfig={{
              backgroundGradientFrom : "#f2f2f2",
              backgroundGradientTo : "#f2f2f2",
              decimalPlaces : 0, 
              color : (opacity = 1) => `rgba(223, 83, 65, ${opacity})`,
              labelColor : (opacity = 1) => `rgba(104, 138, 25, ${opacity})`,
              propsForVerticalLabels : {
                fontSize : 11,
                translateX : dateLabels.length > 10 ? 0 : -5,
                translateY : -8  
              }
            }}
  
            
  
            decorator={() => {
              return tooltipPos.visible ? <View style={{
                alignItems: 'center', justifyContent: 'center'
              }}><Svg>
                      <Rect 
                        x={tooltipPos.x - 30} 
                        y={tooltipPos.y + 5} 
                        rx="5" 
                        ry="5"
                        width="60" 
                        height="30"
                        fill={Colors.dark_yellow} />
                        <Text
                            x={tooltipPos.x + 0}
                            y={tooltipPos.y + 25}
                            fill="white"
                            fontSize="13"
                            fontWeight="bold"
                            textAnchor="middle">
                            {`${tooltipPos.value} Ks`}
                        </Text>
                  </Svg>
                </View> : null
          }}
          
          onDataPointClick={(data) => {
            let isSamePoint = (tooltipPos.x === data.x 
                                && tooltipPos.y === data.y)
  
            isSamePoint ? setTooltipPos((previousState) => {
                return { 
                          ...previousState,
                          value: data.value,
                          visible: !previousState.visible
                       }
            })
                : 
            setTooltipPos({ x: data.x, value: data.value, y: data.y, visible: true });
  
        }}
            style={{
              paddingBottom : 40 
            }}
            verticalLabelRotation={dateLabels.length > 10 ? 90 : 60}
            bezier/>
        </ScrollView>
      </View>
    );
  } else return null
};

export default RiceChart;
