import React, {useState, useEffect} from "react";
import {
  Modal,
  View, 
  TouchableOpacity,
  TextInput,
  ScrollView,
  Text} from "react-native";
import { Button } from "react-native-paper";
import Ionicons from "react-native-vector-icons/Ionicons";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

import { useLocal } from "../../hook/useLocal";
import Colors from "assets/colors";
import {
  DepotFilterModal as styles
} from "assets/style";

const FilterModal = ({
  defaultState,
  defaultTownship,
  defaultName,
  states,
  allTownships,
  onClose, 
  onSubmit
}) => {

  const local = useLocal();

  const [
    name,
    setName
  ] = useState("");
  const [
    showStates,
    setShowStates
  ] = useState(false);
  const [
    showTownship,
    setShowTownship
  ] = useState(false);
  const [
    state,
    setState
  ] = useState();
  const [
    township,
    setTownship
  ] = useState();
  const [
    townships,
    setTownships
  ] = useState();

  useEffect(() => {
    setName(defaultName);
    setState(defaultState);
    setTownship(defaultTownship);
    if (defaultState) {
      const filterTownships = allTownships?.filter(ts => ts.state_id == defaultState.id+"");
      setTownships(filterTownships)
    }
  }, [defaultName, defaultState, defaultTownship, allTownships]);

  const handleSubmit = () => {
    setShowStates(false)
    setShowTownship(false)
    onSubmit(name, state, township);
  }

  const stateHandler = (st) => {
    setShowStates(false);
    setState(st)

    setTownship();
    if (st) {
      const filterTownships = allTownships?.filter(township => township.state_id == st.id+"");
      setTownships(filterTownships)
    } else {
      setTownships();
    }
  }

  const townshipHandler = (ts) => {
    setShowTownship(false);
    setTownship(ts)
  }

  const onNameChange = (txt) => {
    setName(txt);
  };

  return (
    <Modal 
      visible={true} 
      onRequestClose={onClose}
      transparent={true}>  
      <View style={styles.modalContainer}>
        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.closeBtn}
          onPress={onClose}>
          <Ionicons
            name="ios-close-circle-outline"
            size={hp(4)}
            color="darkgray" />
        </TouchableOpacity>
        <View style={styles.modal}>

          <TextInput
            placeholder={local.name}
            value={name}
            onChangeText={onNameChange}
            keyboardType="default"
            autoCapitalize="none"
            style={styles.input}
            underlineColor="transparent" />

          <View>
            <TouchableOpacity 
              activeOpacity={0.8}
              onPress={()=> {
                setShowStates(!showStates)
                setShowTownship(false)
              }}
              style={styles.selectWrapper}>
                <Text style={styles.selectLabel}>
                  {state ? state.name : local.chooseStates}
                </Text>
              <Ionicons
                name={showStates ? "caret-up-circle" : "caret-down-circle"}
                size={hp(2.8)}
                color={Colors.light_green} />
            </TouchableOpacity>

            {showStates && <TouchableOpacity 
              activeOpacity={1}
              style={[styles.filterList, styles.stateFilterList]}>
              {states && states.length > 0 ? <>
                <TouchableOpacity
                  onPress={ () => stateHandler(null) }>
                  <Text style={[styles.name, {paddingBottom : hp(2)}]}>{local.chooseStates}</Text>
                </TouchableOpacity>
                {states?.map((state, index) => {
                  return <TouchableOpacity
                    onPress={ () => stateHandler(state) }>
                    <Text style={[styles.name, {paddingBottom : index === states.length -1 ? 0 : hp(2)}]}>{state.name}</Text>
                  </TouchableOpacity>
                })} 
              </> : null}
            </TouchableOpacity>}
          </View>

          <View>
            <TouchableOpacity 
              activeOpacity={0.8}
              disabled={ !state }
              onPress={()=> {
                setShowTownship(!showTownship)
                setShowStates(false);
              }}
              style={styles.selectWrapper}>
                <Text style={[styles.selectLabel, {color: !state ? "gray" : "#2b2b29"}]}>
                  {township ? township.name : local.chooseTownship}
                </Text>
              <Ionicons
                name={showTownship ? "caret-up-circle" : "caret-down-circle"}
                size={hp(2.8)}
                color={!state ? "rgba(134, 196, 64, 0.5)" : Colors.light_green} />
            </TouchableOpacity>

            {showTownship && 
            <ScrollView  
              showsVerticalScrollIndicator={false}
              style={styles.tsFilterList}>
                {townships && townships.length > 0 ? <>
                  <TouchableOpacity
                    onPress={ () => townshipHandler(null) }>
                    <Text style={[styles.name, {paddingBottom : hp(2)}]}>{local.chooseTownship}</Text>
                  </TouchableOpacity>
 
                    {townships?.map((ts, index) => {
                      return <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={ () => townshipHandler(ts) }>
                        <Text style={[styles.name, {paddingBottom : index === townships.length -1 ? hp(3) : hp(2)}]}>{ts.name}</Text>
                      </TouchableOpacity>
                    })}
  
                </> : null}
              </ScrollView>}
         
    
          </View>

         

          <View style={styles.footerRow}>
            <Button
              mode="contained"
              onPress={handleSubmit}
              style={styles.button}
              labelStyle={{fontSize: hp(1.8)}}>
              {local.submit}
            </Button>
            {/* <Button
              mode=""
              onPress={handleSubmit}
              style={[styles.button, {width: wp(35)}]}
              labelStyle={{fontSize: hp(1.8), color: Colors.dark_orange}}
              disabled={error}>
              RESET
            </Button> */}
          </View>
        </View>
      </View>
    </Modal>
     
  );
};

export default FilterModal;
