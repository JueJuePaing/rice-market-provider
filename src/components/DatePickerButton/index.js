import React from "react";
import {
  View, 
  TouchableOpacity,
  Text
} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

import { useLocal } from "../../hook/useLocal";
import {
  DatePickerButton as styles
} from "assets/style";

const DatePickerButton = ({ startDate, endDate, width, pressHandler }) => {
  const local = useLocal();

  return (
    <TouchableOpacity 
        activeOpacity={0.8}
        onPress={pressHandler}
        style={[styles.dateRangeWrapper, {width : width ? width : wp(80) }]}>
        <Ionicons
          name="calendar"
          size={hp(2.6)}
          color={"#fff"} />
        <View>
          <Text style={styles.selectTxt}>
            {local.chooseDate}
          </Text>
          {!startDate && !endDate ? <Text style={[styles.dateRangeTxt, {textAlign : 'center'}]}>
            {local.all}
          </Text> : <Text style={styles.dateRangeTxt}>
            {startDate} ~ {endDate}
          </Text>}
        </View>
        <Ionicons
          name="chevron-down"
          size={hp(2.3)}
          color={"#fff"} />
      </TouchableOpacity>
    );
};

export default DatePickerButton;
