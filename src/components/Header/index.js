import React from 'react';
import {
    View,
    Image,
    Text,
    TouchableOpacity
} from 'react-native';
import Ionicons from "react-native-vector-icons/Ionicons";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import Feather from "react-native-vector-icons/Feather";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

import {Header as styles} from "assets/style";

const Header = ({depot, chatHandler, notiHandler, newsHandler}) => {
    return <View style={styles.headerContainer}>
        {depot ? <TouchableOpacity
            activeOpacity={0.8}
            style={styles.selectBtn}
            >
            <Text style={styles.selectTxt}>
              {depot}
            </Text>
            {/* <Ionicons
              name="caret-down-circle"
              size={hp(2.5)}
              color={'#abc276'} /> */}
        </TouchableOpacity> : 
        <Image
            source={require("assets/images/app_logo_nobg.png")}
            style={styles.logo} />}
        <View style={styles.rightContent}>
            <TouchableOpacity
                style={styles.headerBtn}
                activeOpacity={0.8}
                onPress={chatHandler}>
                {/* <View style={styles.chatBadge}>
                    <Text style={styles.chatCount}>3</Text>
                </View> */}
                <Ionicons
                    name="md-chatbubbles-sharp"
                    size={hp(3)}
                    color={"#abc276"} />
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.headerBtn}
                activeOpacity={0.8}
                onPress={newsHandler}>
                <FontAwesome5
                    name="book-reader"
                    size={hp(3)}
                    color={"#abc276"} />
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.headerBtn}
                activeOpacity={0.8}
                onPress={notiHandler}>
                <View style={styles.notiBadge} />
                <Feather
                    name="bell"
                    size={hp(3)}
                    color={"#abc276"} />
            </TouchableOpacity>
        </View>
    </View>
}

export default Header;