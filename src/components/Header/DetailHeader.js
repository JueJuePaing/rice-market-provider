import React from 'react';
import {
    View,
    Image,
    Text,
    TouchableOpacity
} from 'react-native';
import Ionicons from "react-native-vector-icons/Ionicons";
import Entypo from "react-native-vector-icons/Entypo";
import Feather from "react-native-vector-icons/Feather";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Octicons from "react-native-vector-icons/Octicons";
import TextTicker from 'react-native-text-ticker';
import { useLocal } from '../../hook/useLocal';

import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

import {DetailHeader as styles} from "assets/style";
import Colors from 'assets/colors';

const DetailHeader = ({
    color,
    title, 
    chat,
    review, 
    info,
    editable,
    noMarquee,
    width,
    add,
    filter,
    showFilter,
    chart,
    filterHandler,
    reviewHandler, 
    backHandler, 
    chatHandler,
    infoHandler,
    editHandler,
    addHandler,
    userDetailHandler
}) => {
    const local = useLocal();

    return <View style={[styles.headerContainer, {
        backgroundColor: color ? color : '#f2f2f2'
    }]}>
        <TouchableOpacity
            style={styles.backBtn}
            activeOpacity={0.8}
            onPress={backHandler}>
            <Ionicons
                name="chevron-back-outline"
                size={hp(2.3)}
                color={"#abc276"} />
        </TouchableOpacity>

        {noMarquee ? 
        <Text style={styles.title}>{title}</Text>
        : <TextTicker
        style={[
          styles.title,
          {
            paddingHorizontal : 5,
            width : width ? width : wp(55),
          }
        ]}
        duration={2000}
        loop
        repeatSpacer={30}
        marqueeDelay={1000}>
            {title}
      </TextTicker>
}
        <View style={styles.rightBtnRow}>
            {userDetailHandler && <TouchableOpacity
                style={styles.userBtn}
                onPress={userDetailHandler}>
                <Octicons
                    name="person-fill"
                    size={hp(2.7)}
                    color={"#ebb526"} />
            </TouchableOpacity>}
            {chat ? <TouchableOpacity
                style={styles.headerBtn}
                activeOpacity={0.8}
                onPress={chatHandler}>
                <Ionicons
                    name="md-chatbubbles-sharp"
                    size={hp(2.7)}
                    color={"#abc276"} />
            </TouchableOpacity> :
            info ?
            <TouchableOpacity
                style={styles.infoBtn}
                onPress={infoHandler}>
                <Entypo
                    name="info-with-circle"
                    size={hp(2.7)}
                    color={Colors.light_green} />
            </TouchableOpacity> :
            editable ? 
            <TouchableOpacity
                style={styles.infoBtn}
                onPress={editHandler}>
                <Feather
                    name="edit"
                    size={hp(2.7)}
                    color={Colors.light_green} />
            </TouchableOpacity> :
            add ? 
                <TouchableOpacity
                    style={styles.infoBtn}
                    onPress={addHandler}>
                    <MaterialIcons
                        name="add-box"
                        size={hp(3.5)}
                        color={Colors.light_green} />
                </TouchableOpacity> :
            filter ? 
                <TouchableOpacity
                    style={styles.infoBtn}
                    onPress={filterHandler}>
                    <Ionicons
                        name="filter"
                        size={hp(2.7)}
                        color={Colors.light_green}
                        style={{
                            transform: [
                                {
                                    rotate: showFilter ? '180deg' : '0deg'
                                }
                            ]
                        }} />
                </TouchableOpacity> : null }
            
            {review && <TouchableOpacity
                style={styles.reviewBtn}
                onPress={reviewHandler}>
                <Image 
                    source={require("assets/images/review1.jpg")}
                    style={styles.reviewImg} />
            </TouchableOpacity>}
        </View>
    </View>
}

export default DetailHeader;