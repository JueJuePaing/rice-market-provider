import React from "react";
import {
  View, Modal, ScrollView, TouchableOpacity, Text
} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";

import { DepotModal as styles } from "assets/style";
import Colors from "assets/colors";

const SelectModal = ({
  data,
  onClose,
  onChoose,
  selectedId,
  title
}) => {

  return (
    <Modal visible={true} transparent={true} onRequestClose={onClose}>
      <View style={styles.container}>
          <TouchableOpacity onPress={onClose} style={styles.filterCloseIcon}>
            <Icon name="close-circle-outline" size={30} color="#fff" />
          </TouchableOpacity>
          <View style={styles.modalBox}>
            <Text style={[styles.name, styles.sortbyName]}>{title}</Text>
            <ScrollView
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}>
              {data && <>
              {data.map((item, index) => {
                return (
                  <TouchableOpacity
                    style={styles.item}
                    key={index}
                    onPress={() => onChoose(item)}>
                    <Text style={styles.sortName}>
                      {item.name ? item.name : item}
                    </Text>
                    <Ionicons
                      name={
                        ((item.id && selectedId === item.id) || selectedId === item)
                          ? "radio-button-on"
                          : "radio-button-off"
                      }
                      size={hp(2.5)}
                      color={Colors.light_green}/>
                  </TouchableOpacity>
                );
              })}
              </> 
             
              }
            </ScrollView>
          </View>
        </View>
    </Modal>
  );
};

export default SelectModal;
