import React from 'react';
import {View, Image, TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';
import Carousel from 'react-native-reanimated-carousel';

// Components
import {IMAGES} from 'data/mockData';
import {
  Banner as styles
} from "assets/style";

// const SLIDER_1_FIRST_ITEM = 0;

export default function Banner({data, home, bannerHandler}) {

  // const [activeSlide, setActiveSlide] = React.useState(SLIDER_1_FIRST_ITEM);

  const _renderItem = ({item}) => {
    return (
    <TouchableOpacity
        style={styles.renderImage}
        activeOpacity={0.9}
        onPress={() => bannerHandler(item)} >
          
          {data ? <Image source={{
            uri : item?.image
          }} style={styles.renderImage} /> : <Image 
          source={item.source} 
          style={styles.renderImage} />}
      </TouchableOpacity>
    );
  };

  return (
    <>
      <View style={[styles.container, {
        height : home ? hp(15) : hp(14)
      }]}>
          <Carousel
            loop
            width={wp(100)}
            height={home ? hp(15) : hp(14)}
            autoPlay={true}
            data={data ? data : IMAGES}
            style={{marginBottom : 4}}
            scrollAnimationDuration={1000}
            // onSnapToItem={(index) => setActiveSlide(index)}
            renderItem={_renderItem} />

          {/* <View style={styles.paginationContainer}>
            <AnimatedDotsCarousel
              length={data ? data.length : IMAGES.length}
              currentIndex={activeSlide}
              maxIndicators={data ? data.length : IMAGES.length}
              interpolateOpacityAndColor={false}
              activeIndicatorConfig={{
                color: "#93cc54",
                margin: 3,
                opacity: 1,
                size: 7,
              }}
              inactiveIndicatorConfig={{
                color: '#b7d498',
                margin: 3,
                opacity: 0.5,
                size: 7,
              }}
              decreasingDots={[
                {
                  config: { color: '#b7d498', margin: 3, opacity: 0.5, size: 6 },
                  quantity: 1,
                },
                {
                  config: { color: '#b7d498', margin: 3, opacity: 0.5, size: 4 },
                  quantity: 1,
                },
              ]}
              />
          </View> */}
      </View>
    </>
  );
}
