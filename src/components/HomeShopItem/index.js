import React from "react";
import {
    View, 
    Text, 
    TouchableOpacity,
    Image
} from "react-native";
import FontAwesome from "react-native-vector-icons/FontAwesome";

import {HomeShopItem as styles} from "assets/style";
import RiceWarehouse from 'assets/icons/RiceWarehouse';
import HomeRiceMill from "assets/icons/HomeRiceMill";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

const HomeShopItem = ({mill, item, itemHandler}) => {

    const renderStarIcons = () => {
        if (!item?.rating) return;

        const stars = [];
        for (let i = 0; i < item.rating; i++) {
          stars.push(
            <FontAwesome
              key={i}
              name="star"
              size={hp(1.8)}
              color="#d6a609"
              style={{ marginRight: wp(1) }}
            />
          );
        }
        return stars;
      };

  return <TouchableOpacity
    activeOpacity={0.8}
    style={styles.item}
    onPress={itemHandler}>
        <View style={styles.iconWrapper}>
            {item.image ? <View style={styles.imgWrapper}>
              <Image
                style={styles.iconImg}
                source={{ uri : item.image }} /></View> : 
            mill ? <HomeRiceMill size={37} color='#fff' /> : 
                <RiceWarehouse size={34} color='#fff' />}
        </View>
        <View style={styles.nameContent}>
            <Text style={styles.name}>
                {item.name}
            </Text>
            {item.rating && <View style={styles.starRow}>
                {renderStarIcons()}
             
            </View>}
        </View>
  </TouchableOpacity>
};

export default HomeShopItem;
