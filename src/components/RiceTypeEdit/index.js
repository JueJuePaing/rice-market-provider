import React from "react";
import {
    TouchableOpacity, 
    Text
} from "react-native";
import Entypo from "react-native-vector-icons/Entypo";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import {RiceTypeEdit as styles} from "assets/style";

const RiceTypeEdit = ({item, checkHandler}) => {
  return <TouchableOpacity
        activeOpacity={0.8}
        style={[styles.typeItem, item.selected && styles.selectedItem]}
        onPress={() => checkHandler(item)}>
       <Text style={[styles.riceName, item.selected && styles.selectedRiceName]}>{item.name}</Text>
       {item.selected && <Entypo 
            name="check"
            size={hp(2.2)}
            color="#fff" />
        }
    </TouchableOpacity>
};

export default RiceTypeEdit;
