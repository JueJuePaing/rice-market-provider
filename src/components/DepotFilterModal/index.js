import React, {useState, useEffect} from "react";
import {
  Modal,
  View, 
  TouchableOpacity,
  Text,
  ScrollView
} from "react-native";
import { Button } from "react-native-paper";
import Ionicons from "react-native-vector-icons/Ionicons";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

import Colors from "assets/colors";
import {
  DepotFilterModal as styles
} from "assets/style";
import { useLocal } from "../../hook/useLocal";

const DepotFilterModal = ({
  defaultState,
  defaultTownship,
  defaultDepot,
  stationList, 
  allTownships, 
  states, 
  onClose, 
  onSubmit
}) => {
  const local = useLocal();

  const [
    showStates,
    setShowStates
  ] = useState(false);
  const [
    showTownship,
    setShowTownship
  ] = useState(false);
  const [
    showStations,
    setShowStations
  ] = useState(false);
  const [
    state,
    setState
  ] = useState();
  const [
    township,
    setTownship
  ] = useState();
  const [
    townships,
    setTownships
  ] = useState();
  const [
    station,
    setStation
  ] = useState();
  const [
    stations,
    setStations
  ] = useState();
  const [
    stateError,
    setStateError
  ] = useState(false);
  const [
    tsError,
    setTsError
  ] = useState(false);
  const [
    stationError,
    setStationError
  ] = useState(false)

  useEffect(() => {

    let initialState = defaultState;

    if (defaultState) {
      setState(defaultState);
    } else {
      const filteredData = states.filter(item => item.id === defaultDepot.state_id || item.id === defaultDepot.state_id+'');
      if (filteredData && filteredData.length > 0) {
        setState(filteredData[0]);
        initialState = filteredData[0];
      }
    }
    
    if (defaultTownship) {
      setTownship(defaultTownship);
    } else {
      const filteredData = allTownships.filter(item => item.id === defaultDepot.township_id || item.id === defaultDepot.township_id+'');
      if (filteredData && filteredData.length > 0) {
        setTownship(filteredData[0]);
      }
    }

    if (defaultDepot) {
      setStation(defaultDepot) 
    }

    if (initialState && allTownships) {
      
      const filterTownships = allTownships?.filter(township => township.state_id === initialState.id+"" || township.state_id === initialState.id);
      setTownships(filterTownships)
    }
  }, [defaultDepot, defaultState, defaultTownship, allTownships, states]);

  useEffect(() => {
    if (state && township) {
      const filteredData = stationList.filter(item => item.state_id === state.id && item.township_id === township.id);
      if (filteredData && filteredData.length > 0) {
        setStations(filteredData);
      }
    }
  }, [state, township, stationList]);

  const handleSubmit = () => {
    setShowStates(false)
    setShowTownship(false)
    setShowStations(false)
    let valid = true;
    if (!state) {
      setStateError(true);
      valid = false;
    }
    if (!township) {
      setTsError(true);
      valid = false;
    }
    if (!station) {
      setStationError(true);
      valid = false;
    }
    if (!valid) return;
    onSubmit(station)
  }

  const stateHandler = (st) => {
    setShowStates(false);
    setState(st)
    setStateError(false)

    setTownship();
    const filterTownships = allTownships?.filter(township => township.state_id == st.id+"");
    setTownships(filterTownships)

    setStations();
    setStation();
  }

  const townshipHandler = (ts) => {
    setShowTownship(false);
    if (township.id !== ts.id) {
      setTownship(ts)
      setTsError(false)
      setStation();
      setStations();
    }
  }

  const stationHandler = (sta) => {
    setStation(sta);
    setShowStations(false)
    setStationError(false)
  }

  return (
    <Modal 
      visible={true} 
      onRequestClose={onClose}
      transparent={true}>  
      <View style={styles.modalContainer}>
        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.closeBtn}
          onPress={onClose}>
          <Ionicons
            name="ios-close-circle-outline"
            size={hp(4)}
            color="darkgray" />
        </TouchableOpacity>
        <View style={styles.modal}>

          <View>
            <TouchableOpacity 
              activeOpacity={0.8}
              onPress={()=> {
                setShowStates(!showStates)
                setShowTownship(false)
                setShowStations(false)
              }}
              style={styles.selectWrapper}>
                <Text style={styles.selectLabel}>
                  {state ? state.name : local.chooseStates}
                </Text>
              <Ionicons
                name={showStates ? "caret-up-circle" : "caret-down-circle"}
                size={hp(2.8)}
                color={Colors.light_green} />
            </TouchableOpacity>

            {!!stateError && <Text style={styles.errTxt}>{local.invalidStates}</Text>}

            {showStates && <TouchableOpacity 
              activeOpacity={1}
              style={[styles.filterList, styles.stateFilterList]}>
              {states && states.length > 0 ? <>
                {states?.map((state, index) => {
                  return <TouchableOpacity
                    onPress={ () => stateHandler(state) }>
                    <Text style={[styles.name, {paddingBottom : index === states.length -1 ? 0 : hp(2)}]}>{state.name}</Text>
                  </TouchableOpacity>
                })} 
              </> : null}
            </TouchableOpacity>}
          </View>

          <View>
            <TouchableOpacity 
              activeOpacity={0.8}
              disabled={ !state }
              onPress={()=> {
                setShowTownship(!showTownship)
                setShowStates(false);
                setShowStations(false)
              }}
              style={styles.selectWrapper}>
                <Text style={[styles.selectLabel, {color: !state ? "gray" : "#2b2b29"}]}>
                  {township ? township.name : local.chooseTownship}
                </Text>
              <Ionicons
                name={showTownship ? "caret-up-circle" : "caret-down-circle"}
                size={hp(2.8)}
                color={!state ? "rgba(134, 196, 64, 0.5)" : Colors.light_green} />
            </TouchableOpacity>

            {!!tsError && <Text style={styles.errTxt}>{local.invalidTownship}</Text>}

            {showTownship && <ScrollView  
              showsVerticalScrollIndicator={false}
              style={styles.tsFilterList}>
                {townships && townships.length > 0 ? <>
                  {townships?.map((ts, index) => {
                    return <TouchableOpacity
                      onPress={ () => townshipHandler(ts) }>
                      <Text style={[styles.name, {paddingBottom : index === townships.length -1 ? hp(3) : hp(2)}]}>{ts.name}</Text>
                    </TouchableOpacity>
                  })} 
                </> : null}
            </ScrollView>}
          </View>

          <View>
            <TouchableOpacity 
              activeOpacity={0.8}
              disabled={ !state || !township }
              onPress={()=> {
                setShowStations(!showStations)
                setShowStates(false)
                setShowTownship(false)
              }}
              style={styles.selectWrapper}>
                <Text style={[styles.selectLabel, {color: (!state || !township) ? "gray" : "#2b2b29"}]}>
                  {station ? station.name : local.chooseStation}
                </Text>
              <Ionicons
                name={showStations ? "caret-up-circle" : "caret-down-circle"}
                size={hp(2.8)}
                color={(!state || !township) ? "rgba(134, 196, 64, 0.5)" : Colors.light_green}/>
            </TouchableOpacity>

            {!!stationError && <Text style={styles.errTxt}>{local.invalidStation}</Text>}

            {showStations && <TouchableOpacity 
              activeOpacity={1}
              style={[styles.filterList, styles.commodityFilterList]}>
                {stations && stations.length > 0 ? <>
                  {stations?.map((sta, index) => {
                    return <TouchableOpacity
                      onPress={ () => stationHandler(sta) }>
                      <Text style={[styles.name, {paddingBottom : index === stations.length -1 ? 0 : hp(2)}]}>{sta.name}</Text>
                    </TouchableOpacity>
                  })} 
                </> : <Text style={styles.noData}>
                  {local.noData}</Text>}
            </TouchableOpacity>}
          </View>

          <View style={styles.footerRow}>
            <Button
              mode="contained"
              onPress={handleSubmit}
              style={styles.button}
              labelStyle={{fontSize: hp(1.8)}}>
              {local.submit}
            </Button>
          </View>
        </View>
      </View>
    </Modal>
     
  );
};

export default DepotFilterModal;
