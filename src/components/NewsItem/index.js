import React, {useState, useEffect} from "react";
import {
    Text, 
    Image,
    View,
    TouchableOpacity
} from "react-native";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {
    heightPercentageToDP as hp, 
    widthPercentageToDP as wp
  } from "react-native-responsive-screen";
import { createThumbnail } from "react-native-create-thumbnail";

import Colors from "assets/colors";
import styles from "./Style";

const NewsItem = ({item, categoryName, itemHandler}) => {

    const [
        thumbnail,
        setThumbnail
    ] = useState();

    useEffect(() => {
        createThumbnail({
          url: item.image,
          timeStamp: 1000,
        })
          .then(response => {
            if (response.path) {
              setThumbnail(response.path)
            }
          })
          .catch(err => console.log({ err }));
      }, [item]);

    return <TouchableOpacity 
        activeOpacity={0.8}
        onPress={() => itemHandler(item)}
        style={styles.item}>
        {item.type === "image" ? <Image
            source={{
                uri : item.image
            }}
            style={styles.newsImg} /> : item.type === "video" ? <View style={styles.videoBg}>
                {thumbnail && <Image
                    source={{
                        uri : thumbnail
                    }}
                    style={styles.newsImg} />}
                <FontAwesome5
                    name={"play"}
                    size={hp(5.5)}
                    color={Colors.light_green}
                    style={{position: 'absolute'}} />
                    </View> : <View style={styles.videoBg} />}
        <Text style={styles.title}>
            {item.title}
        </Text>

        {/* <WebView
            originWhitelist={['*']}
            source={{ html: item.description }}
            style={styles.webView} /> */}

    </TouchableOpacity>
};

export default NewsItem;
