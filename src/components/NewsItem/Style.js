import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

const styles = StyleSheet.create({
    item: {
        width: wp(94),
        alignSelf: 'center',
        alignItems: 'center',
        marginBottom: hp(3),
    },
    videoBg: {
        width: wp(94),
        height: hp(22),
        borderRadius: hp(1),
        backgroundColor: '#a8a5a5',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius : hp(1)
    },
    newsImg: {
        width: wp(94),
        height: hp(22),
        borderRadius: hp(1),
        resizeMode: 'cover'
    },
    title: {
        fontWeight: 'bold',
        fontSize: hp(1.8),
        color: '#000',
        paddingBottom: hp(.7),
        paddingTop: hp(1),
        alignSelf: 'flex-start'
    },
    webView: {
        width: wp(94),
        height: hp(5),
    }
});

export default styles;