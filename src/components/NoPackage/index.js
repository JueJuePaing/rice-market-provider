import React from "react";
import {
    Text, 
    TouchableOpacity
} from "react-native";
import { Button } from "react-native-paper";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import Ionicons from 'react-native-vector-icons/Ionicons';
import {NoPackage as styles} from "assets/style";

const NoPackage = ({home, text, pkgError, top, buyHandler, reloadHandler}) => {
  return <><Text style={[styles.noData, {
    marginTop: top ? top : home ? hp(10) : hp(35)
  }]}>
        {text}
        </Text>
        {pkgError && <Button
            mode="contained"
            onPress={buyHandler}
            style={styles.button}
            labelStyle={{fontSize: hp(1.8)}}>
            BUY
        </Button>}
        <TouchableOpacity style={{
            alignSelf: 'center',
            marginTop: hp(2)
            }}
            onPress={reloadHandler}>
            <Ionicons
                name='reload'
                size={hp(3.5)}
                color="#5e5e5c"
                />
        </TouchableOpacity>
    </>
};

export default NoPackage;
