import React from "react";
import {
    View, 
    Text
} from "react-native";

import {RiceType as styles} from "assets/style";

const RiceType = ({item}) => {
  return <View style={styles.typeItem}>
       <Text style={styles.riceName}>{item.name}</Text>
    </View>
};

export default RiceType;
