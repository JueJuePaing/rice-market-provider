import React from "react";
import {View, Modal, Text, TouchableOpacity} from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";

import {LanguageModal as styles} from "assets/style";
import MMFlag from "assets/icons/MMFlag";
import EngFlag from "assets/icons/EngFlag";
import AntDesign from "react-native-vector-icons/AntDesign";

const ProfileLanguage = ({visible, lan, onClose, languageHandler}) => {
  if (visible) {
    return (
      <Modal 
        visible={true} 
        onRequestClose={onClose}
        transparent={true}>
        <TouchableOpacity style={styles.profileLanContainer} activeOpacity={1} onPress={onClose}>
            <TouchableOpacity
              style={[styles.profileLanItem, styles.enFlag, lan === 'en' ? styles.selected : null]}
              activeOpacity={0.8}
              onPress={() => languageHandler('en')}>
              <EngFlag />
              <Text style={styles.label}>English</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.profileLanItem, styles.mmFlag, lan === 'mm' ? styles.selected : null]}
              activeOpacity={0.8}
              onPress={() => languageHandler('mm')}>
              <MMFlag />
              <Text style={styles.label}>Myanmar</Text>
            </TouchableOpacity>
        </TouchableOpacity>
      </Modal>
    );
  } else {
    return null;
  }
};

export default ProfileLanguage;
