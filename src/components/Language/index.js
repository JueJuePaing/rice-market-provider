import React from "react";
import {View, Modal, Text, TouchableOpacity} from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";

import {LanguageModal as styles} from "assets/style";
import MMFlag from "assets/icons/MMFlag";
import EngFlag from "assets/icons/EngFlag";
import AntDesign from "react-native-vector-icons/AntDesign";

const Language = ({visible, lan, onClose, languageHandler}) => {
  if (visible) {
    return (
      <Modal 
        visible={true} 
        onRequestClose={onClose}
        transparent={true}>
        <TouchableOpacity
          activeOpacity={1}
          onPressOut={onClose}>
          <AntDesign
            size={hp(2)}
            color={lan === 'en' ? '#f7e7be' : '#f2f0ed'}
            name="caretup"
            style={styles.upward} />
          <View style={styles.container}>
            <TouchableOpacity
              style={[styles.lanItem, styles.enFlag, lan === 'en' ? styles.selected : null]}
              activeOpacity={0.8}
              onPress={() => languageHandler('en')}>
              <EngFlag />
              <Text style={styles.label}>English</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.lanItem, styles.mmFlag, lan === 'mm' ? styles.selected : null]}
              activeOpacity={0.8}
              onPress={() => languageHandler('mm')}>
              <MMFlag />
              <Text style={styles.label}>Myanmar</Text>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </Modal>
    );
  } else {
    return null;
  }
};

export default Language;
