import React from "react";
import {
    View, 
    Text, 
    TouchableOpacity,
    Image
} from "react-native";
import moment from 'moment-timezone';

import {RiceItem as styles} from "assets/style";
import Colors from "assets/colors";
import PriceRise from "../PriceRise";
import PriceLow from "../PriceLow";
import { useLocal } from "../../hook/useLocal"

const RiceItem = ({item, itemHandler}) => {
    const local = useLocal();

    const formattedDate = moment(item.date, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD hh:mm A');

  return <TouchableOpacity
    activeOpacity={0.8}
    style={styles.item}
    onPress={itemHandler}>
        {item.date && <Text style={styles.date}>
            {formattedDate}
        </Text>}
        <View style={styles.titleRow}>
            <Image
                source={require("assets/images/logo1.jpg")}
                style={styles.ricebag} />
        <View style={{
            
        }}>
            <Text style={styles.name}>
                    {item.name}
            </Text>
            <Text style={styles.remark}>
                    {item.unit ?? local.noUnit}
            </Text>
        </View>
        </View>
       <View style={styles.divider} />

        <View style={styles.priceContent}>
            <View style={styles.priceBlock3}>
                
                <View style={{
                    alignItems: 'flex-start'
                }}>
                    <View style={styles.circleRow}>
                        {/* <View style={styles.circle} /> */}
                        <Text style={styles.label3}>{local.low}</Text>
                    </View>
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}>
                   
                        {
                        item.low_status === "up" ? <PriceRise /> :
                            item.low_status === "down" ? <PriceLow /> : 
                            <TouchableOpacity 
                                activeOpacity={1} 
                                style={[styles.normalIcon, {backgroundColor: Colors.low_color}]} />}
                        
                        <Text style={[styles.value3, {color:Colors.low_color}]}>
                            {`${item.price_low} Ks`}
                        </Text>
                    </View>
                </View>
            </View>
            <View style={styles.priceBlock3}>
                <View style={{
                    alignItems: 'flex-start'
                }}>
                    <View style={styles.circleRow}>
                        {/* <View style={[styles.circle, {backgroundColor: Colors.medium_color}]} /> */}
                        <Text style={styles.label3}>{local.medium}</Text>
                    </View>
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}>
                       {
                        item.medium_status === "up" ? <PriceRise /> :
                            item.medium_status === "down" ? <PriceLow /> : 
                            <TouchableOpacity 
                                activeOpacity={1} 
                                style={[styles.normalIcon, {backgroundColor: Colors.medium_color}]} />}
                        <Text style={[styles.value3, {color: Colors.medium_color}]}>
                            {`${item.price_medium} Ks`}
                        </Text>
               
                    </View>
                </View>
            </View>
            <View style={styles.priceBlock3}>
                <View style={{
                    alignItems: 'flex-start'
                }}>
                    <View style={styles.circleRow}>
                        {/* <View style={[styles.circle, {backgroundColor: Colors.light_green}]} /> */}
                        <Text style={styles.label3}>{local.high}</Text>
                    </View>
                  
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}>
               {
                        item.high_status === "up" ? <PriceRise /> :
                            item.high_status === "down" ? <PriceLow /> : 
                            <TouchableOpacity 
                                activeOpacity={1} 
                                style={[styles.normalIcon, {backgroundColor: Colors.light_green}]} />}
                        <Text style={[styles.value3, {color: Colors.light_green}]}>
                            {`${item.price_high} Ks`}
                        </Text>
               
                    </View>
                </View>
            </View>
       </View>
  </TouchableOpacity>
};

export default RiceItem;
