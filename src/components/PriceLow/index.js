import React from "react";
import {
    View, TouchableOpacity
} from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";

import Colors from "/assets/colors";
import {PriceLow as styles} from "assets/style";

const PriceLow = ({}) => {
  return <View style={styles.container}>
    <TouchableOpacity activeOpacity={1} style={styles.circle2} />
    <TouchableOpacity activeOpacity={1} style={styles.circle} />
    <AntDesign
        name='caretdown'
        size={hp(1)}
        color={"#a5cc78"} />
  </View>
};

export default PriceLow;
