import React, {
  useState,
  useEffect,
  useContext
} from "react";
import {
  Modal,
  View, 
  TouchableOpacity,
  TextInput,
  ToastAndroid,
  Text} from "react-native";
import { Button } from "react-native-paper";
import Ionicons from "react-native-vector-icons/Ionicons";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";

import { Context } from "context/Provider";
import apiUrl from "utils/apiUrl";
import { fetchPostByToken } from "utils/fetchData";
import {
  EditRicePriceModal as styles
} from "assets/style";
import Colors from "assets/colors";
import Loading from "components/Loading";

const EditRicePriceModal = ({riceTypes, item, onClose, onSubmit}) => {

  const {
    token
  } = useContext(Context);

  const [
    typeError,
    setTypeError
  ] = useState(false);
  const [
    lowError,
    setLowError
  ] = useState(false);
  const [
    mediumError,
    setMediumError
  ] = useState(false);
  const [
    highError,
    setHighError
  ] = useState(false);
  const [
    unitError,
    setUnitError
  ] = useState(false);
  const [
    lowPrice,
    setLowPrice
  ] = useState();
  const [
    mediumPrice,
    setMediumPrice
  ] = useState();
  const [
    highPrice,
    setHighPrice
  ] = useState();
  const [
    unit,
    setUnit
  ] = useState('');
  const [
    type,
    setType
  ] = useState();
  const [
    editingType,
    setEditingType
  ] = useState(false);
  const [
    loading,
    setLoading
  ] = useState(false);

  useEffect(()=> {
    if (item) {
      setLowPrice(item.price_low)
      setMediumPrice(item.price_medium)
      setHighPrice(item.price_high)
      setUnit(item.unit);
    }
  }, [item])

  const onLowPriceChange = (txt) => {
    setLowError(false);
    setLowPrice(txt);
  };

  const onMediumPriceChange = (txt) => {
    setMediumError(false);
    setMediumPrice(txt);
  };

  const onHighPriceChange = (txt) => {
    setHighError(false);
    setHighPrice(txt);
  };

  const onUnitChange = (txt) => {
    setUnitError(false);
    setUnit(txt);
  };

  const handleSubmit = async () => {
    let valid = true;
    if (!type && !item) {
      valid = false;
      setTypeError(true);
    }
    if (!lowPrice) {
      valid = false;
      setLowError(true);
    }
    if (!mediumPrice) {
      valid = false;
      setMediumError(true);
    }
    if (!highPrice) {
      valid = false;
      setHighError(true);
    }
    if (!unit) {
      valid = false;
      setUnitError(true);
    }
    if (!valid) return;

    setLoading(true);
    let response;
 
    if (item) { // Edit 
      const data = {
        id : item.id,
        // rice_type_id : type.id,
        price_low : lowPrice,
        price_medium : mediumPrice,
        price_high : highPrice,
        unit
      };
  
      response = await fetchPostByToken(
        apiUrl.updatePrice,
        data,
        token
      );
    } else { // Add new rice price
      const data = {
        rice_type_id : type.id,
        price_low : lowPrice,
        price_medium : mediumPrice,
        price_high : highPrice,
        unit
      };
  
      response = await fetchPostByToken(
        apiUrl.newRicePrice,
        data,
        token
      );
    }

    setLoading(false);
    if (response?.success) {
      ToastAndroid.show(response?.message ?? 'Success', ToastAndroid.SHORT)
      onSubmit();
    } else {
      ToastAndroid.show(response?.message ?? 'Fail', ToastAndroid.SHORT)
    }
  }

  const typeHandler = (chosenType) => {
    setType(chosenType)
    setEditingType(false)
    setTypeError(false)
  }

  const handleModalOpen = () => {
    if (item) {
      console.log("opened...")
      setLowPrice(item?.price_low+"")
      setMediumPrice(item?.price_medium+"")
      setHighPrice(item?.price_high+"")
      setUnit(item?.unit);
      const filterType = riceTypes.find(riceType => riceType.id === item.rice_type_id);
      setType(filterType)
    }
  }

  return (
    <Modal 
      visible={true} 
      onRequestClose={onClose}
      onShow={handleModalOpen}
      transparent={true}>
      <View style={styles.modalContainer}>
        <TouchableOpacity
            activeOpacity={0.8}
            style={styles.closeBtn}
            onPress={onClose}>
            <Ionicons
              name="ios-close-circle-outline"
              size={hp(3.2)}
              color="darkgray" />
          </TouchableOpacity>
        <View style={styles.modal}>
          {/* {item ? <Text style={styles.title}>
            {item.name}
          </Text> :  */}
          <>
          <Text style={styles.title}>
            {item ? "Edit Rice Price" : "Add Rice Price"}
          </Text>
          <View>
            <TouchableOpacity 
              activeOpacity={0.8}
              onPress={()=> setEditingType(!editingType)}
              style={styles.selectWrapper}>
                <Text style={{
                  fontSize: hp(1.8),
                  color: type ? "#000" : "darkgray"
                }}>
                  {type ? type.name : "ဆန်အမျိုးအစား ရွေးချယ်ပါ" }
                </Text>
              <Ionicons
                name={editingType ? "caret-up-circle" : "caret-down-circle"}
                size={hp(2.8)}
                color={Colors.light_green} />
            </TouchableOpacity>
            {!!typeError && <Text style={styles.errTxt}>Please choose rice type</Text>}
            {editingType && <TouchableOpacity 
                activeOpacity={1}
                style={styles.filterList}>
                {riceTypes && riceTypes.length > 0 ? <>
                  {riceTypes?.map((riceType, index) => {
                    return <TouchableOpacity
                      onPress={ () => typeHandler(riceType) }>
                      <Text style={[styles.riceName, {paddingBottom : index === riceTypes.length -1 ? 0 : hp(2)}]}>{riceType.name}</Text>
                    </TouchableOpacity>
                  })} 
                </> : null}
              </TouchableOpacity>}
          </View>
          </>

          <Text style={[styles.label, {color: '#000'}]}>
            Low Price
          </Text>
          <TextInput
            placeholder="Enter here"
            value={lowPrice}
            onChangeText={onLowPriceChange}
            keyboardType="numeric"
            autoCapitalize="none"
            style={[styles.input, {borderColor: 'lightgray'}]}
            underlineColor="transparent" />
          {!!lowError && <Text style={styles.errTxt}>Please enter low price</Text>}

          <Text style={[styles.label, {color: '#000'}]}>
            Medium Price
          </Text>
          <TextInput
            placeholder="Enter here"
            value={mediumPrice}
            onChangeText={onMediumPriceChange}
            keyboardType="numeric"
            autoCapitalize="none"
            style={[styles.input, {borderColor: 'lightgray'}]}
            underlineColor="transparent" />
          {!!mediumError && <Text style={styles.errTxt}>Please enter medium price</Text>}

          <Text style={[styles.label, {color: '#000'}]}>
            High Price
          </Text>
          <TextInput
            placeholder="Enter here"
            value={highPrice}
            onChangeText={onHighPriceChange}
            keyboardType="numeric"
            autoCapitalize="none"
            style={[styles.input, {borderColor: 'lightgray'}]}
            underlineColor="transparent" />
          {!!highError && <Text style={styles.errTxt}>Please enter high price</Text>}

          <Text style={[styles.label, {color: '#000'}]}>
            Unit
          </Text>
          <TextInput
            placeholder="Enter here"
            value={unit}
            onChangeText={onUnitChange}
            keyboardType="default"
            autoCapitalize="none"
            style={[styles.input, styles.textAreaInput, {borderColor: 'lightgray'}]}
            underlineColor="transparent"
            multiline={true}
            numberOfLines={2}
            textAlignVertical="top" />
          {!!unitError && <Text style={styles.errTxt}>Please enter unit</Text>}

          <Button
            mode="contained"
            onPress={handleSubmit}
            style={styles.button}
            labelStyle={{fontSize: hp(1.8)}}>
            SUBMIT
          </Button>
        </View>
      </View>
      <Loading visible={loading} />
    </Modal>
     
  );
};

export default EditRicePriceModal;
