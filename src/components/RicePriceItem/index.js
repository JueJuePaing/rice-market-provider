import React from "react";
import {
    View, 
    Text, 
    TouchableOpacity
} from "react-native";
import Feather from "react-native-vector-icons/Feather";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";

import { useLocal } from "../../hook/useLocal";
import {RiceItem as styles} from "assets/style";
import Colors from "assets/colors";
import PriceRise from "../PriceRise";
import PriceLow from "../PriceLow";


const RicePriceItem = ({item, editHandler}) => {
    const local = useLocal();
  return <TouchableOpacity
    activeOpacity={1}
    style={styles.item}>
        {item.date && <Text style={styles.priceDate}>
            {item.date}
        </Text>}
        <View style={styles.nameRow}>
            <Text style={styles.name}>
                {item.name}
            </Text>
          
            <TouchableOpacity
                style={styles.editBtn}
                activeOpacity={0.8}
                onPress={()=> editHandler(item)} >
                <Feather
                    name="edit-3"
                    size={hp(2)}
                    color="lightgray" />
            </TouchableOpacity>
        </View>
       {/* <View style={styles.divider} /> */}
        <View style={[styles.priceContent, styles.priceContent2]}>
            <View style={styles.priceBlock3}>
                <View>
                <View style={styles.circleRow}>
                    {/* <View style={styles.circle} /> */}
                    <Text style={styles.label3}>{local.low}</Text>
                </View>
                {/* <Text style={[styles.value3, {color: Colors.low_color}]}>
                    {`${item.low} Ks`}
                </Text> */}
                  <View style={{
                    flexDirection: 'row',
                    alignItems: 'center'
                    }}>
                    {
                        item.low_status === "up" ? <PriceRise /> :
                            item.low_status === "down" ? <PriceLow /> : 
                            <TouchableOpacity 
                                activeOpacity={1} 
                                style={[styles.normalIcon, {backgroundColor: Colors.low_color}]} />}
                    <Text style={[styles.value3, {color:Colors.low_color}]}>
                        {`${item.price_low} Ks`}
                    </Text>
                </View>
                </View>
            </View>
            <View style={styles.priceBlock3}>
                <View>
                    <View style={styles.circleRow}>
                        {/* <View style={[styles.circle, {backgroundColor: Colors.medium_color}]} /> */}
                        <Text style={styles.label3}>{local.medium}</Text>
                    </View>
                    <View style={{
                            flexDirection: 'row',
                            alignItems: 'center'
                        }}>
                        {
                            item.medium_status === "up" ? <PriceRise /> :
                                item.medium_status === "down" ? <PriceLow /> : 
                                <TouchableOpacity 
                                    activeOpacity={1} 
                                    style={[styles.normalIcon, {backgroundColor: Colors.medium_color}]} />}
                        <Text style={[styles.value3, {color:Colors.medium_color}]}>
                            {`${item.price_medium} Ks`}
                        </Text>
                    </View>
                </View>
            </View>
            <View style={styles.priceBlock3}>
                <View>
                <View style={styles.circleRow}>
                    {/* <View style={[styles.circle, {backgroundColor: Colors.light_green}]} /> */}
                    <Text style={styles.label3}>{local.high}</Text>
                </View>
                {/* <Text style={[styles.value3, {color: Colors.light_green}]}>
                    {`${item.high} Ks`}
                </Text> */}
                <View style={{
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}>
                    {
                        item.high_status === "up" ? <PriceRise /> :
                            item.high_status === "down" ? <PriceLow /> : 
                            <TouchableOpacity 
                                activeOpacity={1} 
                                style={[styles.normalIcon, {backgroundColor: Colors.light_green}]} />}
                    <Text style={[styles.value3, {color:Colors.light_green}]}>
                        {`${item.price_high} Ks`}
                    </Text>
                </View>
                </View>
            </View>
       </View>
  </TouchableOpacity>
};

export default RicePriceItem;
