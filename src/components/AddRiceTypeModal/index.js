import React, {useEffect, useState} from "react";
import {
  Modal,
  View, 
  TouchableOpacity,
  TextInput,
  Text} from "react-native";
import { Button } from "react-native-paper";
import Ionicons from "react-native-vector-icons/Ionicons";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";

import { useLocal } from "hook/useLocal";
import {
  AddRiceTypeModal as styles
} from "assets/style";


const AddRiceTypeModal = ({onClose, onSubmit, editingRiceType}) => {

  const local = useLocal();
  
  const [
    name,
    setName
  ] = useState("");
  const [
    nameError,
    setNameError
  ] = useState(true);

  useEffect(() =>{
    if (editingRiceType) {
      if (editingRiceType.name) {
        setNameError(false);
        setName(editingRiceType.name);
      }
    }
  }, [editingRiceType]);

  const onNameChange = (txt) => {
    setName(txt);
    if (!!txt) {
      setNameError(false);
    } else {
      setNameError(true)
    }
  };

  const handleSubmit = () => {
    let valid = true;
    if (!name) {
      setNameError(true);
      valid = false
    } 
    if (!valid) return;
    onSubmit(name)
  }

  return (
    <Modal 
      visible={true} 
      onRequestClose={onClose}
      transparent={true}>
      <View style={styles.modalContainer}>
        <View style={styles.modal}>
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.closeBtn}
            onPress={onClose}>
            <Ionicons
              name="ios-close-circle-outline"
              size={hp(3.2)}
              color="darkgray" />
          </TouchableOpacity>
          <Text style={styles.title}>
            {local.addRiceType}
          </Text>
          <TextInput
            placeholder={local.riceTypeName}
            value={name}
            onChangeText={onNameChange}
            keyboardType="default"
            autoCapitalize="none"
            style={styles.input}
            placeholderTextColor='gray'
            underlineColor="transparent" />
          <Button
            mode="contained"
            onPress={handleSubmit}
            style={styles.button}
            labelStyle={{fontSize: hp(1.8)}}
            disabled={nameError}>
            {local.submit}
          </Button>
        </View>
      </View>
    </Modal>
     
  );
};

export default AddRiceTypeModal;
