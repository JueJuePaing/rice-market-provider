import React, {useState} from "react";
import {View, TouchableOpacity, Text} from "react-native";
import {Portal, Modal} from "react-native-paper";
import {Calendar} from "react-native-calendars";
import {
  DateRangePicker as styles
} from "assets/style";
import Colors from "assets/colors";
import { useLocal } from "../../hook/useLocal";

const DateRangePicker = ({visible, onClose, onSubmit}) => {
  const [range, setRange] = useState({});
  const local = useLocal();

  const onDayPress = (day) => {
    if (Object.keys(range).length === 2) {
      // If we already have a range selected, reset it and start over
      setRange({
        [day.dateString] : {
          selected : true,
          marked : true,
          selectedColor : Colors.dark_yellow
        }
      });
    } else if (Object.keys(range).length === 1) {
      // If we have one day selected, select the range
      const start = Object.keys(range)[0];
      const end = day.dateString;
      const rangeDates = {};

      // Generate an object with all the dates in the range
      for (
        let d = new Date(start);
        d <= new Date(end);
        d.setDate(d.getDate() + 1)
      ) {
        const date = new Date(d).toISOString().split("T")[0];
        rangeDates[date] = {
          selected : true,
          marked :
            d.toDateString() === new Date(start).toDateString() ||
            d.toDateString() === new Date(end).toDateString()
              ? true
              : false,
          selectedColor : Colors.dark_yellow
        };
      }

      // Update the range state with the selected dates
      setRange(rangeDates);
    } else {
      // If we have no dates selected, select the starting day of the range
      setRange({
        [day.dateString] : {
          selected : true,
          marked : true,
          selectedColor : Colors.dark_yellow
        }
      });
    }
  };

  const onCancel = () => {
    setRange({});
    onClose();
  };

  const onConfirm = () => {
    // Extract start and end dates from range object
    const dates = Object.keys(range);
    const start = dates[0];
    const end = dates[dates.length - 1];

    onSubmit(start, end);
  };

  return (
      <Portal>
        <Modal visible={visible} onDismiss={onCancel}>
          <View style={styles.modal}>
            <Calendar
              current={new Date().toISOString()}
              markedDates={range}
              onDayPress={onDayPress}
              style={styles.calendar}/>
            <View style={styles.footer}>
              <TouchableOpacity onPress={onCancel}>
                <Text style={styles.footerButton}>{local.cancel}</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={onConfirm}>
                <Text style={styles.footerButton}>{local.confirm}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </Portal>
  );
};

export default DateRangePicker;
