import React, {useState} from "react";
import {View, TouchableOpacity, Text} from "react-native";
import {Portal, Modal} from "react-native-paper";
import DatePicker from 'react-native-date-picker';

import { useLocal } from "../../hook/useLocal";
import {
  DateTimePicker as styles
} from "assets/style";

const DateTimePicker = ({currentDate, onClose, onSubmit}) => {

  const local = useLocal();
  const [
    date,
    setDate
  ] = useState(currentDate);


  const onDateChange = (selectedDate) => {
    setDate(selectedDate);
  };
  
  return <Portal>
  <Modal visible={true} onDismiss={onClose}>
    <View style={styles.modal}>
     <DatePicker
      androidVariant="iosClone"
      style={styles.datePicker}
      mode="datetime"
      date={date}
      onDateChange={onDateChange}
      textColor='#000'
    />
      <View style={styles.footer}>
        <TouchableOpacity onPress={onClose}>
          <Text style={styles.footerButton}>{local.cancel}</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => onSubmit(date)}>
          <Text style={styles.footerButton}>{local.confirm}</Text>
        </TouchableOpacity>
      </View>
    </View>
  </Modal>
  </Portal>;

};

export default DateTimePicker;
