import React from "react";
import {
    View, 
    Text, 
    TouchableOpacity,
    Image
} from "react-native";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";

import {ShopItem as styles} from "assets/style";
import RiceWarehouse from 'assets/icons/RiceWarehouse';
import HomeRiceMill from "assets/icons/HomeRiceMill";

const ShopItem = ({mill, item, itemHandler, profileHandler}) => {
  return <TouchableOpacity
    activeOpacity={1}
    style={styles.item}>
        {item.image ? <TouchableOpacity
            onPress={() => profileHandler ? profileHandler(item) : null}>
            <Image
                style={styles.iconImg}
                source={{ uri : item.image }} /></TouchableOpacity> : <View style={styles.iconWrapper}>
            {mill ? <HomeRiceMill color='#fff' /> : <RiceWarehouse color='#fff' />}
        </View>}
        <TouchableOpacity 
            activeOpacity={0.8}
            onPress={itemHandler}
            style={styles.nameContent}>
            <View style={styles.row}>
                <Text style={styles.name}>
                    {item.name}
                </Text>
                {item.rating && <View style={styles.typeBadge}>
                    <Text style={styles.typeCount}>
                        {item.rating ? Math.ceil(item.rating) : 0}
                    </Text>
                    <FontAwesome
                        name="star"
                        size={hp(1.6)}
                        color="#d6a609" />
                </View>}
            </View>
            <Text 
                numberOfLines={1}
                style={styles.address}>
                {item.address}
            </Text>
        </TouchableOpacity>
  </TouchableOpacity>
};

export default ShopItem;
