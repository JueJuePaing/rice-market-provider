import React, {useState} from "react";
import {
  Modal,
  View, 
  TouchableOpacity,
  TextInput,
  ToastAndroid,
  Text} from "react-native";
import { Button } from "react-native-paper";
import Ionicons from "react-native-vector-icons/Ionicons";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

import {
  ReviewModal as styles
} from "assets/style";
import Rating from "components/Rating/RatingBtn";
import {useLocal} from '../../hook/useLocal';

const ReviewModal = ({name, defaultRating, onClose, onSubmit}) => {
  const local = useLocal();

  const [
    rating,
    setRating
  ] = useState(0);
  const [
    review,
    setReview
  ] = useState("");

  const onReviewChange = (txt) => {
    setReview(txt);
  };

  const handleSubmit = () => {
    onSubmit(rating, review)
  }

  const rateHandler = (rate) => {
    setRating(rate);
  }

  return (
    <Modal 
      visible={true} 
      onRequestClose={onClose}
      transparent={true}>
      <View style={styles.modalContainer}>
        <View style={styles.modal}>
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.closeBtn}
            onPress={onClose}>
            <Ionicons
              name="ios-close-circle-outline"
              size={hp(3.2)}
              color="darkgray" />
          </TouchableOpacity>
          <Text style={styles.title}>
            {local.giveFeedback}
          </Text>
          <Text style={styles.description}>
            {`${local.whatyouthink} ${name}`} 
          </Text>

          <Rating 
            rating={rating}
            rateHandler={rateHandler}  />

          {/* <View style={styles.ratingContent}>
            <TouchableOpacity
              activeOpacity={0.8}
              style={[styles.ratingBtn, {backgroundColor: rating === 1 ? 'rgba(219, 230, 195, 0.4) ': null}]}
              onPress={()=> {
                setError(false);
                setRating(1)
              }}>
              <Ionicons
                name="sad-outline"
                size={rating === 1 ? wp(10) : wp(9)}
                color={rating === 1 ?  Colors.dark_yellow : "darkgray"} />
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.8}
              style={[styles.ratingBtn, {backgroundColor: rating === 2? 'rgba(219, 230, 195, 0.4) ': null}]}
              onPress={()=> {
                setError(false);
                setRating(2)
              }}>
              <MCIcon
                name="emoticon-sad-outline"
                size={rating === 2 ? wp(10) : wp(9.3)}
                color={rating === 2 ?  Colors.dark_yellow : "darkgray"} />
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.8}
              style={[styles.ratingBtn, {backgroundColor: rating === 3 ? 'rgba(219, 230, 195, 0.4) ': null}]}
              onPress={()=> {
                setError(false);
                setRating(3)
              }}>
              <MCIcon
                name="emoticon-neutral-outline"
                size={rating === 3 ? wp(10) : wp(9.3)}
                color={rating === 3 ?  Colors.dark_yellow : "darkgray"} />
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.8}
              style={[styles.ratingBtn, {backgroundColor: rating === 4 ? 'rgba(219, 230, 195, 0.4) ': null}]}
              onPress={()=> {
                setError(false);
                setRating(4)
              }}>
              <MCIcon
                name="emoticon-happy-outline"
                size={rating === 4 ? wp(10) : wp(9.3)}
                color={rating === 4 ?  Colors.dark_yellow : "darkgray"} />
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.8}
              style={[styles.ratingBtn, {backgroundColor: rating === 5 ? 'rgba(219, 230, 195, 0.4) ': null}]}
              onPress={()=> {
                setError(false);
                setRating(5)
              }}>
              <Ionicons
                name="happy-outline"
                size={rating === 5 ? wp(10) : wp(9.3)}
                color={rating === 5 ?  Colors.dark_yellow : "darkgray"} />
            </TouchableOpacity>
          </View> */}
          <TextInput
            numberOfLines={6}
            multiline={true}
            placeholder={local.enterReview}
            value={review}
            onChangeText={onReviewChange}
            keyboardType="default"
            autoCapitalize="none"
            textAlignVertical="top"
            style={styles.input}
            underlineColor="transparent"/>
          <Button
            mode="contained"
            onPress={handleSubmit}
            style={styles.button}
            labelStyle={{fontSize: hp(1.8)}}
            disabled={!review}>
            {local.submit}
          </Button>
        </View>

      </View>
    </Modal>
     
  );
};

export default ReviewModal;
