import React, {
  memo
} from "react";
import {
  View
} from "react-native";
import { useTheme } from "react-native-paper";

import {
  MainAppContainer as style
} from "assets/style";

function MainApplicationContainer({children}) {

  const { colors } = useTheme();

  return (
    <View style={[ style.container, { backgroundColor : colors.background }] }>
      { children }
    </View>
  );
}

export default memo(MainApplicationContainer);
