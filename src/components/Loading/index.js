import React from "react";
import {View, Modal} from "react-native";
import {UIActivityIndicator} from "react-native-indicators";

import {LoadingModal as styles} from "assets/style";
import Colors from "assets/colors";

const Loading = ({visible}) => {
  if (visible) {
    return (
      <Modal visible={true} transparent={true}>
        <View style={styles.container}>
          <UIActivityIndicator color={Colors.light_yellow} size={35} />
        </View>
      </Modal>
    );
  } else {
    return null;
  }
};

export default Loading;
