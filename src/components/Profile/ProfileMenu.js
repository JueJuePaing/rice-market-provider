import React from "react";
import {
    View, 
    Text, 
    TouchableOpacity
} from "react-native";
import Ionicons from 'react-native-vector-icons/Ionicons';
import { heightPercentageToDP as hp } from "react-native-responsive-screen";

import {ProfileMenu as styles} from "assets/style";

const ProfileMenu = ({item, itemHandler}) => {
  return <TouchableOpacity
        activeOpacity={0.8}
        style={styles.item}
        onPress={itemHandler}>
        <Text style={styles.name}>{item.name}</Text>
        <Ionicons
            name="chevron-forward-outline"
            size={hp(2.5)}
            color="#42413f" />
    </TouchableOpacity>
};

export default ProfileMenu;
