import React, {
  memo
} from "react";
import {
  View,
  KeyboardAvoidingView
} from "react-native";
import {
  useSafeArea
} from "react-native-safe-area-context";

import {
  MainAppBody as style
} from "assets/style";

import {
  isIos
} from "utils/funcs";

function MainApplicationBody({children}) {
  
  const {
    bottom : insetBottom,
    top : insetTop
  } = useSafeArea();

  const iosPlatform = isIos();
  const keyboardBehavior = iosPlatform ? "padding" : "height";
  const keyboardOffset = 0;

  return (
    <View
      style={ [
        style.container,
        {
          paddingBottom : insetBottom,
          paddingTop : insetTop
        }
      ] }>
      <KeyboardAvoidingView
        style={ style.contentContainer }
        keyboardVerticalOffset={ keyboardOffset }
        behavior={ keyboardBehavior }>
        { children }
      </KeyboardAvoidingView>
    </View>
  );
}

export default memo(MainApplicationBody);
