import React, {
  useState,
  useEffect
} from "react";
import {
    View, 
    Text, 
    TouchableOpacity,
    Image
} from "react-native";
import {
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import { createThumbnail } from "react-native-create-thumbnail";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import {MessageText as styles} from "assets/style";
import Colors from "assets/colors";

const MessageSelfVideo = ({item, playVideoHandler}) => {

  const [
    thumbnail, 
    setThumbnail
  ] = useState();

  useEffect(() => {
    createThumbnail({
      url: item.file_name,
      timeStamp: 1000,
    })
      .then(response => {
        if (response.path) {
          setThumbnail(response.path)
        }
      })
      .catch(err => console.log({ err }));
  }, [item]);

  return <View style={styles.selfContent}>
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={playVideoHandler}
      style={styles.thumbnailContent}>
      <Image  
        source={{
          uri : thumbnail
        }}
        style={styles.videoThumbnail} />

      <FontAwesome5
        name={"play"}
        size={hp(5.5)}
        color={Colors.light_green}
        style={{position: 'absolute'}} />

    </TouchableOpacity>
    <Text style={[styles.time, styles.selfTime]}>{item.date_time}</Text>
  </View>
};

export default MessageSelfVideo;
