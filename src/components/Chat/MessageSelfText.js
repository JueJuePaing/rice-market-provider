import React from "react";
import {
    View, 
    Text, 
    TouchableOpacity
} from "react-native";

import {MessageText as styles} from "assets/style";

const MessageSelfText = ({item, itemHandler}) => {
  return <View style={styles.selfContent}>
    <TouchableOpacity
      activeOpacity={0.8}
      style={[styles.item, styles.selfItem]}
      onLongPress={itemHandler}>
      <Text style={[styles.message, styles.selfMessage]}>{item.message}</Text>
    </TouchableOpacity>
    <Text style={[styles.time, styles.selfTime]}>{item.date_time}</Text>
  </View>
};

export default MessageSelfText;
