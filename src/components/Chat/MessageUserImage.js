import React from "react";
import {
    View, 
    Text, 
    Image,
    TouchableOpacity
} from "react-native";

import {MessageText as styles} from "assets/style";

const MessageUserImage = ({profile, item, itemHandler}) => {
  return <View style={styles.userContent}>
    <View style={styles.userRow}>
      <View style={styles.profileContent}>
        <Image
          style={styles.profile}
          source={{ uri : profile }} />
      </View>
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={itemHandler}>
       <Image 
        source={{
          uri : item.file_name
        }}
        style={[styles.chatImg, styles.userChatImg]} />
      </TouchableOpacity>
    </View>
    <Text style={[styles.time, styles.userTime]}>{item.date_time}</Text>
  </View>
};

export default MessageUserImage;
