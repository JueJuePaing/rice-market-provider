import React from "react";
import {
    View, 
    Text, 
    TouchableOpacity,
    Image
} from "react-native";

import {ChatItem as styles} from "assets/style";

const ChatItem = ({item, itemHandler}) => {
  return <TouchableOpacity
    activeOpacity={0.8}
    style={styles.item}
    onPress={itemHandler}>
      <View style={styles.profileContent}>
        <Image
          style={styles.profile}
          source={{ uri : item.profile }} />
      </View>
      <View>
        <Text style={styles.name}>{item.friend_name}</Text>
        <Text style={styles.lastMessage}>{item.last_text}</Text>
      </View>
      <Text style={styles.timestamp}>{item.date_time}</Text>
  </TouchableOpacity>
};

export default ChatItem;
