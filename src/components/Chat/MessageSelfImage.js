import React from "react";
import {
    View, 
    Text, 
    Image,
    TouchableOpacity
} from "react-native";

import {MessageText as styles} from "assets/style";

const MessageSelfImage = ({item, itemHandler}) => {
  return <View style={styles.selfContent}>
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={itemHandler}>
      <Image 
        source={{
          uri : item.file_name
        }}
        style={[styles.chatImg, styles.selfChatImg]} />
    </TouchableOpacity>
    <Text style={[styles.time, styles.selfTime]}>{item.date_time}</Text>
  </View>
};

export default MessageSelfImage;
