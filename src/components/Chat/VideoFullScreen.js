import React, {
  useRef,
  useState,
  useEffect,
  useCallback
} from "react";
import {
    View,
    TouchableOpacity,
    BackHandler
} from "react-native";
import Video from 'react-native-video';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import AntDesign from 'react-native-vector-icons/AntDesign';
import Slider from '@react-native-community/slider';
import {UIActivityIndicator} from "react-native-indicators";

import {
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
 
import Colors from "assets/colors";
import {MessageText as styles} from "assets/style";

const VideoFullScreen = ({item, onClose}) => {

  const videoRef = useRef(null);

  const [
    duration,
    setDuration
  ] = useState(1);
  const [
    videoLoading,
    setVideoLoading
  ] = useState(true)
  const [isPlaying, setIsPlaying] = useState(true);
  const [currentTime, setCurrentTime] = useState(0);

  const onLoad = (data) => {
    setVideoLoading(false)
    setDuration(data.duration);
  };

 
  const backButtonHandler = useCallback(() => {
    return true;
  }, []);
  
useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, [backButtonHandler]);
  
  const handleProgress = (progress) => {
    setCurrentTime(progress.currentTime);
  };

  const togglePlayPause = () => {
      setIsPlaying(!isPlaying);

  };

  const handleSliderChange = (value) => {
    if (videoRef.current) {
      videoRef.current.seek(value);
      setCurrentTime(value);
    }
  };

  return <View style={styles.videoContent}>
    <TouchableOpacity onPress={onClose} style={styles.closeIcon}>
      <Icon name="close-circle-outline" size={hp(4.5)} color="#fff" />
    </TouchableOpacity>
    <Video
      ref={videoRef}
      source={{
        uri : item.file_name
      }}
      controls={false} // enable player controls
      paused={!isPlaying}
      resizeMode="contain" 
      style={styles.video}
      onLoad={onLoad}   
      onEnd={()=> {
        setIsPlaying(false)
        setCurrentTime(0)
        videoRef.current.seek(0);
      }} 
      onProgress={handleProgress} />
    {!videoLoading && <>
      <TouchableOpacity
        activeOpacity={0.8}
        style={styles.playButton}
        onPress={togglePlayPause}>
        <AntDesign
          name={!isPlaying ? "play" : "pausecircleo"}
          size={hp(4.5)}
          color='lightgray' />
      </TouchableOpacity>
      <Slider
        style={styles.slider}
        minimumValue={0}
        maximumValue={duration ? duration : 1}
        value={currentTime}
        onValueChange={handleSliderChange}
        minimumTrackTintColor="#009688"
        maximumTrackTintColor="#ccc"
        thumbTintColor="#009688"
      />
    </>}

      {videoLoading && <UIActivityIndicator 
        style={{position: 'absolute'}} 
        color={Colors.light_yellow} size={35} />}

  </View>
};

export default VideoFullScreen;
