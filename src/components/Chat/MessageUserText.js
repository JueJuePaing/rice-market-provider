import React from "react";
import {
    View, 
    Text, 
    Image,
    TouchableOpacity
} from "react-native";

import {MessageText as styles} from "assets/style";

const MessageUserText = ({profile, item, itemHandler}) => {
  return <View style={styles.userContent}>
    <View style={styles.userRow}>
      <View style={styles.profileContent}>
        <Image
          style={styles.profile}
          source={{ uri : profile }} />
      </View>
      <TouchableOpacity
        activeOpacity={0.8}
        style={[styles.item, styles.userItem]}
        onLongPress={itemHandler}>
        <Text style={[styles.message, styles.userMessage]}>{item.message}</Text>
      </TouchableOpacity>
    </View>

      <Text style={[styles.time, styles.userTime]}>{item.date_time}</Text>

  </View>
};

export default MessageUserText;
