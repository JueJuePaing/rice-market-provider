import React from "react";
import {
    View, 
    TextInput,
    TouchableOpacity
} from "react-native";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import {MessageInput as styles} from "assets/style";
import Colors from "assets/colors";

const MessageInput = ({
  message, 
  sending,
  changeMessage,
  videoRecordHandler,
  cameraHandler,
  sendHandler
}) => {

  return <View style={styles.container}>
    <View style={styles.inputWrapper}>
      <TextInput
          editable={!sending}
          placeholder="Message"
          value={sending ? '' : message}
          onChangeText={changeMessage}
          keyboardType="default"
          autoCapitalize="none"
          style={styles.input}
          underlineColor="transparent"/>
      <TouchableOpacity
        style={[styles.imgButton, {marginRight : wp(1)}]}
        activeOpacity={0.8}
        onPress={cameraHandler}>
        <Feather
          name="camera"
          size={wp(6)}
          color='gray' />
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.imgButton}
        activeOpacity={0.8}
        onPress={videoRecordHandler}>
        <Feather
          name="video"
          size={wp(6)}
          color='gray' />
      </TouchableOpacity> 
    </View>
    <TouchableOpacity
      style={styles.sendBtn}
      activeOpacity={0.8}
      onPress={sendHandler}>
      <FontAwesome
        name="send"
        size={wp(6)}
        color={Colors.light_green} />
    </TouchableOpacity>
  </View>
};

export default MessageInput;
