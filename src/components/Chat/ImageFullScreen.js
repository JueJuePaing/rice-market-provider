import React, {
  useEffect,
  useCallback
} from "react";
import {
    View,
    TouchableOpacity,
    BackHandler,
    Image
} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import {
  heightPercentageToDP as hp, 
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
 
import {MessageText as styles} from "assets/style";

const ImageFullScreen = ({item, onClose}) => {

  
  const backButtonHandler = useCallback(() => {
    return true;
  }, []);
  
useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, [backButtonHandler]);
  
  return <View style={styles.videoContent}>
    <TouchableOpacity onPress={onClose} style={styles.closeIcon}>
      <Icon name="close-circle-outline" size={hp(4.5)} color="#fff" />
    </TouchableOpacity>
    <Image
      source={{ uri : item.file_name }}
      style={{
        width : wp(100),
        height : hp(70),
         resizeMode : 'contain'
      }} />
  </View>
};

export default ImageFullScreen;
