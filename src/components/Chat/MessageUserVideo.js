import React, {useState, useEffect} from "react";
import {
    View, 
    Text, 
    Image,
    TouchableOpacity
} from "react-native";
import {
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import { createThumbnail } from "react-native-create-thumbnail";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import Colors from "assets/colors";
import {MessageText as styles} from "assets/style";

const MessageUserVideo = ({profile, item, playVideoHandler}) => {

  const [
    thumbnail, 
    setThumbnail
  ] = useState();


  useEffect(() => {
    createThumbnail({
      url: item.file_name,
      timeStamp: 1000,
    })
      .then(response => {
        if (response.path) {
          setThumbnail(response.path)
        }
      })
      .catch(err => console.log({ err }));
  }, [item]);

  return <View style={styles.userContent}>
    <View style={styles.userRow}>
      <View style={styles.profileContent}>
        <Image
          style={styles.profile}
          source={{ uri : profile }} />
      </View>
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={playVideoHandler}
        style={styles.thumbnailContent}>

        <Image  
          source={{
            uri : thumbnail
          }}
          style={[styles.videoThumbnail, {
            borderBottomLeftRadius : 0,
            borderBottomRightRadius : hp(3)
          }]} />
        <FontAwesome5
          name={"play"}
          size={hp(5.5)}
          color={Colors.light_green}
          style={{position: 'absolute'}} />
      </TouchableOpacity>
    </View>
    <Text style={[styles.time, styles.userTime]}>{item.date_time}</Text>
  </View>
};

export default MessageUserVideo;
