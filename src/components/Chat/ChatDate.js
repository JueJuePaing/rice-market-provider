import React from "react";
import {
    View, 
    Text
} from "react-native";

import {MessageText as styles} from "assets/style";

const ChatDate = ({date}) => {
  return <View style={styles.dateContent}>
    <Text style={styles.date}>{date}</Text>
  </View>
};

export default ChatDate;
