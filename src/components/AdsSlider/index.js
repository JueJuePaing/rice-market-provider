
import React, {
  useRef, useState 
} from "react";
import {
  FlatList, Text, View, Image, TouchableOpacity
} from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";

import {
  AdsSlider as styles
} from "assets/style";

const CENTER_ITEM_WIDTH = wp(80);
const ITEM_SPACING = 10;

const AdsSlider = ({ data, dataHandler }) => {
  const [centerIndex, setCenterIndex] = useState(0);
  const flatListRef = useRef(null);

  const onMomentumScrollEnd = (event) => {
    const contentOffset = event.nativeEvent.contentOffset.x;
    const index = Math.round(contentOffset / (CENTER_ITEM_WIDTH + ITEM_SPACING));
    setCenterIndex(index);
  };

  const scrollToCenterItem = () => {
    const x = centerIndex * (CENTER_ITEM_WIDTH + ITEM_SPACING);
    flatListRef.current?.scrollToOffset({
      offset : x,
      animated : false 
    });
  };

  const renderItem = ({
    item, index 
  }) => {
    const isCenter = index === centerIndex;
    const itemStyle = isCenter ? styles.centerItem : styles.item;
    console.log(isCenter);
    return (
      <TouchableOpacity
        onPress={()=> dataHandler(item)}
        style={[itemStyle, {
          width : data.length === 1 ? wp(90) : wp(80)
        }]}>
        <Image
          source={{ uri : item.image }}
          style={styles.bannerImg}
        />
        <View style={styles.textWrapper}>
          <Text style={styles.text}>{item.title}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <FlatList
        ref={flatListRef}
        data={data}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        horizontal
        contentContainerStyle={styles.carbonContainer}
        showsHorizontalScrollIndicator={false}
        snapToInterval={CENTER_ITEM_WIDTH + ITEM_SPACING}
        decelerationRate="fast"
        onMomentumScrollEnd={onMomentumScrollEnd}
        onLayout={scrollToCenterItem}
        getItemLayout={(data, index) => ({
          length : CENTER_ITEM_WIDTH + ITEM_SPACING,
          offset : (CENTER_ITEM_WIDTH + ITEM_SPACING) * index,
          index
        })}
        initialNumToRender={5}
        windowSize={5}
        maxToRenderPerBatch={5}/>
    </View>
  );
};
export default AdsSlider;
