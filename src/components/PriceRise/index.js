import React from "react";
import {
    View, TouchableOpacity
} from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";

import {PriceRise as styles} from "assets/style";

const PriceRise = ({}) => {
  return <View style={styles.container}>
    <AntDesign
        name='caretup'
        size={hp(1)}
        color='#f27f72' />
    <TouchableOpacity activeOpacity={1} style={styles.circle} />
    <TouchableOpacity activeOpacity={1} style={styles.circle2} />
  </View>
};

export default PriceRise;
