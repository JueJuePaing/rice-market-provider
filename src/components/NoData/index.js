import React from "react";
import {
    Text, 
    TouchableOpacity
} from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import Ionicons from 'react-native-vector-icons/Ionicons';
import {NoPackage as styles} from "assets/style";

const NoData = ({text, reloadHandler}) => {
  return <><Text style={styles.noData}>
        {text}
        </Text>
        <TouchableOpacity style={{
            alignSelf: 'center',
            marginTop: hp(2)
            }}
            onPress={reloadHandler}>
            <Ionicons
                name='reload'
                size={hp(3.5)}
                />
        </TouchableOpacity>
    </>
};

export default NoData;
