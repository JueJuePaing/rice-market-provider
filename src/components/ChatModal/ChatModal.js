import React from "react";
import {View, Modal, Text, TouchableOpacity} from "react-native";

import {useLocal} from 'hook/useLocal';
import styles from './Style';

const ChatModal = ({visible, image, onClose, chooseImgHandler, chooseVideoHandler}) => {
  const local = useLocal();

  if (visible) {
    return (
      <Modal 
        visible={true} 
        onRequestClose={onClose}
        transparent={true}>
        <TouchableOpacity style={styles.modalContainer} activeOpacity={1} onPress={onClose}>
          <View style={styles.modal}>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={()=> image ? chooseImgHandler(1) : chooseVideoHandler(1)}
              style={[styles.item, styles.firstItem]}>
              <Text style={styles.itemTxt}>
                {image ? local.takeCamera : local.recordVideo}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={()=> image ? chooseImgHandler(2) : chooseVideoHandler(2)}
              style={[styles.item, styles.secondItem]}>
              <Text style={styles.itemTxt}>
                {local.chooseGallery}
              </Text>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </Modal>
    );
  } else {
    return null;
  }
};

export default ChatModal;
