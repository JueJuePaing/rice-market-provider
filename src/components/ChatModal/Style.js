import { StyleSheet } from "react-native";
import {
    heightPercentageToDP as hp, 
    widthPercentageToDP as wp
  } from "react-native-responsive-screen";

const styles = StyleSheet.create({
    modalContainer : {
        backgroundColor : 'rgba(0,0,0,0.8)',
        flex : 1,
        alignItems : "center",
        justifyContent : "flex-end"
    },
    modal : {
        backgroundColor : "#fff",
        width : wp(90),
        borderRadius: hp(1),
        paddingHorizontal : wp(4),
        marginBottom : hp(1)
      },
      item: {
        width: wp(85),
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical : hp(2.2),
        borderBottomWidth : 0.4,
        borderBottomColor : 'lightgray'
    },
    secondItem: {
        borderBottomWidth: 0
    },
    itemTxt: {
        fontSize: hp(1.9),
        color: "#474744"
    }
});

export default styles;