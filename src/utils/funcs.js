import {
    Platform,
    Dimensions
} from "react-native";

export const isIos = () => getPlatform() === "ios";

export const isAndroid = () => getPlatform() === "android";

export const getPlatform = () => Platform.OS;

export const isLandscape = () => Dimensions.get("window").width > Dimensions.get("window").height;
