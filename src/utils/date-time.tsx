

export const getDatesWithRange = (startDate : string, endDate : string): string[] => {
  const dates = [];
  const currentDate = new Date(startDate);
  while (currentDate <= new Date(endDate)) {
    dates.push(currentDate.toISOString().slice(0, 10));
    currentDate.setDate(currentDate.getDate() + 1);
  }
  return dates;
};
