import React, { useState } from "react";
import { StatusBar } from "react-native";
import { SafeAreaProvider } from "react-native-safe-area-context";

const Context = React.createContext();

const Provider = (props) => {
    const { children } = props;

    const [
      auth, 
      setAuth
    ] = useState(false);
    const [
      token,
      setToken
    ] = useState('');
    const [
      userInfo,
      setUserInfo
    ] = useState();
    const [
      lang, 
      setLang
    ] = useState('en');

    const context = {
      auth,
      userInfo,
      token,
      lang,
      changeAuth : val => {
        setAuth(val);
      },
      changeUserInfo: val => {
        setUserInfo(val);
      },
      changeToken : val => {
        setToken(val);
      },
      changeLang: val => {
        setLang(val);
      },
      };

    return (
      <Context.Provider value={context}>
        <SafeAreaProvider>
          <StatusBar barStyle={'dark-content'} backgroundColor="#f2f2f2" />
            {children}
        </SafeAreaProvider>
      </Context.Provider>
    );
};

export {
  Provider, Context
}
  