const Colors = {
  white : "#fff",
  black : "#000",
  red : "#d0312d",
  gray : "#808080",
  light_gray : "#D3D3D3",
  orange : "#FF9800",

  // dark_yellow : "#ba761c",
  dark_yellow : "#FAA41A",

  // light_yellow : "#f2bf43",
  light_yellow : "#FDBF16",

  dark_green : "#36661a",

  // light_green : "#87ad2b",
  light_green : "#86C440",

  dark_orange : "#a05424",
  low_color : "#FDBF16", //dbc672
  medium_color : "#FAA41A", // d9875b

  dark_blue: "#203E78"
};

export default Colors;