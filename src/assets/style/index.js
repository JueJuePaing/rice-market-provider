import {
  StyleSheet,
  StatusBar
} from "react-native";
import {
  heightPercentageToDP as hp, 
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import {
  SPLASH_SCREEN_LOGO_SIZE,
  WINDOW_HEIGHT,
  WINDOW_WIDTH
} from "assets/dimensions";
import Colors from "../colors";

const DEFAULT_CONTAINER_PADDING = 12;

export const globalStyle = StyleSheet.create({
  full_screen : {
    flex : 1
  },
  full_screen_center : {
    flex : 1,
    alignItems : "center",
    justifyContent : "center"
  },
  horizontalRow : {
    flexDirection : "row"
  },
  center_all: {
    alignItems : "center",
    justifyContent : "center"
  },
  full_width : {
    width : "100%"
  },
  right : {
    alignItems : "flex-end"
  },
  margin : {
    margin : 10
  },
  margin_top : {
    marginTop : 10
  },
  padding : {
    padding : 10
  },
  white : {
    color : Colors.white
  },
  black : {
    color : Colors.black
  },
  gray : {
    color : Colors.gray
  },
  background_gray : {
    backgroundColor : Colors.gray
  },
  background_red : {
    backgroundColor : Colors.red,
    color : Colors.white
  }
});

export const MainAppContainer = StyleSheet.create({
  container : {
    flex : 1
  }
});

export const MainAppBody = StyleSheet.create({
  container : {
    flex : 1
  },
  contentContainer : {
    flex : 1
  }
});


export const SplashScreen = StyleSheet.create({
  container : {
    flex : 1,
    alignItems : "center",
    justifyContent : "center",
    paddingHorizontal : DEFAULT_CONTAINER_PADDING
  },
  version : {
    position : "absolute",
    bottom : 20
  },
  splash_logo : {
    width : SPLASH_SCREEN_LOGO_SIZE,
    height : SPLASH_SCREEN_LOGO_SIZE,
    resizeMode : "contain"
  },
  app_desc : {
    textAlign : "center",
    color : "gray"
  },
  versionTxt : {
    color : "gray"
  }
});

export const PackageScreen = StyleSheet.create({
  errorTxt : {
    fontSize : hp(1.8),
    paddingTop: hp(2)
  },
  selectWrapper : {
    alignSelf: 'center',
    // width : wp(48),
    borderRadius : hp(3),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    // backgroundColor : Colors.light_green,
    paddingHorizontal: 10,
    height: hp(5.5),
    borderWidth : 1,
    borderColor : 'lightgray',
    marginTop: hp(2),
    width: wp(92)
  },
  dateRangeWrapper : {
    width : wp(92),
    borderWidth : 1,
    borderColor : 'lightgray',
    alignSelf : 'center',
    paddingHorizontal : wp(5),
    paddingVertical : hp(1),
    borderRadius : hp(3),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    // backgroundColor : Colors.light_green,
    marginTop : hp(2)
  },
  selectTxt : {
    color: 'darkgray',
    fontSize: hp(1.5),
    textAlign: 'center',
    paddingBottom : hp(.4)
  },
  dateRangeTxt : {
    color: "#000",
    fontSize: hp(1.7)
  },
  dateRangeTxt2 : {
    color: "#000",
    fontSize: hp(1.7)
  },
  description : {
    color: "#2e2e2d",
    fontSize: hp(1.6),
    paddingLeft : wp(5),
    marginTop: hp(1)
  },
  button: {
    width: wp(92),
    alignSelf: 'center',
    // marginTop: hp(4),
    borderRadius : hp(3),
    position : 'absolute',
    bottom: hp(3)
  },
  container : {
    flex : 1,
    backgroundColor: '#fff'
  },
  content: {
    
  },
  errTxt: {
    alignSelf: 'flex-start',
    color: 'red',
    fontSize: hp(1.6),
    marginTop: -4,
    marginBottom: 5
  },
  content: {

  },
});

export const LoginScreen = StyleSheet.create({
  container : {
    flex : 1,
    alignItems : "center",
    justifyContent : "center",
    paddingHorizontal : 20,
    backgroundColor : "#FFFEFF",
    paddingBottom: hp(8)
  },
  errTxt: {
    alignSelf: 'flex-start',
    color: 'red',
    fontSize: hp(1.6),
    marginTop: -4,
    marginBottom: 5
  },
  logoContainer : {
    marginBottom : 0,
    // flexDirection : 'row',
    alignItems: 'center'
  },
  logo : {
    width : wp(80),
    height: hp(30),
    resizeMode : 'contain'
  },
  appname : {
    fontSize: hp(2.3),
    fontWeight: 'bold',
    marginBottom : hp(7),
    marginTop: hp(1),
    color: '#000'
  },
  formContainer : {
    width : "100%"
  },
  inputContainer : {
    flexDirection : "row",
    alignItems : "center",
    marginVertical : 10,
    borderWidth : 1,
    borderColor : "#ddd",
    borderRadius : 5
  },
  inputWrapper : {
    height : 40,
    flex : 1,
    borderWidth : 0,
    borderRadius : 5,
    overflow : "hidden"
    // paddingBottom: 20,
  },
  input : {
    height : 40,
    marginTop : 0,
    backgroundColor : "#FFFEFF",
    flex : 1,
    paddingHorizontal : 10,
    fontSize : hp(1.8),
    color: '#000'
  },
  inputIcon : {
    marginHorizontal : 10,
    color : "#4A4550"
  },
  checkboxContainer : {
    flexDirection : "row",
    alignItems : "center",
    justifyContent : "center",
    marginVertical : 10
  },
  checkbox : {
    backgroundColor : "transparent",
    borderColor : "transparent"
  },
  checkboxLabel : {
    marginLeft : 5,
    fontSize : 16
  },
  button : {
    // backgroundColor: '#DB3B26',
    marginTop : hp(2.5)
  },
  forgotPasswordBtn : {
    alignSelf : "flex-end",
    marginTop: -5
  },
  forgotPassword : {
    fontSize : hp(1.6),
    color : Colors.dark_yellow,
    fontWeight: 'bold'
  },
  errorTxt : {
    fontSize : 14
  },
  language : {
    position : 'absolute',
    top : hp(1),
    right : wp(4)
  },
  backBtn : {
    height : hp(4.3),
    width : hp(4.3),
    alignItems: 'center',
    justifyContent : 'center',
    borderWidth : 3,
    borderColor : 'rgba(171, 194, 118, 0.5)',
    borderRadius: hp(1.5),
    position : "absolute",
    top : hp(1),
    left : wp(4)
  },
  regRow: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop : hp(2)
  },
  regLabel: {
    color: 'darkgray',
    fontSize: hp(1.7),
    marginRight: wp(2)
  }
});

export const LoadingModal = StyleSheet.create({
  container : {
    backgroundColor : "rgba(0,0,0,0)",
    flex : 1,
    alignItems : "center",
    justifyContent : "center"
  }
  // modalBox : {
  //   width : 70,
  //   backgroundColor : "rgba(0,0,0,0.1)",
  //   borderRadius : 10,
  //   overflow : "hidden",
  //   alignItems : "center",
  //   justifyContent : "center",
  //   height : 70
  // }
});

export const WarehouseScreen = StyleSheet.create({
  container : {
    flex : 1,
    backgroundColor: '#f2f2f2'
  },
  list : {
    marginTop: hp(1),
    marginBottom: hp(1)
  },
  fabContent: {
    position: 'absolute',
    bottom: hp(2),
    right: 10
  },
  fabIcon : {
    backgroundColor: Colors.light_green,
    width: 56,
    height: 56,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export const LanguageModal = StyleSheet.create({
  upward : {
    right : wp(4.7),
    position : 'absolute',
    top : hp(4.7),
  },
  profileLanContainer: {
    backgroundColor : "rgba(0,0,0,0.6)",
    flex : 1,
    justifyContent : "center",
    alignItems : "center"
  },
  container : {
    backgroundColor : "#f7f7f7",
    width : wp(45),
    height: hp(12),
    right : wp(4),
    position : 'absolute',
    top : hp(6),
    borderRadius: hp(1),

    elevation : 1,
    shadowOffset : {width : 1, height : 1},
    shadowOpacity : 1,
    shadowRadius : 5,
    shadowColor: '#6b4e09'
  },
  lanItem : {
    flexDirection: 'row',
    alignItems: 'center',
    width : wp(45),
    paddingLeft : wp(4),
    height: hp(6)
  },
  profileLanItem : {
    flexDirection: 'row',
    alignItems: 'center',
    width : wp(80),
    paddingLeft : wp(5),
    height: hp(7),
    backgroundColor: '#f5f3ed'
  },
  enFlag : {
    borderTopLeftRadius: hp(1),
    borderTopRightRadius: hp(1),
    // paddingTop: hp(0)
  },
  mmFlag : {
    borderBottomLeftRadius: hp(1),
    borderBottomRightRadius: hp(1),
    // paddingTop: hp(1)
  },
  selected : {
    backgroundColor: '#f7e7be', // Colors.light_yellow
  },
  label: {
    paddingLeft: wp(3),
    fontSize: hp(1.6),
    color: '#000'
  }
});

export const Header = StyleSheet.create({
  headerContainer : {
    backgroundColor: '#f2f2f2',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: wp(100),
    paddingHorizontal: wp(3),
    height: hp(8)
  },
  logo: {
    height: hp(6),
    width: hp(13),
    resizeMode: 'contain',
    marginLeft: wp(-5)
  },
  rightContent: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerBtn: {
    marginLeft: wp(4)
  },
  notiBadge: {
    width: wp(2.2),
    height: wp(2.2),
    backgroundColor: Colors.dark_yellow,
    borderRadius: wp(2),
    position: 'absolute',
    right: wp(1),
    zIndex: 2
  },
  chatBadge: {
    width: wp(4),
    height: wp(4),
    backgroundColor: Colors.light_yellow,
    borderRadius: wp(3),
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: wp(-1),
    zIndex: 2,
    top: hp(-.5)
  },
  chatCount: {
    color: '#fff',
    fontSize: hp(1.2)
  },
  appname: {
    fontSize: hp(1.8),
    color: Colors.dark_yellow,
    paddingLeft: wp(1)
  },
  logoContent: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  selectBtn : {
    // marginLeft : wp(3),
    marginTop: hp(1),
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'center'
  },
  selectTxt : {
    marginRight: wp(1),
    fontSize: hp(1.8),
    color: '#000', //'#abc276'
    fontWeight: 'bold'
  }
});

export const ShopItem = StyleSheet.create({
  item: {
    width: wp(92),
    alignSelf: 'center',
    marginBottom: hp(1.5),
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: hp(1),
    backgroundColor: '#fff', // #f5ead0 lightYellow
    padding: 8
  },
  iconWrapper: {
    width: hp(7),
    height: hp(7),
    backgroundColor: '#b7cc83',
    borderRadius: hp(1),
    alignItems: 'center',
    justifyContent: 'center'
  },
  iconImg: {
    width: hp(7),
    height: hp(7),
    borderRadius: hp(1),
    resizeMode : 'cover'
  },
  nameContent: {
    marginLeft: wp(3)
  },
  name: {
    fontWeight: 'bold',
    fontSize: hp(1.75),
    color: '#000',
    maxWidth : wp(60)
  },
  address: {
    fontSize: hp(1.55),
    color: Colors.gray,
    paddingTop: hp(.6),
    width: wp(70)
  },
  row : {
    flexDirection: 'row',
    //width: hp(55),
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  typeBadge: {
    // backgroundColor: 'rgba(242, 192, 67, 0.9)',
    paddingLeft: wp(2),
    paddingVertical: hp(.3),
    borderRadius: hp(1),
    flexDirection: 'row',
    alignItems: 'center',
    paddingRight: wp(1.2),
    borderWidth: .2,
    borderColor: 'rgba(242, 192, 67, 0.9)',
    backgroundColor: '#faf7ed'
  },
  typeCount: {
    fontSize: hp(1.5),
    color: '#d6a609',
    paddingRight: wp(.7)
  }
});

export const DetailHeader = StyleSheet.create({
  headerContainer : {
    backgroundColor: '#f2f2f2',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: wp(100),
    paddingHorizontal: wp(3),
    height: hp(7)
  },
  backBtn : {
    height : hp(4.3),
    width : hp(4.3),
    alignItems: 'center',
    justifyContent : 'center',
    borderWidth : 3,
    borderColor : 'rgba(171, 194, 118, 0.5)',
    borderRadius: hp(1.5)
  },
  title : {
    fontSize : hp(2.1),
    color: '#000'
  },
  rightBtnRow: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  headerBtn: {
    marginRight: wp(2)
  },
  chatBadge: {
    width: wp(4),
    height: wp(4),
    backgroundColor: Colors.light_yellow,
    borderRadius: wp(3),
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: wp(-1),
    zIndex: 2,
    top: hp(-.5)
  },
  chatCount: {
    color: '#fff',
    fontSize: hp(1.2)
  },
  reviewBtn : {
    paddingHorizontal : wp(0.5),
    borderRadius: wp(10)
  },
  reviewImg : {
    width : wp(5),
    height : wp(5),
    resizeMode : 'contain'
  },
  userBtn: {
    marginRight: wp(2.5)
  }
});

export const RiceItem = StyleSheet.create({
  normalIcon: {
    width: wp(2),
    height: wp(2),
    borderRadius: wp(6),
    marginRight: wp(.5),
    marginTop: hp(.3)
  },
  item : {
    width : wp(94),
    alignSelf : 'center',
    borderRadius : hp(1.5),
    paddingTop : hp(2),
    backgroundColor: '#fff',
    marginTop : hp(1.5),
    paddingHorizontal: wp(3)
  },
  name : {
    fontSize: hp(1.8),
    fontWeight: 'bold',
    color: '#000',
    paddingBottom: hp(.7)
  },
  remark : {
    fontSize: hp(1.6),
    color: Colors.gray,
    width: wp(70)
  },
  remark2 : {
    fontSize: hp(1.5),
    color: Colors.gray,
    width: wp(70),
    marginTop : hp(.5)
  },
  divider: {
    width: wp(94),
    alignSelf: 'center',
    height: 1.3,
    backgroundColor: 'rgba(171, 194, 118, 0.3)',
    marginVertical: hp(2)
  },
  priceBlock : {
    alignItems: 'center',
    width: wp(30),
    borderRightWidth: .9,
    borderRightColor : 'rgba(171, 194, 118, 0.8)'
  },
  label : {
    fontSize : hp(1.5),
    color: "#fff"
  },
  value : {
    fontSize : hp(2),
    fontWeight: 'bold',
    paddingTop: hp(.4),
    color: "#fff"
  },
  ricebag : {
    width: wp(17),
    height: hp(8),
    resizeMode: 'contain',
    marginRight: wp(3)
  },
  titleRow : {
    flexDirection: 'row',
    alignItems: 'center',
   // justifyContent: 'center'
  },
  rightRow: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  editBtn: {
    marginTop: hp(-2)
  },
  nameRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  priceBlock2: {
    alignItems: 'center',
    width: wp(28.5),
    // backgroundColor: 'rgba(171, 194, 118, 0.5)',
    paddingVertical: hp(1.5),
    borderRadius: hp(1)
  },
  label2 : {
    fontSize : hp(1.5),
    color: "#fff"
  },
  value2 : {
    fontSize : hp(2),
    fontWeight: 'bold',
    paddingTop: hp(.4),
    color: "#fff"
  },


  priceContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp(90),
    marginBottom: hp(4),
  },
  priceContent2: {
    width: wp(94),
    marginLeft: wp(-3),
    marginTop: hp(1)
  },
  priceBlock3 : {
    justifyContent: 'center',
    width: wp(30),
    flexDirection:'row',

  },
  label3 : {
    // fontSize : hp(1.35),
    // color: "lightgray"
    fontSize : hp(1.45),
    color: "#a8aba7",
    fontWeight: '600'
  },
  value3: {
    fontSize : hp(1.9),
    fontWeight: 'bold',
    // paddingTop: hp(.6)
  },
  circleRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: hp(.7)
  },
  circle: {
    width: wp(2),
    height: wp(2),
    borderRadius: wp(3),
    backgroundColor: Colors.low_color, // '#dbc672',
    marginRight: wp(1.5)
  },
  date : {
    fontSize: hp(1.45),
    color: '#9aa19e',
    position: 'absolute',
    right: wp(3),
    top: wp(3.5)
  },
  priceDate: {
    fontSize: hp(1.4),
    color: '#9aa19e',
    position: 'absolute',
    right: wp(2),
    bottom: wp(2)
  }
});

export const DateRangePicker = StyleSheet.create({
  button : {
    width : wp(85),
    backgroundColor : "#FFFEFF",
    borderRadius : hp(1.5),
    marginBottom : 20,
    flexDirection : "row",
    paddingVertical : hp(1.2),
    justifyContent : "space-between",
    alignItems : "center",
    paddingHorizontal : wp(3),
    alignSelf: 'center',
    borderWidth : 2,
    borderColor : 'rgba(171, 194, 118, 0.5)',
  },
  buttonSelectText : {
    color : "#212022",
    fontWeight : "bold",
    fontSize : 12,
    paddingBottom : 7
  },
  buttonText : {
    color : Colors.dark_green,
    fontWeight : "bold",
    fontSize : 13
  },
  buttonContent : {
    alignItems : "center",
    justifyContent: 'center'
  },
  modal : {
    backgroundColor : "#fff",
    padding : 20,
    borderRadius : 10
  },
  modalButtons : {
    flexDirection : "row",
    justifyContent : "flex-end",
    marginTop : 10
  },
  selectedRange : {
    backgroundColor : "#fff",
    borderRadius : 5,
    padding : 10,
    flexDirection : "row",
    alignItems : "center",
    marginTop : 10,
    elevation : 3,
    shadowOffset : {width : 1, height : 1},
    shadowOpacity : 0.5,
    shadowRadius : 5
  },
  calendar : {
    marginBottom : 20
  },
  footer : {
    flexDirection : "row",
    justifyContent : "flex-end"
  },
  footerButton : {
    color : Colors.dark_orange,
    fontWeight : "bold",
    marginLeft : 20
  }
});

export const RiceChart = StyleSheet.create({

});

export const ReviewModal = StyleSheet.create({
  modalContainer : {
    backgroundColor : 'rgba(0,0,0,0.4)',
    flex : 1,
    alignItems : "center",
    justifyContent : "center"
  },
  modal : {
    backgroundColor : "#fff",
    width : wp(90),
    borderRadius: hp(1),
    paddingHorizontal : wp(5),
    paddingTop: hp(2.5),
    paddingBottom : hp(3.5)
  },
  closeBtn : {
    alignSelf: 'flex-end'
  },
  title : {
    fontSize: hp(2),
    fontWeight: 'bold',
    marginBottom : hp(1),
    color: '#000'
  },
  description : {
    fontSize: hp(1.7),
    marginBottom : hp(3),
    color: Colors.gray
  },
  ratingContent : {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp(75),
  },
  ratingBtn : {
    width: wp(14),
    height: wp(14),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: wp(12)
  },
  input : {
   // height : hp(30),
    marginTop : hp(2),
    // backgroundColor : "#FFFEFF",
    padding : 10,
    fontSize : hp(1.6),
    borderWidth : 1,
    borderColor : 'lightgray',
    color:'#000'
  },
  button: {
    marginTop: hp(2)
  }
});

export const DepotModal = StyleSheet.create({
  container : {
    backgroundColor : "rgba(0,0,0,0.6)",
    flex : 1,
    justifyContent : "center",
    alignItems : "center"
  },
  filterCloseIcon : {
    alignSelf : "center",
    marginBottom : 10
  },
  modalBox : {
    width : "94%",
    backgroundColor : "#fff",
    overflow : "hidden",
    borderRadius : 10,
    padding : 20,
    paddingTop : 15,
    alignItems : "center",
    maxHeight : hp(60)

  },
  sortbyName : { 
    paddingBottom : 25,
  },
  name : {
    color : "#6C6A6D",
    fontSize : hp(1.6),
    paddingTop : 15,
    paddingBottom : 25
  },
  item : {
    flexDirection : "row",
    alignItems : "center",
    justifyContent : "space-between",
    paddingHorizontal : 10,
    width : wp(85),
    paddingBottom : 20
  },
  sortName : {
    color : "#000",
    fontSize : hp(1.8)
  }
});


export const ChatItem = StyleSheet.create({
  item : {
    width: wp(95),
    // backgroundColor: '#fff',
     alignSelf: 'center',
     marginBottom : hp(1),
    paddingHorizontal : wp(2),
    paddingVertical: hp(1.5),
    borderRadius: hp(1.1),
    flexDirection: 'row',
    alignItems: 'center'
  },
  lastMessage : {
    fontSize : hp(1.6),
    color: 'darkgray',
    paddingTop: hp(.5)
  },
  name : {
    fontSize : hp(1.8),
    fontWeight: 'bold',
    color: '#000'
  },
  timestamp: {
    position : 'absolute',
    right : wp(3),
    top: hp(1.5),
    color: 'lightgray',
    fontSize: hp(1.3)
  },
  profileContent : {
    width: wp(13),
    height: wp(13),
    borderRadius: wp(20),
    backgroundColor: '#f5e5bf',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: wp(2)
  },
  profile: {
    width: wp(12),
    height: wp(12),
    resizeMode: 'contain',
    borderRadius: wp(20),
  },
  dot : {
    width: wp(2),
    height: wp(2),
    borderRadius: wp(2),
    backgroundColor: Colors.light_green,
    position : 'absolute',
    right : wp(3),
    top: hp(4.5),
  }
});

export const MessageText = StyleSheet.create({
  selfContent: {
    alignSelf: 'flex-end',
    marginRight: wp(3),
    alignItems: 'flex-end',
    marginBottom: hp(1)
  },
  userContent: {
    marginLeft: wp(3),
    marginBottom: hp(1),
    alignItems: 'flex-start'
  },
  item: {
    maxWidth : wp(76),
    borderTopLeftRadius: hp(3),
    borderTopRightRadius: hp(3),
    paddingHorizontal: wp(4),
   //  marginBottom: hp(.4),
    paddingVertical: hp(1.3)
  },
  selfItem: {
    backgroundColor: Colors.light_green,
    borderBottomLeftRadius: hp(3)
  },
  userItem: {
    backgroundColor: '#f2f2f2',
    borderBottomRightRadius: hp(3)
  },
  message : {
    fontSize: hp(1.7)
  },
  selfMessage: {
    color: '#fff'
  },
  userMessage: {
    color: '#000'
  },
  time : {
    fontSize: hp(1.2),
    color: '#a19c91',
    marginTop: hp(.4)
  },
  userTime: {
    marginLeft: wp(11)
  },
  profileContent : {
    width: wp(10),
    height: wp(10),
    borderRadius: wp(20),
    backgroundColor: '#f5e5bf',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: wp(1)
  },
  profile: {
    width: wp(9),
    height: wp(9),
    borderRadius: wp(20),
    resizeMode: 'contain'
  },
  userRow: {
    flexDirection: 'row',
    alignItems: 'flex-end'
  },
  dateContent: {
    paddingHorizontal: wp(3),
    paddingVertical: hp(.8),
    borderRadius: hp(1.8),
    backgroundColor:'#f5f2eb',
    alignSelf: 'center',
    marginBottom: hp(2)
  },
  date: {
    color: '#573e08',
    fontSize: hp(1.4),
    fontWeight: 'bold'
  },
  chatImg: {
    width: wp(65),
    height: hp(30),
  },
  selfChatImg: {
    borderTopLeftRadius: hp(3),
    borderTopRightRadius: hp(3),
    borderBottomLeftRadius: hp(3)
  },
  userChatImg: {
    borderTopLeftRadius: hp(3),
    borderTopRightRadius: hp(3),
    borderBottomRightRadius: hp(3)
  },
  videoContent: {
   flex: 1,
   backgroundColor: 'rgba(0,0,0,0.8)',
   alignItems: 'center',
   justifyContent: 'center',
   position : 'absolute',
   zIndex: 5,
   width: wp(100),
   height: hp(100)
  },
  video: {
    width: wp(100),
    height: hp(40),
    // position : 'absolute',
 
 
  },
  videoThumbnail: {
    width: wp(65),
    height: hp(30),
    borderTopLeftRadius: hp(3),
    borderTopRightRadius: hp(3),
    borderBottomLeftRadius: hp(3),
    overflow: 'hidden'
  },
  userVideo: {
    width: wp(65),
    height: hp(30),
    borderTopLeftRadius: hp(3),
    borderTopRightRadius: hp(3),
    borderBottomRightRadius: hp(3),
    backgroundColor: '#f2f2f2'
  },
  closeIcon : {
    position: 'absolute',
    top: hp(8)
  },
  playButton : {
    position: 'absolute'
  },
  slider: {
    width: wp(100),
  },
  thumbnailContent: {
    width: wp(65),
    height: hp(30),
    alignItems: 'center',
    justifyContent: 'center'
  }
});

export const MessageInput = StyleSheet.create({
  inputWrapper : {
    height: hp(6),
    width: wp(86),
    borderRadius: hp(5),

    flexDirection: 'row',
    paddingHorizontal: wp(2),
    alignItems: 'center',
    backgroundColor: '#f2f2f2'
  },
  input : {
    height : hp(5),
    width: wp(65), //67
    padding : 10,
    fontSize : hp(1.7),
    color:'#000'
   },
   imgButton : {
    width: wp(8)
   },
   container : {
    flexDirection: 'row',
    marginLeft: wp(2),
    marginVertical: hp(1),
    alignItems: 'center',
    justifyContent: 'space-between',
    marginRight: wp(3)
   },
   sendBtn: {
    alignItems: 'center',
    justifyContent: 'center'
   }
});

export const ChatInfoScreen = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2'
  },
  content: {
    marginTop: hp(1)
  },
  profileContent: {
    width: wp(20),
    height: wp(20),
    borderWidth: 1.5,
    borderRadius: wp(4),
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: 'rgba(160, 84, 36, 0.5)',
    alignSelf: 'center',
    marginBottom: hp(2)
  },
  profile: {
    width: wp(20),
    height: wp(20),
    borderRadius: wp(4),
    resizeMode: 'cover'
  },
  row : {
    marginHorizontal: wp(3),
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: hp(1.5)
  },
  value: {
    paddingLeft: wp(3),
    fontSize: hp(1.9),
    color: "#000"
  },
  mediaContent: {
    marginTop: hp(2)
  },
  tabRow : {
    flexDirection: 'row',
    marginLeft: wp(3),
    marginTop: hp(2),
    marginBottom: hp(4)
  },
  tab: {
    paddingVertical: hp(1.2),
    paddingHorizontal: wp(5),
    borderRadius: hp(3),
    borderWidth: 1,
    borderColor: Colors.light_green
  },
  selectedTab: {
    backgroundColor: Colors.light_green,
    borderWidth: 0
  },
  tabLabel: {
    fontSize: hp(1.8),
    color: Colors.gray
  },
  selectedTabLabel: {
    color: '#fff'
  },
  list: {
    flex: 1,
    width: wp(94),
    alignSelf: 'center'
  },
  mediaItem: {
    marginBottom: wp(2),
    width : wp(30),
    height : hp(13),
    alignItems: 'center',
    justifyContent: 'center',
    marginRight : wp(1),
    backgroundColor: 'rgba(57,57,57,0.8)',
    borderRadius : hp(.5),
  },
  mediaFile: {
    width : wp(30),
    height : hp(13),
    borderRadius : hp(.5),

  },
  friendName: {
    fontSize: hp(2),
    fontWeight: 'bold',
    color:'#000',
    marginBottom: hp(2),
    textAlign: 'center'
  },
  noData: {
    fontSize: hp(1.8),
    color: '#8b8f8b'
  },
  noDataContent: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop : hp(5)
  },
});

export const ProfileScreen = StyleSheet.create({
  container : {
    flex: 1,
    backgroundColor: '#f2f2f2'
  },
  editBtn: {
    position: 'absolute',
    bottom: hp(2.5),
    right: hp(2.5)
  },
  cornerContent: {
    paddingBottom: hp(2),
    borderBottomLeftRadius: hp(2),
    borderBottomRightRadius: hp(2),
    alignItems: 'center',
    paddingTop: hp(3),
    zIndex: 2,
    backgroundColor: '#fff',
    paddingBottom: hp(5)
  },
  profileContent: {
    width: wp(20),
    height: wp(20),
    borderRadius: wp(2), //wp(20)
    borderColor: '#cfbf99',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: hp(2),
    // borderWidth: 1
  },
  profile: {
    width: wp(18),
    height: wp(18),
    resizeMode: 'cover',
    borderRadius: wp(2),
  },
  name: {
    fontSize: hp(2),
    fontWeight: 'bold',
    color: '#000'
  },
  infoContent: {
    width: wp(90),
    height: hp(15),
    backgroundColor: '#fcfafa',
    borderRadius: hp(1),
    marginTop: hp(3),
    marginBottom: hp(-7),

    elevation: 3,
    shadowColor: '#000',
    shadowOpacity: 0.2,
    shadowRadius: 3,
    shadowOffset: {width: 0, height: 3}
  },
  menuContent: {
    flex: 1,
    backgroundColor: '#f2f2f2',
    zIndex: 1,
    alignItems: 'center',
    paddingTop: hp(2),
    paddingBottom: hp(2)
  },
  section: {

  },
  sectionTitle: {
    fontWeight: 'bold',
    fontSize: hp(1.7),
    color: '#000',
    paddingBottom: hp(1),
    paddingTop: hp(2)
  },
  logoutBtn: {
    alignSelf: 'center',
    marginVertical: hp(2),
    paddingHorizontal: wp(4),
    paddingVertical: hp(1),
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: .5,
    borderRadius: hp(1),
    backgroundColor: '#ba3a2f',
    shadowOffset: {width: 5, height: 10},
    shadowColor: 'lightgray',
    shadowOpacity: 0.5,
    shadowRadius: hp(1),
    elevation: 3
},
logout: {
    fontSize: hp(1.7),
    color: '#fff',
    paddingBottom: hp(.1),
    paddingLeft: wp(1.5)
},
});

export const ProfileMenu = StyleSheet.create({
  item : {
    width: wp(90),
    height: hp(6),
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderRadius: hp(.5),
    paddingHorizontal: wp(3),
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: hp(1.5)
  },
  name : {
    fontSize: hp(1.8),
    color: '#42413f'
  },

});


export const ChangePasswordScreen = StyleSheet.create({
  container : {
    flex : 1,
    backgroundColor : "#f2f2f2"
  },
  content: {
    paddingTop: hp(0),
    paddingHorizontal: 20
  },
  inputContainer : {
    flexDirection : "row",
    alignItems : "center",
    marginVertical : 7,
    borderWidth : 1,
    borderColor : "#ddd",
    borderRadius : 5,
    backgroundColor: '#fff'
  },
  inputWrapper : {
    height : 40,
    flex : 1,
    borderWidth : 0,
    borderRadius : 5,
    overflow : "hidden"
  },
  input : {
    height : 40,
    marginTop : 0,
    backgroundColor : "#FFFEFF",
    flex : 1,
    paddingHorizontal : 10,
    fontSize : hp(1.8),
    color: '#000'
  },
  inputIcon : {
    marginHorizontal : 10,
    color : "#4A4550"
  },
  button : {
    marginTop : hp(2.5)
  },
});

export const ForgetPasswordScreen = StyleSheet.create({
  container : {
    flex : 1,
    backgroundColor : "#f2f2f2"
  },
  content: {
    paddingTop: hp(0),
    paddingHorizontal: 20
  },
  inputContainer : {
    flexDirection : "row",
    alignItems : "center",
    marginVertical : 7,
    borderWidth : 1,
    borderColor : "#ddd",
    borderRadius : 5,
    backgroundColor: '#fff'
  },
  inputWrapper : {
    height : 40,
    flex : 1,
    borderWidth : 0,
    borderRadius : 5,
    overflow : "hidden"
  },
  input : {
    height : 40,
    marginTop : 0,
    backgroundColor : "#FFFEFF",
    flex : 1,
    paddingHorizontal : 10,
    fontSize : hp(1.8),
    color:'#000'
  },
  inputIcon : {
    marginHorizontal : 10,
    color : "#4A4550"
  },
  button : {
    marginTop : hp(2.5)
  },
});

export const ProfileDashboardScreen = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2'
  },
  selectWrapper : {
    width : wp(48),
    borderRadius : hp(3),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor : Colors.light_green,
    paddingHorizontal: wp(2),
    height: hp(5.5)
  },
  selectTxt : {
    color: '#dae3c5',
    fontSize: hp(1.5),
    textAlign: 'center',
    paddingBottom : hp(.4)
  },
  dateRangeTxt : {
    color: "#fff",
    fontSize: hp(1.6)
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: wp(1.5),
    marginTop : hp(1.5),
    marginBottom: hp(2)
  },
  legendRow : {
    flexDirection : 'row',
    alignItems : 'center',
    justifyContent : 'space-around',
    marginBottom : hp(4),
    marginTop : hp(5)
  },
  label : {
    fontSize : hp(1.5),
    color: "gray"
  },
  circleRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  circle: {
    width: wp(2),
    height: wp(2),
    borderRadius: wp(3),
    backgroundColor: '#dbc672',
    marginRight: wp(1.5)
  },
});

export const Rating = StyleSheet.create({
  ratingContent: {
    flexDirection : 'row',
    alignItems: 'center'
  },
  star: {
    marginRight: wp(1)
  }
});

export const RiceTypeConfigurationScreen = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2'
  },
  list: {
    marginBottom: hp(2),
    marginTop: hp(2)
  },
  hiddenRow: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    borderColor: '#DDD',
    marginRight: wp(3),
    height: hp(6)
  }
});

export const RiceTypeEditScreen = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2'
  },
  list: {
    marginTop: hp(2),
    marginBottom: hp(12)
  },
  button: {
    marginTop: hp(1),
    width: wp(94),
    alignSelf: 'center',
  },
  searchBar: {
    width: wp(94),
    backgroundColor: '#fff',
    height: hp(6.5),
    borderRadius: hp(1.5),
    alignSelf: 'center',
    textAlignVertical: 'center',
    elevation: 3,
    shadowColor: Colors.dark_green,
    shadowOpacity: 0.2,
    shadowRadius: 3,
    shadowOffset: {width: 1, height: 3}
  }
});

export const RiceType = StyleSheet.create({
  typeItem: {
    width: wp(94),
    backgroundColor: '#fff',
    borderRadius: hp(.6),
    paddingHorizontal: wp(4),
    // paddingVertical: hp(2),
    marginBottom: hp(1.2),
    alignSelf: 'center',
    height: hp(6),
    justifyContent: 'center'
  },
  riceName: {
    fontSize: hp(1.8),
    color: '#000'
  }
});

export const RiceTypeEdit = StyleSheet.create({
  typeItem: {
    width: wp(94),
    backgroundColor: '#fff',
    borderRadius: hp(.6),
    paddingHorizontal: wp(4),
    // paddingVertical: hp(1.7),
    height: hp(6),
    marginBottom: hp(1.2),
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  riceName: {
    fontSize: hp(1.8),
    color: '#000'
  },
  selectedItem: {
    backgroundColor: Colors.light_green
  },
  selectedRiceName: {
    color: '#fff'
  },
});


export const AddRiceTypeModal = StyleSheet.create({
  modalContainer : {
    backgroundColor : 'rgba(0,0,0,0.7)',
    flex : 1,
    alignItems : "center",
    justifyContent : "center"
  },
  modal : {
    backgroundColor : "#fff",
    width : wp(94),
    borderRadius: hp(1),
    paddingHorizontal : wp(4),
    paddingTop: hp(2.5),
    paddingBottom : hp(3.5)
  },
  closeBtn : {
    alignSelf: 'flex-end'
  },
  title : {
    fontSize: hp(1.8),
    fontWeight: 'bold',
    marginBottom : hp(1),
    color: '#000',
    textAlign: 'center'
  },
  input : {
    marginTop : hp(2),
    paddingHorizontal : 10,
    fontSize : hp(1.8),
    borderWidth : 1,
    borderColor : 'lightgray',
    borderRadius: hp(1),
    height: hp(6),
    color: '#000'
  },
  button: {
    marginTop: hp(2)
  }
});

export const RicePricesScreen = StyleSheet.create({
  container : {
    flex : 1,
    backgroundColor: '#f2f2f2'
  },
  list : {
    marginBottom: hp(2)
  },
  slideContainer: {
    justifyContent: 'center',
    marginRight: wp(5),
    alignSelf: 'flex-end',
  },
  dateRangeWrapper : {
    width : wp(80),
    alignSelf : 'center',
    paddingHorizontal : wp(5),
    paddingVertical : hp(1),
    borderRadius : hp(5),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor : Colors.light_green,
    marginTop : hp(1.5)
  },
  selectTxt : {
    color: '#dae3c5',
    fontSize: hp(1.5),
    textAlign: 'center',
    paddingBottom : hp(.4)
  },
  dateRangeTxt : {
    color: "#fff",
    fontSize: hp(1.7)
  }
});

export const EditRicePriceModal = StyleSheet.create({
  container : {
    flex : 1,
    backgroundColor: '#f2f2f2',
    marginHorizontal: wp(3)
  },
  modalContainer : {
    backgroundColor : 'rgba(0,0,0,0.7)',
    flex : 1,
    alignItems : "center",
    justifyContent : "center"
  },
  modal : {
    backgroundColor : "#fff",
    width : wp(94),
    borderRadius: hp(1),
    paddingHorizontal : wp(4),
    paddingTop: hp(2.5),
    paddingBottom : hp(3.5)
  },
  closeBtn : {
    alignSelf: 'center',
    marginBottom: hp(2)
  },
  title : {
    fontSize: hp(2),
    fontWeight: 'bold',
    marginBottom : hp(1),
    color: '#000',
    textAlign: 'center'
  },
  label: {
    paddingLeft: 2,
    fontSize: hp(1.6),
    color: '#30302e'
  },
  input : {
    paddingHorizontal : 10,
    fontSize : hp(1.8),
    borderWidth : 1,
    borderColor : 'lightgray',
    borderRadius: hp(1),
    marginVertical: hp(1),
    height: hp(5.5),
    color:'#000'
  },
  textAreaInput : {
    height: hp(8),
    color:'#000'
  },
  button: {
    marginTop: hp(2)
  },
  selectWrapper : {
    borderRadius : hp(1),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    height: hp(5.5),
    borderWidth : 1,
    borderColor : 'lightgray',
    marginTop: hp(2),
    marginBottom: hp(1)
  },
  errTxt: {
    alignSelf: 'flex-start',
    color: 'red',
    fontSize: hp(1.6),
    marginBottom: hp(1),
    marginTop: hp(-0.5)
  },
  filterList: {
    borderRadius : hp(1),
    width : wp(94),
    backgroundColor: '#fff',
    marginTop: hp(8.2),
    position: 'absolute',
    zIndex: 2,
    padding: hp(1.7),
    borderWidth : 1,
    borderColor : 'lightgray',
    maxHeight: hp(55),
    overflow: 'hidden'
  },
  riceName: {
    color: '#000',
    fontSize: hp(1.9)
  },
  dateRow: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  dateBtn: {
    width: wp(94),
    borderWidth: 1,
    // paddingVertical: hp(.9),
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: hp(1),
    paddingLeft: wp(3),
    borderWidth : 1,
    borderColor : 'lightgray',
    borderRadius: hp(1),
    marginVertical: hp(1),
    height: hp(5.5),
  },
  dateValue: {
    fontSize: hp(1.7),
    paddingLeft: wp(2),
    color: '#000'
  },
  datePicker: {
    backgroundColor: Colors.light_green,
    position: 'absolute',
    alignSelf: 'center'
  }
});

export const NewsScreen = StyleSheet.create({
  container : {
    flex : 1,
    backgroundColor: '#fff',
    // marginBottom: hp(10)
  },
  categoryContent: {
    marginHorizontal: wp(3),
    paddingBottom: hp(2)
  },
  tab: {
    paddingHorizontal: wp(5),
    borderRadius: hp(3),
    borderWidth: 1,
    borderColor: Colors.light_green,
    zIndex: 2,
    paddingVertical: hp(1.2)
  },
  selectedTab: {
    backgroundColor: Colors.light_green,
    borderWidth: 0
  },
  tabLabel: {
    fontSize: hp(1.8),
    color: Colors.gray
  },
  selectedTabLabel: {
    color: '#fff'
  },
  item: {
    width: wp(94),
    alignSelf: 'center',
    alignItems: 'center',
    height: hp(90),
},
newsImg: {
    width: wp(94),
    height: hp(25),
    borderRadius: hp(1),
    resizeMode: 'cover'
},
title: {
    fontWeight: 'bold',
    fontSize: hp(1.8),
    color: '#000',
    paddingBottom: hp(.7),
    paddingTop: hp(1),
    alignSelf: 'flex-start'
},
webView: {
    width: wp(94)
},
  list : {
    marginBottom : hp(2)
  }
});

export const Banner = StyleSheet.create({
  container: {
    width: wp(100),
    borderRadius: hp(.6),
    alignItems: 'center',
    marginTop: hp(1),
    alignSelf: 'center',
    // backgroundColor: 'red'
  },
  renderImage: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
    overflow: 'hidden'
  },
  paginationContainer: {
    position: 'absolute',
    bottom: 0,
    zIndex : 3
  },
  paginationDot: {
    width: 8,
    height: 8,
    borderRadius: 4,
    marginHorizontal: 0,
    backgroundColor: Colors.light_green //'rgba(255, 255, 255, 0.92)',
  },
  paginationInactiveDot: {
    width: 8,
    height: 8,
    borderRadius: 4,
    marginHorizontal: 0,
    backgroundColor: 'rgba(134, 196, 64, 0.5)'
  },
});

export const HomeShopItem = StyleSheet.create({
  item : {
    width: wp(42),
    height: hp(18),
    borderRadius: hp(1),
    marginRight: wp(2),
    backgroundColor: 'rgba(183, 204, 131, 0.1)'
  },
  iconWrapper: {
    width: wp(42),
    height: hp(10),
    backgroundColor: '#b7cc83',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopRightRadius: hp(1),
    borderTopLeftRadius: hp(1)
  },
  imgWrapper: {
    width: wp(42),
    height: hp(10),
    backgroundColor: '#b7cc83',
    overflow : 'hidden',
    borderTopRightRadius: hp(1),
    borderTopLeftRadius: hp(1),
  },
  iconImg: {
    width: wp(42),
    height: hp(10),
    resizeMode: 'cover'
  },
  nameContent : {
    paddingLeft: wp(2),
    height: hp(8),
    justifyContent: 'center'
  },
  name: {
    fontWeight: 'bold',
    fontSize: hp(1.75),
    color: '#000',
    paddingBottom: hp(.5)
  },
  starRow : {
    flexDirection: 'row'
  }
});

export const AdsSlider = StyleSheet.create({
  container : {
    flexGrow : 1,
    // justifyContent : "center",
    marginTop : hp(2)
  },
  item : {
    flexDirection : "row",
    alignItems : "center",
    height : hp(10),
    // marginLeft : 5,
    backgroundColor : "#FFFFFF",
    borderRadius : 10,
    elevation : 5,
    marginVertical : 10,
    paddingHorizontal : 10,
    shadowColor : "#000000",
    shadowOffset : {
      width : 0,
      height : 2
    },
    shadowOpacity : 0.25,
    shadowRadius : 3.84,
    justifyContent: 'space-between',

  },
  bannerImg : {
    width: wp(20),
    height: hp(9),
    borderRadius : 5
  },
  centerItem : {
    flexDirection : "row",
    alignItems : "center",
    height : hp(11),
    marginHorizontal : 5,
    backgroundColor : "#FFFFFF",
    borderRadius : 10,
    elevation : 5,
    marginVertical : 5,
    // marginLeft : 10,
    paddingHorizontal : 10,
    shadowColor : "#000000",
    shadowOffset : {
      width : 0,
      height : 2
    },
    shadowOpacity : 0.25,
    shadowRadius : 3.84,
    justifyContent: 'space-between'
  },
  text : {
    color : "#000",
    fontSize: hp(1.8),
    textAlign : "center"
  },
  price : {
    color : "#F79944",
    textAlign : "center",
    fontWeight : "bold"
  },
  textWrapper : { 
    flex : 1,
    marginRight: wp(1),
    marginLeft :wp(2)
  },
  carbonContainer : { alignItems : "center" },
  btn: {
    paddingVertical: hp(.3),
    borderRadius: hp(1),
    backgroundColor : 'rgba(181, 163, 139, 0.5)',
    width: wp(25),
    alignItems: 'center',
    // position: 'absolute',
    // bottom: 0,
    // right: 0
    alignSelf: 'flex-end'
  },
  btnText: {
    color : Colors.dark_orange,
    fontSize: hp(1.4)
  }
});

export const FilterModal = StyleSheet.create({
  modalContainer : {
    backgroundColor : 'rgba(0,0,0,0.7)',
    flex : 1,
    alignItems : "center",
    justifyContent : "center"
  },
  modal : {
    backgroundColor : "#fff",
    width : wp(96),
    borderRadius: hp(.5),
    paddingHorizontal : wp(4),
    paddingTop: hp(2.),
    paddingBottom : hp(3.5)
  },
  closeBtn : {
    alignSelf: 'center',
    marginBottom: hp(1.5)
  },
  title : {
    fontSize: hp(1.8),
    fontWeight: 'bold',
    marginBottom : hp(1),
    color: '#000',
    textAlign: 'center'
  },
  input : {
    marginTop : hp(2),
    paddingHorizontal : 10,
    fontSize : hp(1.8),
    borderWidth : 1,
    borderColor : 'lightgray',
    borderRadius: hp(1),
    height: hp(5.5),
    color:'#000'
  },
  button: {
    marginTop: hp(2),
    width: wp(49),
    borderRadius: hp(3)
  },
  selectWrapper : {
    // width : wp(48),
    borderRadius : hp(1),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    // backgroundColor : Colors.light_green,
    paddingHorizontal: 10,
    height: hp(5.5),
    borderWidth : 1,
    borderColor : 'lightgray',
    marginTop: hp(2)
  },
  selectTxt : {
    color: '#dae3c5',
    fontSize: hp(1.5),
    textAlign: 'center',
    paddingBottom : hp(.4)
  },
  footerRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: "space-between"
  }
});

export const PriceRise = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: wp(1.5)
  },
  circle: {
    width: wp(.86),
    height: wp(.86),
    borderRadius: wp(4),
    backgroundColor: '#f27f72'
  },
  circle2: {
    width: wp(.6),
    height: wp(.6),
    borderRadius: wp(4),
    backgroundColor: '#f27f72', // f05c4d
    marginTop: 1,// hp(.3)
  }
});


export const PriceLow = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: wp(1.5)
  },
  circle: {
    width: wp(.86),
    height: wp(.86),
    borderRadius: wp(4),
    backgroundColor: '#a5cc78'
  },
  circle2: {
    width: wp(.6),
    height: wp(.6),
    borderRadius: wp(4),
    backgroundColor: '#a5cc78', //Colors.light_green,
    marginBottom: 1
  }
});

export const DepotFilterModal = StyleSheet.create({
  modalContainer : {
    backgroundColor : 'rgba(0,0,0,0.8)',
    flex : 1,
    alignItems : "center",
    justifyContent : "center"
  },
  modal : {
    backgroundColor : "#fff",
    width : wp(96),
    borderRadius: hp(.5),
    paddingHorizontal : wp(4),
    paddingTop: hp(2.),
    paddingBottom : hp(3.5)
  },
  closeBtn : {
    alignSelf: 'center',
    marginBottom: hp(1.5)
  },
  title : {
    fontSize: hp(1.8),
    fontWeight: 'bold',
    marginBottom : hp(1),
    color: '#000',
    textAlign: 'center'
  },
  input : {
    marginTop : hp(2),
    paddingHorizontal : 10,
    fontSize : hp(1.8),
    borderWidth : 1,
    borderColor : 'lightgray',
    borderRadius: hp(1),
    height: hp(5.5),
    color:'#000'
  },
  button: {
    marginTop: hp(2),
    // width: wp(49),
    borderRadius: hp(3)
  },
  selectWrapper : {
    // width : wp(48),
    borderRadius : hp(1),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    // backgroundColor : Colors.light_green,
    paddingHorizontal: 10,
    height: hp(5.5),
    borderWidth : 1,
    borderColor : 'lightgray',
    marginTop: hp(2.5)
  },
  selectTxt : {
    color: '#dae3c5',
    fontSize: hp(1.5),
    textAlign: 'center',
    paddingBottom : hp(.4)
  },
  footerRow: {
    // flexDirection: 'row',
    // alignItems: 'center',
    // justifyContent: "space-between"
  },
  filterList: {
    borderRadius : hp(1),
    width : wp(88),
    backgroundColor: '#fff',
    marginTop: hp(8.2),
    position: 'absolute',
    zIndex: 2,
    padding: hp(1.7),
    borderWidth : 1,
    borderColor : 'lightgray',
  },

  stateFilterList: {

  },
  tsFilterList: {
    maxHeight: hp(30),
    overflow: 'hidden',
    borderRadius : hp(1),
    width : wp(88),
    backgroundColor: '#fff',
    padding: hp(1.7),
    borderWidth : 1,
    borderColor : 'lightgray'
  },
  commodityFilterList: {

  },
  name: {
    color: '#000',
    fontSize: hp(1.9)
  },
  noData: {
    textAlign: 'center',
    paddingVertical: hp(.6)
  },
  selectLabel: {
    color: '#2b2b29',
    fontSize: hp(1.9)
  },
  errTxt: {
    alignSelf: 'flex-start',
    color: 'red',
    fontSize: hp(1.6),
    marginTop: hp(1)
  },
});

export const NoPackage = StyleSheet.create({
  button: {
    marginTop: hp(3),
    width: wp(40),
    borderRadius: hp(3),
    alignSelf: 'center'
  },
  noData: {
    alignSelf: 'center',
    fontSize: hp(1.9),
    color: '#000',
    paddingHorizontal: wp(3)
  },
});

export const DateTimePicker = StyleSheet.create({
  modal : {
    backgroundColor : "#fff",
    padding : 20,
    borderRadius : 10
  },
  footer : {
    flexDirection : "row",
    justifyContent : "flex-end",
    marginTop: hp(2)
  },
  footerButton : {
    color : Colors.dark_orange,
    fontWeight : "bold",
    marginLeft : 20
  }
})

export const DatePickerButton = StyleSheet.create({
  dateRangeWrapper : {
    width : wp(80),
    alignSelf : 'center',
    paddingHorizontal : wp(5),
    paddingVertical : hp(1),
    borderRadius : hp(5),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor : Colors.light_green,
    marginVertical : hp(.5)
  },
  selectTxt : {
    color: '#dae3c5',
    fontSize: hp(1.5),
    textAlign: 'center',
    paddingBottom : hp(.4)
  },
  dateRangeTxt : {
    color: "#fff",
    fontSize: hp(1.7)
  }
});