import React from "react";
import Svg, {Path} from "react-native-svg";

const RiceWarehouse = ({color, size=24}) => {
  return <Svg
    width={size}
    height={size}
    viewBox="0 0 256 230">
    <Path fill={color} d="M61.2 106h37.4v31.2H61.2V106zm0 72.7h37.4v-31.2H61.2v31.2zm0 41.4h37.4v-31.2H61.2v31.2zm48.5-41.4H147v-31.2h-37.4v31.2zm0 41.4H147v-31.2h-37.4v31.2zm48.5-31.2v31.2h37.4v-31.2h-37.4zM255 67.2 128.3 7.6 1.7 67.4l7.9 16.5 16.1-7.7v144h18.2V75.6h169v144.8h18.2v-144l16.1 7.5 7.8-16.7z" />
  </Svg>
};


export default RiceWarehouse;
