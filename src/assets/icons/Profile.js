import React from "react";
import Svg, {Path, Circle} from "react-native-svg";

const Profile = ({color}) => {
  return <Svg
    width={24}
    height={24}
    viewBox="0 0 64 64"
    fill="none"
    stroke="#000"
    strokeWidth={3}
  >
    <Circle stroke={color} cx={32} cy={18.14} r={11.14} />
    <Path 
      stroke={color}
      d="M54.55 56.85A22.55 22.55 0 0 0 32 34.3 22.55 22.55 0 0 0 9.45 56.85Z" />
  
  </Svg>
};

export default Profile;
