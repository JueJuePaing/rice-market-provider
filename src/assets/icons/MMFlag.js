import React from "react";
import Svg, {Path, G, Rect, ClipPath, Mask, Defs} from "react-native-svg";

const MMFlag = () => {
  return <Svg
    width={30}
    height={30}
    fill="none"
    viewBox="0 -4 28 28"
  >
 <G ClipPath="url(#a)">
      <Rect width={28} height={20} fill="#fff" rx={2} />
      <Mask
        id="b"
        width={28}
        height={20}
        x={0}
        y={0}
        maskUnits="userSpaceOnUse"
        style={{
          maskType: "alpha",
        }}
      >
        <Rect width={28} height={20} fill="#fff" rx={2} />
      </Mask>
      <G fillRule="evenodd" clipRule="evenodd" mask="url(#b)">
        <Path fill="#F13D4F" d="M0 20h28v-6.667H0V20Z" />
        <Path fill="#4AC94B" d="M0 13.333h28V6.667H0v6.666Z" />
        <Path fill="#FFD043" d="M0 6.667h28V0H0v6.667Z" />
        <Path
          fill="#fff"
          d="m14 12.34-3.527 2.514 1.301-4.13-3.48-2.578 4.33-.04L14 4l1.375 4.107 4.331.039-3.48 2.577 1.3 4.131L14 12.34Z"
        />
      </G>
    </G>
    <Defs>
      <ClipPath id="a">
        <Rect width={28} height={20} fill="#fff" rx={2} />
      </ClipPath>
    </Defs>
  </Svg>
};

export default MMFlag;
