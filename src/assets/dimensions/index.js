import {
  Dimensions
} from "react-native";

export const WINDOW_WIDTH = Dimensions.get("window").width;
export const WINDOW_HEIGHT = Dimensions.get("window").height;

export const SPLASH_SCREEN_LOGO_SIZE = WINDOW_WIDTH * 0.4;
