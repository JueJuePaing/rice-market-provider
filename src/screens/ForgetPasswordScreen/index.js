import React, {
  useState, 
  memo, 
  useRef,
  useEffect,
  useContext
} from "react";
import {
  View,
  TextInput, 
  Image, 
  TouchableOpacity,
  ToastAndroid
} from "react-native";
import {
  Button,
  HelperText,
  DefaultTheme
} from "react-native-paper";
import Icon from "react-native-vector-icons/MaterialIcons";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import Ionicons from "react-native-vector-icons/Ionicons";

import {
  LoginScreen as styles
} from "assets/style";
import apiUrl from "utils/apiUrl";
import { fetchPost } from "utils/fetchData";
import { setItem } from "utils/appStorage";
import { Context } from "context/Provider";
import { useLocal } from "hook/useLocal";

import Loading from "components/Loading";
import Language from "components/Language";

const ForgetPasswordScreen = ({ navigation }) => {

  const local = useLocal();

  const {
    lang,
    changeLang
  } = useContext(Context);

  const [
    phone,
    setPhone
  ] = useState("");
  const [
    loading, 
    setLoading
  ] = useState(false);
  const [
    error, 
    setError
  ] = useState("");
  const [
    lanModalOpened,
    setLanModalOpened
  ] = useState(false);

  const _isMounted = useRef(false);

  const theme = {
    ...DefaultTheme,
    colors : {
      ...DefaultTheme.colors,
      primary : "#87ad2b"
    }
  };

  useEffect(() => {
    _isMounted.current = true;

    return function clean() {
      _isMounted.current = false;
    };
  }, []);

  const handlePhoneChange = (phno) => {
    setError("");
    setPhone(phno);
  };

  const handleSubmit = async () => {
    if (!phone) {
      setError(local.invalidPhoneNumber);
      return;
    }

    setLoading(true);
 
    const data = {
      phone_number: phone
    };

    const response = await fetchPost(apiUrl.sendOTP, data);

    setLoading(false);
    if (response?.success) {
      navigation.navigate('OTP', {phone, forgetPassword: true, send_otp : response.send_otp})
      ToastAndroid.show(response.data?.message ?? local.successful, ToastAndroid.SHORT)
    } else {
      setError(response?.data?.message ? response.data.message : response?.message ?? local.fail);
    }
  };

  const languageHandler = () => {
    setLanModalOpened(true);
  };

  const chooseLan = (lan) => {
    changeLang(lan);
    setItem('@lang', lan);
    setLanModalOpened(false);
  }

  return (
    <View style={styles.container}>
       <TouchableOpacity
          style={styles.backBtn}
          activeOpacity={0.8}
          onPress={()=> navigation.goBack()}>
          <Ionicons
              name="chevron-back-outline"
              size={hp(2.3)}
              color={"#abc276"} />
      </TouchableOpacity>
      <TouchableOpacity 
        style={styles.language}
        activeOpacity={0.8}
        onPress={languageHandler}>
        <SimpleLineIcons
          size={hp(3)}
          color="#000"
          name="globe"
           />
      </TouchableOpacity>
      <View style={styles.logoContainer}>
        <Image
          source={require("assets/images/app_logo.jpeg")}
          style={styles.logo} />
      </View>
      <View style={styles.formContainer}>
        <View style={styles.inputContainer}>
          <Icon name="phone" size={hp(2.2)} style={styles.inputIcon} />
          <View style={styles.inputWrapper}>
            <TextInput
              placeholder={local.phoneNumber}
              value={phone}
              onChangeText={handlePhoneChange}
              keyboardType="number-pad"
              autoCapitalize="none"
              style={styles.input}
              underlineColor="transparent"/>
          </View>
        </View>
        {!!error && (
          <HelperText type="error" style={styles.errorTxt}>
            {error}
          </HelperText>
        )}
        <Button
          mode="contained"
          theme={theme}
          onPress={handleSubmit}
          style={styles.button}
          labelStyle={{fontSize: hp(1.8)}}
          disabled={loading}>
          {local.submit}
        </Button>
        
      </View>
      <Loading visible={loading} />
      <Language 
        visible={lanModalOpened}
        lan={ lang }
        languageHandler={chooseLan}
        onClose={()=> setLanModalOpened(false)} />
    </View>)
};

export default memo(ForgetPasswordScreen);
