import React, {
  memo,
  useContext,
  useState,
  lazy,
  useEffect,
  useCallback
} from "react";
import {
  View,
  Text,
  Image,
  StatusBar,
  ScrollView,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  BackHandler
} from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import AntDesign from 'react-native-vector-icons/AntDesign'
import Feather from 'react-native-vector-icons/Feather';
import { useNavigation } from '@react-navigation/native';

import { setItem } from "utils/appStorage";
import {
  ProfileScreen as styles
} from "assets/style";
import apiUrl from "utils/apiUrl";
import { fetchPostByToken } from "utils/fetchData";
import Colors from 'assets/colors';
import { Context } from "context/Provider";
import { clearStorage } from "utils/appStorage";
import {useLocal} from '/hook/useLocal';

const MainApplicationContainer = lazy(() => import("components/MainApplicationContainer"));
const MainApplicationBody = lazy(() => import("components/MainApplicationBody"));
import ProfileMenu from "components/Profile/ProfileMenu";
import ProfileLanguage from "components/Language/ProfileLanguage";

const ProfileScreen = ({ navigation }) => {

  const {
    lang,
    token,
    changeToken,
    changeAuth,
    userInfo,
    changeLang
  } = useContext(Context);

  const local = useLocal();
  const stackNavigation = useNavigation();
  const parsedInfo = userInfo ? JSON.parse(userInfo) : null;

  const PROFILE_MENUS = [
  {
      id: 1,
      title: local.general,
      menus: [
          {
              id: 1,
              name: local.changeLanguage
          },
          {
              id: 2,
              name: local.changePassword
          }
      ]
  },
  {
      id: 2,
      title: local.detail,
      menus: [
          {
              id: 4,
              name: local.dashboard,
          },
          {
              id: 5,
              name: local.reviews
          },
          {
            id: 8,
            name: local.buyPackage
          },
          {
            id: 10,
            name: local.packageHistory
          }
      ]
  },
]

  if (parsedInfo.user_type_id !== 3) {
    PROFILE_MENUS[1].menus.push( 
      {
          id: 6,
          name: local.riceTypeConfiguration
      })
    PROFILE_MENUS[1].menus.push( 
        {
            id: 7,
            name: local.ricePrices
        })
  }
  PROFILE_MENUS[1].menus.push( 
    {
        id: 9,
        name: local.termsAndConditions
    })

  const [
    lanModalOpened,
    setLanModalOpened
  ] = useState(false);

  const backButtonHandler = useCallback(() => {
    stackNavigation.navigate('HomeStack', {
      screen : 'HomeScreen'
    });
    return true;
  }, []);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, [backButtonHandler]);

  const menuHandler = (item) => {
    if (item.id === 1) {
      setLanModalOpened(true);
    } else if (item.id === 2) {
      navigation.navigate('ChangePassword', {token, forgetPassword: false});
    } else if (item.id === 3) {
      navigation.navigate("ForgetPassword");
    } else if (item.id === 4) {
      navigation.navigate("ProfileDashboard");
    } else if (item.id === 5) {
      navigation.navigate('Reviews', {user: parsedInfo});
    } else if (item.id === 6) {
      navigation.navigate("RiceTypeConfiguration");
    } else if (item.id === 7) {
      navigation.navigate("RicePrice");
    } else if (item.id === 8) {
      navigation.navigate("Package");
    } else if (item.id === 9) {
      navigation.navigate('TermsAndConditions')
    } else if (item.id === 10) {
      navigation.navigate('PackageHistory')
    }
  }

  const chooseLan = (lan) => {
    changeLang(lan);
    setItem('@lang', lan);
    setLanModalOpened(false);
  }

  const showConfirmAlert = () => {
    Alert.alert(
      local.confirmation,
      local.logoutConfirm,
      [
        { text: local.cancel, onPress: console.log("do nothing"), style: 'cancel' },
        { text: local.confirm, onPress: onLogout },
      ],
      { cancelable: false } // Prevent dismissing the alert by tapping outside or pressing the back button
    );
  };

  const onLogout = async () => {

    const response = await fetchPostByToken(
      apiUrl.logout,
      {},
      token
    );

    if (response?.success) {
      await clearStorage();

      changeToken()
      changeAuth(false);
      navigation.replace("Login");
    } else {
      ToastAndroid.show(local.cannotLogout, ToastAndroid.SHORT)
    }
    
  }
  return <MainApplicationContainer>
    <MainApplicationBody>
      <StatusBar backgroundColor='#fff' barStyle='dark-content' />
      <ScrollView showsVerticalScrollIndicator={false} style={ styles.container }>
        <View style={styles.cornerContent}>
          <View style={styles.profileContent}>
            <Image
              style={styles.profile}
              source={parsedInfo ? { uri : parsedInfo.image } : require('assets/images/rice1.png')}
               />
          </View>
          <Text style={styles.name}>​​
            {parsedInfo?.name}
          </Text>
          <TouchableOpacity
            style={styles.editBtn}
            onPress={()=> navigation.navigate('UpdateProfile', {userData: parsedInfo})}>
            <Feather
              name='edit'
              color={Colors.light_green}
              size={hp(2.4)} />
          </TouchableOpacity>
        </View>
        <View style={styles.menuContent}>
          {
            PROFILE_MENUS.map((item) => {
              return <View style={styles.section}>
              <Text style={styles.sectionTitle}>{item.title}</Text>
                {item.menus.map((menu) => {
                  return <ProfileMenu
                    item={menu}
                    itemHandler={()=> menuHandler(menu)} />
                })}
              </View>
            })
          }

          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.logoutBtn}
            onPress={ showConfirmAlert }>
            <AntDesign 
                name="logout"
                size={hp(2)}
                color='#fff' />
            <Text style={styles.logout}>
              {local.logout}
            </Text>
        </TouchableOpacity>
        
        </View>
        <ProfileLanguage 
          visible={lanModalOpened}
          lan={ lang }
          languageHandler={chooseLan}
          onClose={()=> setLanModalOpened(false)} />
      </ScrollView>
    </MainApplicationBody>
  </MainApplicationContainer>;
};

export default memo(ProfileScreen);
