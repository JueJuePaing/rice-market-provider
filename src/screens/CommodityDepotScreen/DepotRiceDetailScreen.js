import React, {
  memo,
  useEffect,
  useContext,
  useState,
  useCallback
} from "react";
import {
  View,
  TouchableOpacity,
  Text,
  ToastAndroid,
  BackHandler
} from "react-native";
import moment from "moment-timezone";
import Ionicons from "react-native-vector-icons/Ionicons";
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { Context } from "context/Provider";
import { getDatesWithRange } from "utils/date-time";
import Colors from "assets/colors";
import apiUrl from "utils/apiUrl";
import { fetchPostByToken } from "utils/fetchData";
import {useLocal} from '../../hook/useLocal';
import styles from "./Style";

import DatePickerButton from "components/DatePickerButton";
import RiceChart from "components/Charts/RiceChart";
import DateRangePicker from "components/DatePicker";
import Header from "components/Header/DetailHeader";
import MainApplicationBody from "components/MainApplicationBody";
import MainApplicationContainer from "components/MainApplicationContainer";
import Loading from "components/Loading";

const WarehouseRiceDetailScreen = ({ navigation, route }) => {
  const local = useLocal();

  const {item, title} = route.params;

  const {
    token
  } = useContext(Context);

  const [
    dateLabels,
    setDateLabels
  ] = useState([]);
  const [
    startDate,
    setStartDate
  ] = useState("");
  const [
    endDate,
    setEndDate
  ] = useState("");
  const [
    datePicker,
    setDatePicker
  ] = useState(false);
  const [
    lowChecked,
    setLowChecked
  ] = useState(true);
  const [
    mediumChecked,
    setMediumChecked
  ] = useState(true);
  const [
    highChecked,
    setHighChecked
  ] = useState(true);
  const [
    loading,
    setLoading
  ] = useState(true);
  const [
    lowData,
    setLowData
  ] = useState()
  const [
    mediumData,
    setMediumData
  ] = useState()
  const [
    highData,
    setHighData
  ] = useState()

  const backButtonHandler = useCallback(() => {
    navigation.goBack();
    return true;
  }, []);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, [backButtonHandler]);

  useEffect(() => {
    const startdate = new Date();
    startdate.setDate(startdate.getDate() - 6);

    const end = new Date();
    end.setDate(end.getDate());

    const year = startdate.getFullYear();
    const month = String(startdate.getMonth() + 1).padStart(2, "0");
    const day = String(startdate.getDate()).padStart(2, "0");
    const formattedStartdate = `${year}-${month}-${day}`;

    const endYear = end.getFullYear();
    const endMonth = String(end.getMonth() + 1).padStart(2, "0");
    const endDay = String(end.getDate()).padStart(2, "0");
    const formattedEnddate = `${endYear}-${endMonth}-${endDay}`;

    setStartDate(formattedStartdate);
    setEndDate(formattedEnddate);
  }, []);

  useEffect(() => {
    if (!!startDate && !!endDate) {
      const dateArr = getDatesWithRange(startDate, endDate);
      const labels = [];
      for (let i = 0; i < dateArr.length; i++) {
        // just for label
        const dateLabel = moment(dateArr[i])
          .format("DD/MM/YYYY");
        labels.push(dateLabel);
      }

      setDateLabels(labels);
    }
  }, [startDate, endDate]);

  useEffect(() => {
    if (item && startDate && endDate) {
      getDetailData()
    }
  }, [item, startDate, endDate])

  const getDetailData = async () => {
    setLoading(true)
    const data = {
      start_date : startDate,
      end_date : endDate,
      rice_type_id : item.rice_type_id
    };

    const response = await fetchPostByToken(
      apiUrl.stationPriceDetailGraph,
      data,
      token
    );

    setLoading(false);
    if (response?.success) {
      if (response.data?.low) {
        setLowData(response.data.low)
      }
      if (response.data?.medium) {
        setMediumData(response.data.medium)
      }
      if (response.data?.high) {
        setHighData(response.data.high)
      }
    } else {
      
      ToastAndroid.show(response?.message ?? local.fail, ToastAndroid.SHORT)
    }
  }

  const backHandler = () => {
    navigation.goBack();
  }

  const onSubmitDate = (start, end) => {
    setDatePicker(false);
    setStartDate(start);
    setEndDate(end);
  }

return <MainApplicationContainer>
    <MainApplicationBody>
      <View
        style={ styles.detailContainer }>
        <Header 
          title={title}
          // review={true}
          // reviewHandler={reviewHandler}
          backHandler={backHandler} />

        <DatePickerButton
          // width={ wp(94) }
          startDate={ startDate }
          endDate={ endDate }
          pressHandler={() => setDatePicker(true)} />

        <View style={styles.legendRow}>
          <View style={styles.circleRow}>
            {/* <View style={styles.circle} /> */}
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.checkbox}
              onPress={()=> setLowChecked(prev => !prev)}>
              <Ionicons
                name={lowChecked ? 'md-checkbox' : 'md-checkbox-outline'}
                size={hp(2.5)}
                color={'#dbc672'} />
            </TouchableOpacity>
            <Text style={styles.label}>{local.low}</Text>
          </View>
          <View style={styles.circleRow}>
            {/* <View style={[styles.circle, {backgroundColor: '#d9875b'}]} /> */}
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.checkbox}
              onPress={()=> setMediumChecked(prev => !prev)}>
              <Ionicons
                name={mediumChecked ? 'md-checkbox' : 'md-checkbox-outline'}
                size={hp(2.5)}
                color={'#d9875b'} />
            </TouchableOpacity>
            <Text style={styles.label}>{local.medium}</Text>
          </View>
          <View style={styles.circleRow}>
            {/* <View style={[styles.circle, {backgroundColor: Colors.light_green}]} /> */}
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.checkbox}
              onPress={()=> setHighChecked(prev => !prev)}>
              <Ionicons
                name={highChecked ? 'md-checkbox' : 'md-checkbox-outline'}
                size={hp(2.5)}
                color={Colors.light_green} />
            </TouchableOpacity>
            <Text style={styles.label}>{local.high}</Text>
            </View>
        </View>

        {(!loading || lowData) && <RiceChart 
          dateLabels={dateLabels}
          low={lowChecked}
          medium={mediumChecked}
          high={highChecked} 
          lowData={lowData}
          mediumData={mediumData}
          highData={highData}  /> }
  
        <DateRangePicker 
          visible={datePicker}
          onClose={()=> setDatePicker(false)}
          onSubmit={onSubmitDate} />

        <Loading visible={loading} />
      
      </View>
    </MainApplicationBody>
  </MainApplicationContainer>;
};

export default memo(WarehouseRiceDetailScreen);
