import React, {
  memo,
  useContext,
  useState,
  useEffect,
  useCallback
} from "react";
import {
  View,
  FlatList,
  ToastAndroid,
  StatusBar,
  BackHandler,
  DeviceEventEmitter,
  Linking
} from "react-native";
import { FAB } from "react-native-paper";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { useNavigation } from '@react-navigation/native';

import styles from "./Style";
import { Context } from "context/Provider";
import apiUrl from "utils/apiUrl";
import { fetchGetByToken, fetchGet, fetchPostByToken } from "utils/fetchData";
import {useLocal} from 'hook/useLocal';

import Loading from "components/Loading";
import NoPackage from "components/NoPackage";
import DateRangePicker from "components/DatePicker";
import DatePickerButton from "components/DatePickerButton";
import Banner from "components/Banner";
import DepotFilterModal from "components/DepotFilterModal";
import RiceItem from "components/RiceItem";
import Header from "components/Header";

const CommodityDepotScreen = ({ navigation, route }) => {
  const local = useLocal();
  const stackNavigation = useNavigation();

  const {
    token
  } = useContext(Context);

  const [
    states,
    setStates
  ] = useState([]);
  const [
    allTownships,
    setAllTownships
  ] = useState([]);
  const [
    error, 
    setError
  ] = useState("");
  const [
    pkgError,
    setPkgError
  ] = useState(false);
  const [
    showFilter,
    setShowFilter
  ] = useState(false);

  const [
    loading, 
    setLoading
  ] = useState(false);
  const [
    stationList,
    setStationList
  ] = useState();
  const [
    station,
    setStation
  ] = useState();
  const [
    stationId,
    setStationId
  ] = useState();
  const [
    riceList,
    setRiceList
  ] = useState();
  const [
    startDate,
    setStartDate
  ] = useState("");
  const [
    endDate,
    setEndDate
  ] = useState("");
  const [
    datePicker,
    setDatePicker
  ] = useState(false);
  const [
    banners,
    setBanners
  ] = useState();
  const [
    filterState,
    setFilterState
  ] = useState();
  const [
    filterTownship,
    setFilterTownship
  ] = useState();

  const [page, setPage] = useState(1);
  const [loadMore, setLoadMore] = useState(false);
  const [moreLoading, setMoreLoading] = useState(false);


  useEffect(() => {
    setPage(1)
    setLoading(true);
    if (route && route.params) {
      const defaultDepotId = route.params.defaultDepotId;
      getStations(defaultDepotId)
    } else {
      getStations(1)
    }
  }, [route]);

  useEffect(()=>{
    if (token) {
      getBannerAds();
    }
  }, [token]);

  useEffect(() => {
    if (stationId) {
      if (page > 1) {
        getStationPrices(stationId);
      }
       else {
        setLoading(true);
        getStationPrices(stationId);
      }
    }
}, [page, stationId, token, startDate, endDate]); 

  const backButtonHandler = useCallback(() => {
    stackNavigation.navigate('HomeStack', {
      screen : 'HomeScreen'
    });
    return true;
  }, []);

  useEffect(()=> {
    const eventEmitter = DeviceEventEmitter.addListener(
        'package_change',
        val => {
          if (token) {
            setLoading(true)
            setPage(1);
            getStationPrices(stationId);
          }
        },
      );
      return () => eventEmitter;
}, [token, startDate, endDate, stationId])

  const getBannerAds = async () => {
    const response = await fetchGetByToken(
      apiUrl.exchangeStationAds,
      token
    );
    if (response && response.success && response.data && response.data.length > 0) {
      setBanners(response.data);
    }
  }

  const getStations = async (defaultId) => {

    const response = await fetchGetByToken(
      apiUrl.exchangeStations,
      token
    );
    if (response?.success) {
      setStationList(response.data);
      if (response.data?.length > 0) {
        if (defaultId) {
          const filterStation = response.data.filter(item => item.id === defaultId || item.id === defaultId + '');
          if (filterStation && filterStation.length > 0) {
            setStation(filterStation[0]);
            setStationId(filterStation[0]?.id)
          } else {
            setStation(response.data[0]);
            setStationId(response.data[0]?.id)
          }
        } else {
          setStation(response.data[0]);
          setStationId(response.data[0]?.id)
        }
      }
    } else {
      setLoading(false)
    }
  }

  const getStationPrices = async (id) => {

    setMoreLoading(true)
    let param = '';
    if (startDate && startDate !== '') {
      param += `&start_date=${startDate}`;
    }
    if (endDate && endDate !== '') {
      param += `&end_date=${endDate}`;
    }

    const response = await fetchGetByToken(
      `${apiUrl.exchangeStationPrices}?page=${page}&id=${id}${param}`,
      token
    );

    if (response?.success) {
      setPkgError(false)
      if (response.data && response.data.length === 0 && page === 1) {
        setError(local.noData)
      } else {
        setError("")


        const newList = page > 1 ? riceList?.concat(response.data) : response.data;
        setRiceList(newList);

        if(response.nextPageUrl){
          setLoadMore(true);
        }else{
          setLoadMore(false);
        }
      }

    } else {
      setError(response?.message ?? local.noData);
     
      if (response?.status === 403) {
        setPkgError(true);
      } else {
        setPkgError(false)
      }
    }
    setLoading(false)
    setMoreLoading(false)
  }

  useEffect(() => {
    const getStatesAndDivision = async () => {
      const response = await fetchGet(apiUrl.statesAndTownships);
      if (response?.success) {
        setStates(response.states ?? []);
        setAllTownships(response.townships ?? [])
      }
    }
    getStatesAndDivision();
  }, []);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, [backButtonHandler]);


  const onFilterHandler = () => {
    setShowFilter(true);
  };

  const onSubmitFilter = (st, state, township) => {
    setShowFilter(false);
    if (stationId !== st.id) {
      setStationId(st.id);
      setStation(st)
      setFilterState(state)
      setFilterTownship(township)

      setRiceList([]);
      setPage(1);
    }
  }

  const chatHandler = () => {
    navigation.navigate('ChatStack')
  }

  const itemHandler = (riceItem) => {
    navigation.navigate("DepotRiceDetail", {item : riceItem, title: station.name + " - " + riceItem.name})
  }

  const handleBuy = () => {
    navigation.navigate('Package');
  }

  const reloadData = async () => {
    setLoading(true)
    getStationPrices(stationId);
  }

  const onSubmitDate = (start, end) => {
    setDatePicker(false);
    setStartDate(start);
    setEndDate(end);

    setRiceList([]);
    setPage(1);
  }

  const handleLoadMore = () => {
    if(loadMore){
        setPage(page + 1)
    }
}

  const bannerHandler = async (item) => {
    if (item?.type === "phone") {
      Linking.openURL(`tel:${item.value}`)
    } else if (item?.type === "website") {
      Linking.openURL(item.value)
    } else if (item?.type === "facebook") {
      Linking.openURL("https://www.facebook.com/profile.php?" + item.value);
    } else if (item?.type === "user") {

      setLoading(true);

      const data = {
        user_id: item.value
      };
  
      const response = await fetchPostByToken(apiUrl.getUserDetail, data, token);

      setLoading(false);
      if (response?.success && response.user) {
        if (response.user.user_type_id === 1) { // Warehouse
          stackNavigation.navigate('WarehouseStack', {
            screen: 'RiceList',
            params: {item : response.user, fromHome : true}
          });
         } else { // Rice Mill
          stackNavigation.navigate('RiceMillStack', {
            screen: 'RiceList',
            params: {item : response.user, fromHome : true}
          });
         }
      } else {
        ToastAndroid.show(response.message ?? local.fail, ToastAndroid.SHORT)
      }
    }
  }

  return <View style={ styles.container }>
          <Header 
            depot={station?.name}
            notiHandler={()=> navigation.navigate('NotificationScreen')} 
            chatHandler={chatHandler}
            newsHandler={()=> navigation.navigate('KnowledgeStack')} />
          <StatusBar backgroundColor='#f2f2f2' barStyle='dark-content' />

          <DatePickerButton
            width={ wp(94) }
            startDate={ startDate }
            endDate={ endDate }
            pressHandler={() => setDatePicker(true)} />

          {banners && banners.length && <Banner 
            home={false}
            data={banners}
            bannerHandler={bannerHandler} />}


            {!loading && <>
                {!!error ? <NoPackage
                  pkgError={pkgError}
                  text={error}
                  home={true}
                  buyHandler={handleBuy}
                  top={hp(20)}
                  reloadHandler={reloadData} /> : <FlatList
                  style={ styles.list }
                  onEndReached={handleLoadMore}
                  onEndReachedThreshold={0.7}
                  ListFooterComponent={moreLoading && <Loading />}
                  alwaysBounceVertical={false}
                  showsVerticalScrollIndicator={false}
                  data={riceList} 
                  keyExtractor={(item) => item.id.toString()}
                  renderItem={({item})=>{
                    return <RiceItem
                      item={ item }
                      itemHandler={() => itemHandler(item)} />;
                  }}
                  />}
            </>}

            {showFilter ? <DepotFilterModal
              defaultState={ filterState }
              defaultTownship={ filterTownship }
              defaultDepot={ station }
              states={ states }
              allTownships={ allTownships }
              stationList={stationList}
              onClose={()=> setShowFilter(false)}
              onSubmit={onSubmitFilter} /> 
              : null} 

        <Loading visible={loading} />

        <DateRangePicker 
          visible={datePicker}
          onClose={()=> setDatePicker(false)}
          onSubmit={onSubmitDate} />

      {!pkgError && <View style={styles.fabContent}>
        <FAB
          icon={({ size, color }) => <Icon name="filter" size={size} color={color} />} 
          onPress={onFilterHandler}
          style={styles.fabIcon}
          color="white"
        />
      </View>}
    </View>
};

export default memo(CommodityDepotScreen);
