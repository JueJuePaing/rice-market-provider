import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import Colors from "assets/colors";

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor: '#f2f2f2'
      },
      list : {
        marginBottom: hp(1)
      },
      fabIcon : {
        backgroundColor: Colors.light_green,
        width: 56,
        height: 56,
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center'
      },
      fabContent: {
        position: 'absolute',
        bottom: hp(2),
        right: 10
      },

      // button: {
      //   marginTop: hp(3),
      //   width: wp(40),
      //   borderRadius: hp(3),
      //   alignSelf: 'center'
      // },
      // noData: {
      //   alignSelf: 'center',
      //   marginTop: hp(35),
      //   fontSize: hp(1.9),
      //   color: '#000',
      //   paddingHorizontal: wp(3)
      // },
      // container : {
      //   flex : 1,
      //   backgroundColor: '#f2f2f2'
      // },
      // list : {
      //   marginTop: hp(-1),
      //   marginBottom: hp(11)
      // },
      // fabContent: {
      //   position: 'absolute',
      //   bottom: hp(11),
      //   right: 10
      // },
      // fabIcon : {
      //   backgroundColor: Colors.light_green,
      //   width: 56,
      //   height: 56,
      //   borderRadius: 30,
      //   justifyContent: 'center',
      //   alignItems: 'center'
      // }


      detailContainer : {
        flex : 1,
        backgroundColor: '#f2f2f2'
      },
      dateRangeContent : {
        backgroundColor: Colors.light_green,
        paddingHorizontal: wp(3),
        paddingVertical: hp(1),
        borderRadius: hp(1),
        alignSelf: 'center',
        marginTop: hp(1),
        marginBottom: hp(2.5)
      },
      legendRow : {
        flexDirection : 'row',
        alignItems : 'center',
        justifyContent : 'space-around',
        marginBottom : hp(4),
        marginTop : hp(11)
      },
      label : {
        fontSize : hp(1.5),
        color: "gray",
        paddingLeft: wp(1)
      },
      circleRow: {
        flexDirection: 'row',
        alignItems: 'center',
      },
      circle: {
        width: wp(2),
        height: wp(2),
        borderRadius: wp(3),
        backgroundColor: '#dbc672',
        marginRight: wp(1.5)
      },
      dateRangeWrapper : {
        width : wp(80),
        alignSelf : 'center',
        paddingHorizontal : wp(5),
        paddingVertical : hp(1),
        borderRadius : hp(5),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor : Colors.light_green,
        marginTop : hp(1.5)
      },
      selectTxt : {
        color: '#dae3c5',
        fontSize: hp(1.5),
        textAlign: 'center',
        paddingBottom : hp(.4)
      },
      dateRangeTxt : {
        color: "#fff",
        fontSize: hp(1.7)
      }
});

export default styles;