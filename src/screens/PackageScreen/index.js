import React, {
  useState, 
  memo, 
  lazy,
  useRef,
  useEffect,
  useCallback,
  useContext
} from "react";
import {
  View,
  ToastAndroid,
  Text, 
  BackHandler,
  TouchableOpacity,
  DeviceEventEmitter
} from "react-native";
import { WebView } from 'react-native-webview';
import { Button, DefaultTheme, HelperText} from "react-native-paper";
import Ionicons from "react-native-vector-icons/Ionicons";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

import Colors from "assets/colors";
import {
  PackageScreen as styles
} from "assets/style";
import { Context } from "context/Provider";
import apiUrl from "utils/apiUrl";
import { fetchGetByToken, fetchPostByToken } from "utils/fetchData";
import {useLocal} from '../../hook/useLocal';

const MainApplicationContainer = lazy(() => import("components/MainApplicationContainer"));
const MainApplicationBody = lazy(() => import("components/MainApplicationBody"));

import SelectModal from "components/SelectModal";
import DateRangePicker from "components/DatePicker";
import Header from "components/Header/DetailHeader";

const PackageScreen = ({ navigation }) => {

  const local = useLocal();
  const theme = {
    ...DefaultTheme,
    colors : {
      ...DefaultTheme.colors,
      primary : "#87ad2b"
    }
  };

  const webViewRef = useRef(null);

  const {
    token,
    userInfo
  } = useContext(Context);

  const parsedInfo = userInfo ? JSON.parse(userInfo) : null;

  const [
    choosingState,
    setChoosingState
  ] = useState(false);
  const [
    loading, 
    setLoading
  ] = useState(false);
  const [
    error, 
    setError
  ] = useState("");
  const [
    packageList,
    setPackageList
  ] = useState();
  const [
    selectedPackage,
    setPackage
  ] = useState();
  const [
    startDate,
    setStartDate
  ] = useState("");
  const [
    endDate,
    setEndDate
  ] = useState("");
  const [
    datePicker,
    setDatePicker
  ] = useState(false);

  const backButtonHandler = useCallback(() => {
    navigation.goBack();
    return true;
}, []);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, [backButtonHandler]);

  useEffect(() => {
    const startdate = new Date();

    const end = new Date();
    end.setDate(end.getDate() + 7);

    const year = startdate.getFullYear();
    const month = String(startdate.getMonth() + 1).padStart(2, "0");
    const day = String(startdate.getDate()).padStart(2, "0");
    const formattedStartdate = `${year}-${month}-${day}`;

    const endYear = end.getFullYear();
    const endMonth = String(end.getMonth() + 1).padStart(2, "0");
    const endDay = String(end.getDate()).padStart(2, "0");
    const formattedEnddate = `${endYear}-${endMonth}-${endDay}`;

    setStartDate(formattedStartdate);
    setEndDate(formattedEnddate);
  }, []);

  useEffect(() => {
    const getPackageList = async () => {
      setLoading(true)
      const response = await fetchGetByToken(
        apiUrl.getPackages,
        token
      );
      setLoading(false)

      if (response?.success) {
        setPackageList(response.data);
      } else {
        setError(response?.message ?? local.noPackage)
      }
    }
    getPackageList();
  }, [token]);
  
  const handleBuy = async () => {
   
    if (!selectedPackage) {
      setError(local.invalidPackage);
      return;
    }

    setLoading(true);

    const data = {
      user_id : parsedInfo.id,
      package_id : selectedPackage?.id,
      start_date : startDate,
      end_date : endDate
    };

    const response = await fetchPostByToken(
      apiUrl.buyPackage,
      data,
      token
    );

    setLoading(false);
    if (response?.success) {
      ToastAndroid.show(response?.message ?? local.successful, ToastAndroid.SHORT)
      DeviceEventEmitter.emit('package_change', 'true');
      navigation.goBack();
    } else {
      if (response?.errors?.end_date) {
        setError(response?.errors?.end_date)
      } else if (response?.errors?.start_date) {
        setError(response?.errors?.start_date)
      }
      ToastAndroid.show(response?.message ?? local.fail, ToastAndroid.SHORT)
    }
  };

  const backHandler = () => {
    navigation.goBack();
  }

  const onChooseModalData = (data) => {
    setChoosingState(false);
    setPackage(data);

    setError('');
  }

  const onSubmitDate = (start, end) => {
    setError('');
    setDatePicker(false);
    setStartDate(start);
    setEndDate(end);
  }

  return <MainApplicationContainer>
  <MainApplicationBody>
    <View
      style={ styles.container }>
      <Header 
        color='#fff'
        title={local.buyPackage}
        backHandler={backHandler} />


      <View style={styles.content}>

        <TouchableOpacity 
          activeOpacity={0.8}
          onPress={()=> setDatePicker(true)}
          style={styles.dateRangeWrapper}>
          <Ionicons
            name="calendar"
            size={hp(2.6)}
            color={Colors.light_green} />
          <View>
            <Text style={styles.selectTxt}>
              {local.chooseDate}
            </Text>
            <Text style={styles.dateRangeTxt}>
              {startDate} ~ {endDate}
            </Text>
          </View>
          <Ionicons
            name="chevron-down"
            size={hp(2.3)}
            color={Colors.light_green} />
        </TouchableOpacity>

        <TouchableOpacity 
          activeOpacity={0.8}
          onPress={()=> setChoosingState(true)}
          style={styles.selectWrapper}>
            <Text style={styles.dateRangeTxt2}>
              {selectedPackage ? selectedPackage?.name : local.choosePackage}
            </Text>
          <Ionicons
            name="caret-down-circle"
            size={hp(2.8)}
            color={Colors.light_green} />
        </TouchableOpacity>

        {selectedPackage && selectedPackage.description && 
          <View style={{
            width : wp(90), 
            height: hp(50),
            alignSelf: 'center',
            marginTop: hp(1)
          }}>
            <WebView
             ref={webViewRef}
             javaScriptEnabled={false}
              originWhitelist={['*']}
              source={{ html: selectedPackage?.description }}
              style={{
                width : wp(90),
                height: hp(50),
              }}
              />
              </View>
        }

      </View>


      {!!error && (
        <HelperText type="error" style={styles.errorTxt}>
          {error}
        </HelperText>
      )}


      <Button
        mode="contained"
        theme={theme}
        onPress={handleBuy}
        style={styles.button}
        labelStyle={{fontSize: hp(1.8)}}
        disabled={loading}>
        {local.buy}
      </Button>

    </View>

    {choosingState && <SelectModal
          title={ local.choosePackage }
          data={ packageList }
          selectedId={ selectedPackage?.id }
          onClose={() => {
            setChoosingState(false)
          }}
          onChoose={onChooseModalData} />}

<DateRangePicker 
          visible={datePicker}
          onClose={()=> setDatePicker(false)}
          onSubmit={onSubmitDate} />

  </MainApplicationBody>
  </MainApplicationContainer>;
};

export default memo(PackageScreen);
