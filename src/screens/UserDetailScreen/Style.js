import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import Colors from "assets/colors";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f2f2f2'
    },
    content: {
        marginHorizontal: wp(3),
        marginTop: hp(1),
        marginBottom: hp(2)
    },
    iconWrapper: {
        width: wp(94),
        height: hp(25),
        backgroundColor: '#b7cc83',
        borderRadius: hp(1),
        alignItems: 'center',
        justifyContent: 'center'
      },
      imgWrapper: {
        width: wp(94),
        height: hp(25),
        backgroundColor: '#b7cc83',
        borderRadius: hp(1),
        overflow : 'hidden'
      },
      iconImg: {
        width: wp(94),
        height: hp(25),
        //borderRadius: hp(1),
        resizeMode: 'cover'
      },
      name: {
        fontSize: hp(2),
        fontWeight: 'bold',
        color:'#000',
        marginTop: hp(2)
      },
      starRow : {
        flexDirection: 'row',
        marginTop: hp(.7)
      },
      row: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: hp(1.5)
      },
      nameRow: {
        justifyContent: 'space-between', 
        marginBottom: hp(1),
        flexDirection: 'row',
        alignItems: 'center'
      },
      contactVal: {
        paddingLeft: wp(2),
        fontSize: hp(1.9),
        color: '#363433',
        fontWeight: '600',
        maxWidth: wp(88)
      },
    detail: {
      marginTop: hp(4),
      // fontSize: hp(1.8),
      // color: '#52504f',

      fontSize: hp(2),
        fontWeight: 'bold',
        color:'#000',
    },
    detailContent: {
      backgroundColor: '#fff',
      marginTop: hp(1),
      borderRadius: hp(.5),
      paddingHorizontal: wp(3),
      paddingTop: hp(2)
    },
    detailRow: {
      marginBottom: hp(2),
      // flexDirection: 'row',
      // alignItems: 'center',
      // justifyContent: 'space-between'
    },
    detailLabel: {
      fontSize: hp(1.8),
      color: '#424140',
    },
    detailVal: {
      fontSize: hp(1.9),
      color: '#363433',
      fontWeight: '600'
    },
    viewAll: {
      paddingLeft: 8,
      marginTop: 6,
      color: Colors.light_green,
      fontSize: hp(1.6)
    },
    valRow: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: hp(.6),
      marginLeft: wp(1)
    },
    noData: {
      fontSize: hp(1.8),
      color: '#8b8f8b',
      paddingLeft: 8,
      marginTop: 5
    },
    tabContent: {
      width: wp(84),
      alignSelf: 'center',
      borderRadius: hp(3),
      backgroundColor: Colors.light_green,
      alignItems: 'center',
      // justifyContent: 'space-around',
      flexDirection:'row',
      // height: hp(6),
      marginBottom: hp(2),
      paddingHorizontal: wp(1.5),
      paddingVertical: wp(1),
      marginTop: hp(1.5)
    },
    tab: {
      width: wp(40.5),
      height: hp(5),
      borderRadius: hp(3),
      alignItems: 'center',
      justifyContent: 'center'
    },
    activeTab: {
      backgroundColor: '#fff'
    },
    tabText: {
      color: '#e8e7e3',
      fontSize: hp(2)
    },
    activeText: {
      color: Colors.dark_green,
      fontWeight: '600'
    },
    noData: {
      fontSize: hp(1.8),
      color: '#8b8f8b'
    },
    noDataContent: {
      alignItems: 'center',
      justifyContent: 'center',
      height: hp(70)
    },
    listContainer: {
      width: wp(94),
      alignSelf: 'center'
    },
    reviewItem: {
      backgroundColor: '#fff',
      width: wp(94),
      paddingHorizontal: wp(4),
      paddingTop: hp(2),
      marginBottom: hp(1.5),
      borderRadius: hp(2)
    },
    reviewRow: {
      flexDirection : 'row',
      alignItems: 'center',
      marginBottom: hp(1.5),
      justifyContent: 'space-between'
    },
    reviewName: {
      color: '#000',
      fontSize: hp(1.8),
      fontWeight: 'bold'
    },
    review: {
      color: 'gray',
      fontSize: hp(1.65),
    },
    time: {
      alignSelf: 'flex-end',
      color: '#a5a8a2',
      fontSize: hp(1.4),
      paddingBottom: hp(1)
    },
});

export default styles;