import React, {
  useState,
  useEffect,
  useCallback,
  useContext
} from "react";
import {
  ScrollView,
  View, 
  Image,
  TouchableOpacity,
  StatusBar,
  Linking,
  Text,
  BackHandler,
  ToastAndroid,
  FlatList
} from "react-native";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

import { Context } from "context/Provider";
import apiUrl from "utils/apiUrl";
import { fetchGet, fetchPostByToken } from "utils/fetchData";
import { useLocal } from "hook/useLocal";
import styles from './Style';

import Loading from "components/Loading";
import Header from "components/Header/DetailHeader";
import RiceWarehouse from 'assets/icons/RiceWarehouse';
import HomeRiceMill from "assets/icons/HomeRiceMill";
import Rating from "components/Rating";

const UserDetailScreen = ({navigation, route}) => {

  const {item, mill} = route.params;
  const local = useLocal();

  const {
    token
  } = useContext(Context);

  const [
    loading,
    setLoading
  ] = useState(true);
  const [
    states,
    setStates
  ] = useState([]);
  const [
    allTownships,
    setAllTownships
  ] = useState([]);
  const [
    userTypes,
    setUserTypes
  ] = useState([]);
  const [
    reviews,
    setReviews
  ] = useState();
  const [
    tab,
    setTab
  ] = useState(1);

  useEffect(() => {
    if (token && item) {
      setLoading(true)
      getReviewList();
    }
  }, [token, item])

  const getReviewList = async () => {
    const data = {
      user_id : item.id
    };

    const response = await fetchPostByToken(
      apiUrl.getReviews,
      data,
      token
    );

    setLoading(false);

    if (response?.success) {
      if (response.data && response.data.length > 0) {
        setReviews(response.data);
      }
    } else {
      ToastAndroid.show(response.message ?? local.somethingWrong, ToastAndroid.SHORT)
    }
  }

  const backButtonHandler = useCallback(() => {
    navigation.goBack();
    return true;
  }, []);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, [backButtonHandler]);

  useEffect(() => {
    const getStatesAndDivision = async () => {
      const response = await fetchGet(apiUrl.statesAndTownships);
      if (response?.success) {
        setStates(response.states);
        setAllTownships(response.townships)
      }
      setLoading(false)
    }
    getStatesAndDivision();
  }, []);

  useEffect(() => {
    const getUserTypes = async () => {
      const response = await fetchGet(apiUrl.userTypes);
      if (response?.success) {
        setUserTypes(response.data);
      }
    }
    getUserTypes();
  }, []);

    const state = states?.length > 0 ? states.filter(state => state.id === item?.states_and_division_id) : null;
    const township = allTownships?.length > 0 ? allTownships.filter(ts => ts.id === item?.township_id) : null;
    const userType = userTypes?.length > 0 ? userTypes.filter(type => type.id === item?.user_type_id): null;

  const renderStarIcons = () => {
    if (!item?.rating) return;

    const stars = [];
    for (let i = 0; i < Math.ceil(parseInt(item.rating)); i++) {
      stars.push(
        <FontAwesome
          key={i}
          name="star"
          size={hp(2.2)}
          color="#d6a609"
          style={{ marginRight: wp(1) }}
        />
      );
    }
    return stars;
  };

  const _renderItem = ({item}) => {

    return ( 
      <TouchableOpacity
        activeOpacity={0.98}
        style={styles.reviewItem}
        key={item.id}>
        <View style={[styles.reviewRow, {
          marginBottom: !!item.review ? hp(1.5) : hp(.5)
        }]}>
          <Text style={styles.reviewName}>{item.reviewer}</Text>
          <Rating rating={item.rating ? Math.ceil(parseInt(item.rating)) : 0} size={"medium"} />
        </View>
        {!!item.message && <Text style={styles.review}>{item.message}</Text>}
        <Text style={styles.time}>{item.time}</Text>
      </TouchableOpacity>
    );
  };
  return ( <ScrollView 
    showsVerticalScrollIndicator={false}
    style={styles.container}>
      <StatusBar backgroundColor='#f2f2f2' />
      <Header
        title={item ? item.name : local.userDetail}
        backHandler={()=> navigation.goBack()}/>

      <View style={styles.tabContent}>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={()=> setTab(1)}
          style={[styles.tab, tab === 1 && styles.activeTab]}>
          <Text style={[styles.tabText, tab === 1 && styles.activeText]}>{local.detail}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={()=> setTab(2)}
          style={[styles.tab, tab === 2 && styles.activeTab]}>
          <Text style={[styles.tabText, tab === 2 && styles.activeText]}>{local.reviews}</Text>
        </TouchableOpacity>
      </View>

      {tab === 1 ? <View style={styles.content}>
        {item.image ? <View style={styles.imgWrapper}>
          <Image
            style={styles.iconImg}
            source={{ uri : item.image }} /></View> : 
          <View style={styles.iconWrapper}>
            {mill ? <HomeRiceMill size={hp(10)} color='#fff' /> : 
            <RiceWarehouse size={hp(10)} color='#fff' />}
          </View>}

          <View style={styles.nameRow}>
            <Text style={styles.name}>{item.name ?? "-"}</Text>
            {item.rating && <View style={styles.starRow}>
              {renderStarIcons()}
            </View>}
          </View>

          <TouchableOpacity
            onPress={()=>  Linking.openURL(`tel:${item.phone_number}`)} 
            style={styles.row}>
            <MaterialIcons 
              name='phone' 
              size={hp(2.5)}
              color='#595957' />
            <Text style={styles.contactVal}>{item?.phone_number ?? '-' }</Text>
          </TouchableOpacity>
          <View style={styles.row}>
            <MaterialIcons 
              name='location-pin' 
              color='#595957'
              size={hp(2.5)} />
            <Text style={styles.contactVal}>{item?.address ?? '-' }</Text>
          </View>

          <Text style={styles.detail}>{local.detail}</Text>

          <TouchableOpacity
             activeOpacity={1}
             style={styles.detailContent}>
            <View style={styles.detailRow}>
              <Text style={styles.detailLabel}>{local.statesAndDivision}</Text>
              <View style={styles.valRow}>
                <MaterialCommunityIcons 
                  name='map' 
                  size={hp(2)}
                  color='#595957'
                   />
                <Text style={styles.detailVal}>
                  &nbsp;&nbsp;{state ? state[0]?.name : "N/A"}
                </Text>
              </View>
            </View>

            <View style={styles.detailRow}>
              <Text style={styles.detailLabel}>{local.township}</Text>
              <View style={styles.valRow}>
                <MaterialCommunityIcons 
                  name='map' 
                  size={hp(2)}
                  color='#595957'
                   />
                <Text style={styles.detailVal}>
                  &nbsp;&nbsp;{township ? township[0]?.name : "N/A"}
                </Text>
              </View>

            </View>

            <View style={styles.detailRow}>
              <Text style={styles.detailLabel}>
                {local.userType}
              </Text>
              <View style={styles.valRow}>
                <MaterialIcons 
                  name='home' 
                  size={hp(2.3)}
                  color='#595957'
                   />
                <Text style={styles.detailVal}>
                  &nbsp;&nbsp;{userType ? userType[0]?.name : "N/A"}
                </Text>
              </View>
            </View>

          </TouchableOpacity>

      </View> : <View style={styles.content}>
        {!reviews ? <View style={styles.noDataContent}>
            <Text style={styles.noData}>{local.noData}</Text>
          </View> : <FlatList
            data={reviews}
            keyExtractor={(item, index) => item.id}
            renderItem={_renderItem}
            showsVerticalScrollIndicator={false}
            style={styles.listContainer}
          />}
      </View>}      
        
      <Loading visible={loading} />
    </ScrollView>
     
  );
};

export default UserDetailScreen;
