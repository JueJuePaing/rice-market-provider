import React, { 
  useState,
  useCallback,
  useEffect
} from "react";
import {
  View,
  StatusBar,
  BackHandler,
  ScrollView
} from "react-native";
import { WebView } from 'react-native-webview';
import {
  heightPercentageToDP as hp, 
  widthPercentageToDP as wp
} from "react-native-responsive-screen";

import apiUrl from "utils/apiUrl";
import {  fetchPost } from "utils/fetchData";
import { useLocal } from "hook/useLocal";

import Header from "components/Header/DetailHeader";
import Loading from "components/Loading";

const TermsConditionsScreen = ({ navigation, route }) => {

  const local = useLocal();
  const [
    loading,
    setLoading
  ] = useState(true);
  const [
    data,
    setData
  ] = useState()

  const backButtonHandler = useCallback(() => {
    navigation.goBack();
    return true;
  }, []);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, [backButtonHandler]);

 
  useEffect(() => {
    getTermsConditions();
  }, []);

  const getTermsConditions = async () => {

    const response = await fetchPost(
      apiUrl.termsAndConditions, {});

    console.log("data>>", response.data);

    if (response?.success) {
      if (response.data && response.data.data) {
        setData(response.data.data);
      } else if (response.data) {
        setData(response.data);
      }
    }
    setLoading(false)
  }

  return  <View style={{
    flex : 1,
    backgroundColor: '#fff',
 }}>
    <StatusBar 
        backgroundColor={"#fff"} 
        barStyle="dark-content" />
     
        <Header
          noMarquee={true}
          title={local.termsAndConditions}
          color="#fff"
          noChat={true}
          backHandler={()=> navigation.goBack()} />

        <ScrollView 
          showsVerticalScrollIndicator={false}
          style={{
                width : wp(90),
                height: hp(100),
                alignSelf: 'center',
                marginVertical: hp(1)
              }}>
          {data && <WebView
              originWhitelist={['*']}
              source={{ html: data }}
              style={{
                width : wp(90),height: hp(90)
              }} />}
        </ScrollView>

      <Loading visible={loading} />
  </View>

};

export default TermsConditionsScreen;
