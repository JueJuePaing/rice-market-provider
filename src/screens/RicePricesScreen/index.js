import React, {
  useState, 
  memo,
  useEffect,
  useCallback,
  useContext
} from "react";
import {
  View,
  StatusBar,
  BackHandler,
  RefreshControl,
  Alert,
  ScrollView,
  TouchableOpacity,
  ToastAndroid
} from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import {SwipeListView} from 'react-native-swipe-list-view';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import {useLocal} from 'hook/useLocal';
import {
  RicePricesScreen as styles
} from "assets/style";
import { Context } from "context/Provider";
import apiUrl from "utils/apiUrl";
import { fetchGetByToken, fetchPostByToken } from "utils/fetchData";
import Colors from "assets/colors";

import Loading from "components/Loading";
import Header from "components/Header/DetailHeader";
import RicePriceItem from "components/RicePriceItem";
import NoPackage from "components/NoPackage";
import DateRangePicker from "components/DatePicker";
import DatePickerButton from "components/DatePickerButton";

const RicePricesScreen = ({ navigation }) => {

  const {
    token,
    userInfo
  } = useContext(Context);
  const local = useLocal();

  const userData = userInfo ? JSON.parse(userInfo) : null;

  const [
    refreshing,
    setRefreshing
  ] = useState(false)
  const [
    startDate,
    setStartDate
  ] = useState("");
  const [
    endDate,
    setEndDate
  ] = useState("");
  const [
    error, 
    setError
  ] = useState("");
  const [
    pkgError,
    setPkgError
  ] = useState(false);
  const [
    loading, 
    setLoading
  ] = useState(true);
  const [
    priceList,
    setPriceList
  ] = useState([]);
  const [
    riceTypes,
    setRiceTypes
  ] = useState();
  const [
    itemHeight,
    setItemHeight
  ] = useState(hp(10));
  const [
    datePicker,
    setDatePicker
  ] = useState(false)

  const [page, setPage] = useState(1);
  const [loadMore, setLoadMore] = useState(false);
  const [moreLoading, setMoreLoading] = useState(false);

  const backButtonHandler = useCallback(() => {
    navigation.goBack();
    return true;
  }, []);

  useEffect(() => {
    if (token) {
      setLoading(true);
      getRiceTypes();
      getRicePrices();
    } else {
      setLoading(false)
    }
  }, [token, startDate, endDate]);

  useEffect(() => {
    getRicePrices()
}, [page]);

  const onRefresh = useCallback(() => {
    setRefreshing(true);

    const reloadData = async () => {
      getRiceTypes();
      setPriceList([]);
      setPage(1);
    };

    reloadData();
}, [token, userData, startDate, endDate]);

const handleLoadMore = () => {
  if(loadMore){
      setPage(page + 1)
  }
}

  const getRiceTypes = async () => {
    const response = await fetchGetByToken(
      apiUrl.getRiceTypes,
      token
    );
    if (response?.success) {
      setRiceTypes(response.data);
      setPkgError(false)
    } else {
      if (response?.status === 403) {
        setPkgError(true);
      } else {
        setPkgError(false)
      }
    }
  }

  const getRicePrices = async () => {

    setMoreLoading(true)

    let param = '';
    if (startDate && startDate !== '') {
      param += `&start_date=${startDate}`;
    }
    if (endDate && endDate !== '') {
      param += `&end_date=${endDate}`;
    }

    const response = await fetchGetByToken(
      `${apiUrl.ricePriceList}?page=${page}&user_id=${userData.id}${param}`,
      token
    );

    if (response?.success) {
      if (response.data?.length === 0 && page === 1) {
        setError(local.noData)
      } else {
        setError("")

        const newList = page > 1 ? priceList?.concat(response.data) : response.data;
        setPriceList(newList);

        if(response.nextPageUrl){
          setLoadMore(true);
        }else{
          setLoadMore(false);
        }
      }
    } else {
      setError(response?.message ?? local.noData);
    }
    setMoreLoading(false)
    setLoading(false)
      setRefreshing(false)
  }

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, [backButtonHandler]);

  const editHandler = (item) => {
    navigation.navigate('AddRicePrice', {item, riceTypes})
  }

  const addHandler = () => {
    navigation.navigate('AddRicePrice', {riceTypes})
  }

  const handleBuy = () => {
    navigation.navigate('Package');
  }

  const reloadHandler = () => {
    setLoading(true);
    getRiceTypes();
    getRicePrices();
  }

  const _renderHiddenItem = ({index, item}) => {
    return (
      <TouchableOpacity
        style={[styles.slideContainer, {height: itemHeight}]}
        onPress={() => swipeDeletHandler(item)}>
        <Icon 
          name='delete'
          size={hp(4)}
          color="#d14e30" 
          />
      </TouchableOpacity>
    );
  };

  const swipeDeletHandler = (item) => {
    Alert.alert(
      local.confirmation,
      local.confirmDelete,
      [
        { text: local.cancel, onPress: console.log("do nothing"), style: 'cancel' },
        { text: local.confirm, onPress: () => deleteRicePrice(item.id) },
      ],
      { cancelable: false } // Prevent dismissing the alert by tapping outside or pressing the back button
    );
  }

  const deleteRicePrice = async (id) => {
    setLoading(true)
    const response = await fetchPostByToken(
      `${apiUrl.deletePrice}?id=${id}`,
      {},
      token
    );

    if (response?.success) {
      getRiceTypes();
      getRicePrices();
    } else {
      if (response?.message) ToastAndroid.show(response.message, ToastAndroid.SHORT);
      setLoading(false)
    }
  }

  const renderItemLayout = (event, index) => {
    const { height } = event.nativeEvent.layout;
    setItemHeight(height);
  };

  const onSubmitDate = (start, end) => {
    setDatePicker(false);
    setStartDate(start);
    setEndDate(end);
  }

  return (
    <ScrollView 
      refreshControl={
        <RefreshControl
            refreshing={ refreshing }
            colors={[ Colors.light_green ]}
            onRefresh={ onRefresh } 
            progressViewOffset={hp(4)} />
      }
      style={styles.container}>
      <StatusBar backgroundColor='#f2f2f2' />
      <Header
        title={local.ricePrices}
        add={!pkgError && !loading && (!userData || userData.user_type_id !== 3)}
        addHandler={addHandler}
        backHandler={()=> navigation.goBack()}/>

        <DatePickerButton
          width={ wp(94) }
          startDate={ startDate }
          endDate={ endDate }
          pressHandler={() => setDatePicker(true)} />
      
      {!loading && <>
        {!!error ? <NoPackage
          text={error}
          pkgError={pkgError}
          buyHandler={handleBuy}
          reloadHandler={reloadHandler} /> : 
          <SwipeListView
            disableRightSwipe
            onEndReached={handleLoadMore}
            onEndReachedThreshold={1}
            ListFooterComponent={moreLoading && <Loading />}
            disableLeftSwipe={!userData || userData.user_type_id === 3}
            data={priceList}
            keyExtractor={(item, index) => item.id}
            renderItem={({item, index})=>{
              return <View onLayout={(event) => renderItemLayout(event, index)}>
              <RicePriceItem
                item={ item }
                editHandler={() => editHandler(item)} />
              </View>
            }}
            renderHiddenItem={_renderHiddenItem}
            rightOpenValue={-wp(14)}
            showsVerticalScrollIndicator={false}
            style={styles.list}
          />
            }
      </>}
      <Loading visible={loading} />

      <DateRangePicker 
        visible={datePicker}
        onClose={()=> setDatePicker(false)}
        onSubmit={onSubmitDate} />

    </ScrollView>)
};

export default memo(RicePricesScreen);
