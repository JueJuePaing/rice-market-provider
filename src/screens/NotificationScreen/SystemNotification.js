import React from "react";
import {
    View, 
    Text,
    StatusBar
} from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import AntDesign from 'react-native-vector-icons/AntDesign';
import moment from "moment-timezone";
import { WebView } from 'react-native-webview';

import styles from "./Style";
import {useLocal} from 'hook/useLocal';

import Header from "components/Header/DetailHeader";
  
const SystemNotificationScreen = ({ navigation, route }) => {

    const {item} = route.params;
    const local = useLocal();

    const dateTimeString = item.created_at;
    const formattedDate = item.date_time ? item.date_time : moment(dateTimeString).fromNow();


    return <View style={styles.detailContainer}>
        <StatusBar backgroundColor={"#fff"} barStyle="dark-content" />

        <Header 
            color="#fff"
            title={local.systemNoti}
            noMarquee={true}
            backHandler={()=> navigation.goBack()} />

        {item && <View style={styles.detailContent}>
            <Text style={styles.systemNotiTitle}>
                {item.title}
            </Text>
            <View style={styles.dateRow}>
                <AntDesign
                    name='clockcircleo'
                    size={hp(2)}
                    color='gray' />
                <Text style={styles.systemNotiDate}>
                    {formattedDate}
                </Text>
            </View>
            {item.data && <View style={styles.dataView}>
                <WebView
                    originWhitelist={['*']}
                    source={{ html: item.data }}
                    style={styles.dataView} />
            </View>}
        </View>}

    </View>

}

export default SystemNotificationScreen;