import React, { 
  useEffect, 
  useState,
  useContext,
  useCallback
} from "react";
import {
  View, 
  Text,
  DeviceEventEmitter,
  StatusBar,
  FlatList,
  TouchableOpacity,
  BackHandler
} from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { useNavigation } from '@react-navigation/native';

import DateRangePicker from "components/DatePicker";
import styles from "./Style";
import { Context } from "context/Provider";
import apiUrl from "utils/apiUrl";
import { fetchGetByToken } from "utils/fetchData";
import {useLocal} from '/hook/useLocal';

import SelectModal from "components/SelectModal";
import NoPackage from "components/NoPackage";
import Loading from "components/Loading";
import Header from "components/Header/DetailHeader";
import DatePickerButton from "components/DatePickerButton";

const NotificationScreen = ({ navigation }) => {

  const {token} = useContext(Context);
  const local = useLocal();
  const stackNavigation = useNavigation();

  const TYPE_DATA = [
    {
      id: "all",
      name: local.all
    },
    {
      id: "knowledge",
      name: local.knowledge
    },
    {
      id: "system",
      name: local.system
    },
    {
      id: "exchange_price",
      name: local.exchange_price
    },
  ]

  const [
    loading,
    setLoading
  ] = useState(false);
  const [
    datePicker,
    setDatePicker
  ] = useState(false);
  const [
    startDate,
    setStartDate
  ] = useState("");
  const [
    endDate,
    setEndDate
  ] = useState("");
  const [
    notificationList,
    setNotificationList
  ] = useState([]);
  const [
    error, 
    setError
  ] = useState("");
  const [
    showModal,
    setShowModal
  ] = useState(false);
  const [
    typeId,
    setTypeId
  ] = useState("all");

  const [page, setPage] = useState(1);
  const [loadMore, setLoadMore] = useState(false);
  const [moreLoading, setMoreLoading] = useState(false);

  const handleLoadMore = () => {
    if(loadMore){
        setPage(page + 1)
    }
}

  const backButtonHandler = useCallback(() => {
    navigation.goBack();
    return true;
  }, []);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, [backButtonHandler]);


  useEffect(() => {
    setLoading(true);
    setNotificationList([]);
    setPage(1);
    getNotificationList(1)
  }, [startDate, endDate, token]);

  useEffect(() => {
    if (page > 1) {
      getNotificationList(page)
    }
}, [page, token]);



const getNotificationList = async (pageNo) => {

  setMoreLoading(true)
  let param = '';
  if (startDate && startDate !== '') {
    param += `&start_date=${startDate}`;
  }
  if (endDate && endDate !== '') {
    param += `&end_date=${endDate}`;
  }

  const response = await fetchGetByToken(
    `${apiUrl.notificationList}?page=${pageNo}${param}`,
    token
  );

  if (response?.success) {

    if (response.data?.length === 0 && pageNo === 1) {
      setError(local.noData)
    } else {
      setError("")

      const newList = pageNo > 1 ? notificationList?.concat(response.data) : response.data;
      setNotificationList(newList);

      if(response.nextPageUrl){
        setLoadMore(true);
      }else{
        setLoadMore(false);
      }
    }
  } else {
    setError(response?.message ?? local.noData);
    
    if (response?.status === 403) {
      setPkgError(true);
    } else {
      setPkgError(false)
    }

  }
  setMoreLoading(false)
  setLoading(false)
}

  const reloadData = async () => {
    setLoading(true);
    getNotificationList(1);
  }

  const onChooseModalData = (data) => {
    setTypeId(data.id);
    setShowModal(false)
  }

  const onSubmitDate = (start, end) => {
    setDatePicker(false);
    setStartDate(start);
    setEndDate(end);
  }

  const itemHandler = (item) => {
  

    if (item.type === "exchange_price") {

      const parsed = JSON.parse(item.data);

      stackNavigation.navigate('ExchangeStationStack', {
        screen: 'CommodityDepot',
        params: {defaultDepotId : parsed?.exchange_station_id ? parseInt(parsed?.exchange_station_id) : 1}
      });

      setTimeout(() => {
        DeviceEventEmitter.emit('change_station_id', parsed?.exchange_station_id ? parseInt(parsed?.exchange_station_id) : 1);
      }, 100);

    } else if (item.type === "system") {
      navigation.navigate('SystemNotification', {item})
    } else if (item.type === "knowledge") {
      const parsed = JSON.parse(item.data);
      stackNavigation.navigate('KnowledgeStack', {
        screen: 'KnowledgeDetail',
        params: {item: null, knowledge_id : parsed.knowledge_id}
      });
    }
  }

  return <View style={styles.container}>
    <StatusBar backgroundColor={"#fff"} barStyle="dark-content" />

      <Header 
        color='#fff'
        title={local.notifications}
        noMarquee={true}
        backHandler={()=> navigation.goBack()} />

      <DatePickerButton
            width={ wp(94) }
            startDate={ startDate }
            endDate={ endDate }
            pressHandler={() => setDatePicker(true)} />

       {!loading && <>
        {!!error ? <View style={{marginTop : hp(-10)}}>
            <NoPackage
              text={error}
              pkgError={false}
              reloadHandler={reloadData} />
        </View> : <FlatList
          alwaysBounceVertical={false}
          showsVerticalScrollIndicator={false}
          data={notificationList} 
          style={{marginVertical : 7}} 
          keyExtractor={(item, index) => index.toString()}

          onEndReached={handleLoadMore}
          onEndReachedThreshold={1}
          ListFooterComponent={moreLoading && <Loading />}

          renderItem={({item})=>{
            return <TouchableOpacity
              activeOpacity={.8}
              style={styles.listItem}
              onPress={()=> itemHandler(item)}
              key={item.id}>
              <Text style={styles.itemTitle}>
                {item.title}
              </Text>
              {/* {item.type === "system" && <Text numberOfLines={2} style={styles.systemData}>{item.data}</Text>} */}
              <View style={styles.typeRow}>
                <TouchableOpacity
                activeOpacity={1}
                style={styles.typeBtn}>
                  <Text style={styles.type}>
                    {item.type === "exchange_price" ? local.exchange_price : item.type === "system" ? local.system : local.knowledge}
                  </Text>
                </TouchableOpacity>
                <Text style={styles.itemDate}>{item.date_time}</Text>
              </View>
            </TouchableOpacity>
          }}/>
        }
        </>}

 
        <DateRangePicker 
          visible={datePicker}
          onClose={()=> setDatePicker(false)}
          onSubmit={onSubmitDate} />
      
        <Loading visible={loading} />

      {showModal && <SelectModal
          title={ local.chooseType }
          data={ TYPE_DATA }
          selectedId={ typeId }
          onClose={() => {
            setShowModal(false)
          }}
          onChoose={onChooseModalData} />}
  </View>

};

export default NotificationScreen;
