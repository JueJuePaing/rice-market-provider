import { StyleSheet } from "react-native";
import {
    heightPercentageToDP as hp, 
    widthPercentageToDP as wp
} from "react-native-responsive-screen";
import Colors from "assets/colors";

const styles = StyleSheet.create({
    container : {
      flex : 1,
      backgroundColor: '#fff'
    },
    list : {
      // marginBottom: hp(15)
    },
    itemTitle : {
      color : '#000', //Colors.dark_green, //'#ba761c', //6c825f
      fontWeight : "600",
      fontSize : hp(1.7),
      paddingBottom : 3,
      paddingTop : 12

      //width: wp(90)
    },
    typeRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: wp(87),
        marginBottom: 5,
        marginTop: hp(1)
    },
    typeBtn: {
        backgroundColor: Colors.light_green,
        paddingHorizontal: wp(2),
        paddingVertical: hp(.5),
        borderRadius : hp(.6)
    },
    systemData: {
        fontSize: hp(1.7),
        color: '#333331',
        marginVertical: 5
    },
    type: {
        fontSize: hp(1.6),
        color: '#fff'
    },
    itemDate : {
      fontSize : 10,
      color : "gray",
    //   position : "absolute",
    //   right : -10,
    //   top : -5
    },
    listItem : {
      backgroundColor : "#E8E6E9",
      marginHorizontal : wp(3),
      borderRadius : hp(1),
      paddingBottom : 10,
      marginBottom: hp(1),
      paddingHorizontal: wp(4)
    },
    itemDesc: {
      fontSize: hp(1.7)
    },
    selectWrapper : {
      width : wp(46),
      borderRadius : hp(2),
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      backgroundColor : Colors.light_green, // '#ba761c',
      paddingHorizontal: wp(2),
      height: hp(5.5)
    },
    selectTxt : {
      color: '#dae3c5',
      fontSize: hp(1.5),
      textAlign: 'center',
      paddingBottom : hp(.4)
    },
    dateRangeTxt : {
      color: "#fff",
      fontSize: hp(1.6)
    },
    row: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginHorizontal: wp(3),
      marginTop : hp(1.5),
      marginBottom: hp(2)
    },
    detailContainer : {
      flex : 1,
      backgroundColor: '#fff',
    },
    detailContent: {
      paddingHorizontal: wp(4),
      paddingVertical : hp(2)
    },
    systemNotiTitle: {
      color : '#000', //Colors.dark_green, //'#ba761c', //6c825f
      fontWeight : "600",
      fontSize : hp(1.9),
      // textAlign: 'center'
    },
    systemNotiDate: {
      fontSize : hp(1.6),
      color : "gray",
      paddingLeft: wp(2)
    },
    systemNotiData : {
      color : '#000', //Colors.dark_green, //'#ba761c', //6c825f
      fontSize : hp(1.8),
      // textAlign: 'center'
    },
    dateRow: {
      flexDirection: 'row',
      alignItems: 'center',
      paddingTop : hp(1.5),
      paddingVertical: hp(3)
    },
    dataView: {
      width : wp(92), 
      height: hp(75)
    }
  });

export default styles;