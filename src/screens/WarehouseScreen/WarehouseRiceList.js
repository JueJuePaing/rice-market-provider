import React, {
  memo,
  useEffect,
  useContext,
  useState,
  useCallback
} from "react";
import {
  View,
  FlatList,
  BackHandler,
  ToastAndroid,
  Linking
} from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { useNavigation } from '@react-navigation/native';

import { Context } from "context/Provider";
import apiUrl from "utils/apiUrl";
import { fetchGetByToken, fetchPostByToken } from "utils/fetchData";
import {useLocal} from '../../hook/useLocal';
import styles from "./Style";

import MainApplicationContainer from "components/MainApplicationContainer";
import MainApplicationBody from "components/MainApplicationBody";
import Header from "components/Header/DetailHeader";
import RiceItem from "components/RiceItem";
import DateRangePicker from "components/DatePicker";
import ReviewModal from "components/ReviewModal";
import Loading from "components/Loading";
import NoPackage from "components/NoPackage";
import DatePickerButton from "components/DatePickerButton";
import Banner from "components/Banner";

const RiceListScreen = ({ navigation, route }) => {
  const local = useLocal();
  const stackNavigation = useNavigation()

  const {item, fromHome} = route.params;
  const {
    token,
    userInfo
  } = useContext(Context);

  const userData = userInfo ? JSON.parse(userInfo) : null;

  const [
    reviewModalOpened,
    setReviewModalOpened
  ] = useState(false);
  const [
    chatLoading,
    setChatLoading
  ] = useState(false);
  const [
    datePicker,
    setDatePicker
  ] = useState(false);
  const [
    loading,
    setLoading
  ] = useState(true);
  const [
    startDate,
    setStartDate
  ] = useState("");
  const [
    endDate,
    setEndDate
  ] = useState("");
  const [
    error, 
    setError
  ] = useState("");
  const [
    pkgError,
    setPkgError
  ] = useState(false);
  const [
    priceList,
    setPriceList
  ] = useState([]);
  const [
    banners,
    setBanners
  ] = useState();

  const [page, setPage] = useState(1);
  const [loadMore, setLoadMore] = useState(false);
  const [moreLoading, setMoreLoading] = useState(false);

  const backButtonHandler = useCallback(() => {
    if (fromHome) {
      navigation.replace("WarehouseSc")
    } else {
      navigation.goBack();
    }
    return true;
  }, [fromHome]);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, [backButtonHandler]);

  useEffect(() => {
    setLoading(true);
    setPriceList([]);
    setPage(1);
    getRicePrices(1)
  }, [startDate, endDate, token]);

  useEffect(() => {
    if (page > 1) {
      getRicePrices(page)
    }
}, [page, token]);

  useEffect(() => {
    if (token) {
      getBannerAds();
    }
  }, [token])

  const handleLoadMore = () => {
    if(loadMore){
        setPage(page + 1)
    }
}

  const getBannerAds = async () => {
    const response = await fetchGetByToken(
      apiUrl.warehouseDetailAds,
      token
    );

    if (response && response.success && response.data && response.data.length > 0) {
      setBanners(response.data);
    }
  }

  const getRicePrices = async (pageNo) => {

    setMoreLoading(true)
    let param = '';
    if (startDate && startDate !== '') {
      param += `&start_date=${startDate}`;
    }
    if (endDate && endDate !== '') {
      param += `&end_date=${endDate}`;
    }

    const response = await fetchGetByToken(
      `${apiUrl.ricePriceList}?page=${pageNo}&user_id=${item.id}${param}`,
      token
    );

    if (response?.success) {
      setPkgError(false)
      if (response.data?.length === 0 && pageNo === 1) {
        setError(local.noData)
      } else {
        setError("")

        const newList = pageNo > 1 ? priceList?.concat(response.data) : response.data;
        setPriceList(newList);

        if(response.nextPageUrl){
          setLoadMore(true);
        }else{
          setLoadMore(false);
        }
      }
    } else {
      setError(response?.message ?? local.noData);
      
      if (response?.status === 403) {
        setPkgError(true);
      } else {
        setPkgError(false)
      }

    }
    setMoreLoading(false)
    setLoading(false)
  }

  const backHandler = () => {
    if (fromHome) {
      navigation.replace("WarehouseSc")
    } else {
      navigation.goBack();
    }
  }

  const itemHandler = (riceItem) => {
    navigation.navigate("WarehouseRiceDetail", {item : riceItem, title: item.name + " - " + riceItem.name})
  }

  const chatHandler = async () => {
    setChatLoading(true)

    const data = {
      friend_id : item.id
    };

    const response = await fetchPostByToken(apiUrl.createConversation, data, token);

    setChatLoading(false);
    if (response?.success && response.channel_id) {
      const chat = {
        channel_id : response.channel_id,
        friend_name: item.name,
        friend_id : item.id
      }
      stackNavigation.navigate('ChatStack', {
        screen: 'ChatDetailScreen',
        params: {chat}
      });
    } else {
      ToastAndroid.show(response.message ?? local.fail, ToastAndroid.SHORT)
    }
  }

  const reviewHandler = () => {
    setReviewModalOpened(true);
  }

  const onSubmitReview = async (rating, review) => {
    setReviewModalOpened(false);
    
    setLoading(true);

    const data = {
      user_id: item.id,
      message: review,
      rating,
      reviwer_id: userData?.id
    };

    const response = await fetchPostByToken(apiUrl.newReview, data, token);

    setLoading(false);
    if (response?.success) {
      ToastAndroid.show(response.message ?? local.successful, ToastAndroid.SHORT)
    } else {
      ToastAndroid.show(response.message ?? local.fail, ToastAndroid.SHORT)
    }
  }

  const onSubmitDate = (start, end) => {
    setDatePicker(false);
    setStartDate(start);
    setEndDate(end);
  }

  const handleBuy = () => {
    navigation.navigate('Package');
  }

  const reloadData = async () => {
    setLoading(true);
    getRicePrices(1);
  }

  const bannerHandler = async (item) => {
    if (item?.type === "phone") {
      Linking.openURL(`tel:${item.value}`)
    } else if (item?.type === "website") {
      Linking.openURL(item.value)
    } else if (item?.type === "facebook") {
      Linking.openURL("https://www.facebook.com/profile.php?" + item.value);
    } else if (item?.type === "user") {
      setLoading(true);

      const data = {
        user_id: item.value
      };
  
      const response = await fetchPostByToken(apiUrl.getUserDetail, data, token);

      setLoading(false);
      if (response?.success && response.user) {
        if (response.user.user_type_id === 1) { // Warehouse
          stackNavigation.navigate('WarehouseStack', {
            screen: 'RiceList',
            params: {item : response.user, fromHome : true}
          });
         } else { // Rice Mill
          stackNavigation.navigate('RiceMillStack', {
            screen: 'RiceList',
            params: {item : response.user, fromHome : true}
          });
         }
      } else {
        ToastAndroid.show(response.message ?? local.fail, ToastAndroid.SHORT)
      }
    }
  }

  const userDetailHandler = () => {
    navigation.navigate('UserDetail', {item, mill : true})
  }
  
  return <MainApplicationContainer>
    <MainApplicationBody>
      <View
        style={ styles.container }>
        <Header 
          title={item.name}
          backHandler={backHandler}
          chat={userData?.id !== item.id ? true : false}
          chatHandler={chatHandler}
          review={true}
          reviewHandler={reviewHandler}
          userDetailHandler={userDetailHandler} />

        <DatePickerButton
          width={ wp(94) }
          startDate={ startDate }
          endDate={ endDate }
          pressHandler={() => setDatePicker(true)} />

        {banners && banners.length > 0 && <Banner 
          home={false}
          data={banners}
          bannerHandler={bannerHandler} />}

      {!loading && <>
        {!!error ? <View style={{marginTop : hp(-10)}}>
            <NoPackage
              text={error}
              pkgError={pkgError}
              buyHandler={handleBuy}
              reloadHandler={reloadData} />
        </View> : 
          <FlatList
            style={ styles.list }
            onEndReached={handleLoadMore}
            onEndReachedThreshold={1}
            ListFooterComponent={moreLoading && <Loading />}
            alwaysBounceVertical={false}
            showsVerticalScrollIndicator={false}
            data={priceList} 
            keyExtractor={(item) => item.id.toString()}
            renderItem={({item})=>{
              return <RiceItem
                item={ item }
                itemHandler={() => itemHandler(item)} />;
            }}
            />
            }
      </>}
      </View>
      {reviewModalOpened && <ReviewModal
        defaultRating={item.rating ? parseInt(item.rating) : 0}
        name={item.name}
        onClose={()=> setReviewModalOpened(false)}
        onSubmit={onSubmitReview} />}

      <Loading visible={loading} />

      <Loading visible={chatLoading} />

      <DateRangePicker 
        visible={datePicker}
        onClose={()=> setDatePicker(false)}
        onSubmit={onSubmitDate} />

    </MainApplicationBody>
  </MainApplicationContainer>;
};

export default memo(RiceListScreen);
