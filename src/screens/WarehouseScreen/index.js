import React, {
  memo,
  useContext,
  useEffect,
  useState,
  useCallback
} from "react";
import {
  FlatList,
  StatusBar,
  BackHandler,
  Linking,
  RefreshControl,
  View,
  ScrollView,
  DeviceEventEmitter,
  ToastAndroid
} from "react-native";
import { useNavigation } from '@react-navigation/native';
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { FAB } from "react-native-paper";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {
  WarehouseScreen as styles
} from "assets/style";
import { Context } from "context/Provider";
import apiUrl from "utils/apiUrl";
import { fetchGetByToken, fetchGet, fetchPostByToken } from "utils/fetchData";
import Colors from "assets/colors";
import {useLocal} from '../../hook/useLocal';

import Header from "components/Header";
import ShopItem from "components/ShopItem";
import FilterModal from "components/FilterModal";
import Loading from "components/Loading";
import NoPackage from "components/NoPackage";
import Banner from "components/Banner";

const WarehouseScreen = ({ navigation }) => {

  const {
    token
  } = useContext(Context);

  const local = useLocal();
  const stackNavigation = useNavigation();

  const [
    refreshing,
    setRefreshing
  ] = useState(false)
  const [
    showFilter,
    setShowFilter
  ] = useState(false);
  const [
    loading,
    setLoading
  ] = useState(false)
  const [
    warehouseList,
    setWarehouseList
  ] = useState();
  const [
    error,
    setError
  ] = useState()
  const [
    pkgError,
    setPkgError
  ] = useState(false);
  const [
    banners,
    setBanners
  ] = useState();
  const [
    states,
    setStates
  ] = useState([]);
  const [
    townships,
    setTownships
  ] = useState([]);
  const [
    filterName,
    setFilterName
  ] = useState();
  const [
    filterState,
    setFilterState
  ] = useState();
  const [
    filterTownship,
    setFilterTownship
  ] = useState();


  const [page, setPage] = useState(1);
  const [loadMore, setLoadMore] = useState(false);
  const [moreLoading, setMoreLoading] = useState(false);

  const onRefresh = useCallback(() => {
    setRefreshing(true);

    const reloadData = async () => {
      setWarehouseList([]);
      setPage(1);

      setLoading(true)
      getWarehouseList();
    };

    reloadData();
}, [token, filterName, filterState, filterTownship]);

  useEffect(() => {
    const getStatesAndDivision = async () => {
      const response = await fetchGet(apiUrl.statesAndTownships);
      if (response?.success) {
        setStates(response.states ?? []);
        setTownships(response.townships ?? [])
      }
    }
    getStatesAndDivision();
  }, []);

  const backButtonHandler = useCallback(() => {
    stackNavigation.navigate('HomeStack', {
      screen : 'HomeScreen'
    });
    return true;
  }, []);

  useEffect(()=> {
    const eventEmitter = DeviceEventEmitter.addListener(
        'package_change',
        val => {
          if (token) {
            setPage(1);
            setWarehouseList([])
            setLoading(true)
            getBannerAds()
            getWarehouseList();
          }
        },
      );
      return () => eventEmitter;
}, [token, filterName, filterState, filterTownship])

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, [backButtonHandler]);

  useEffect(() => {
    getBannerAds()
  }, [token])

  useEffect(() => {

    if (page > 1) {
      getWarehouseList()
    }  else {

      setLoading(true)
      
      getWarehouseList();
    }

}, [page, token, filterName, filterState, filterTownship]);

  const handleLoadMore = () => {
    if(loadMore){
        setPage(page + 1)
    }
}

  const getBannerAds = async () => {
    const response = await fetchGetByToken(
      apiUrl.warehouseListAds,
      token
    );
    if (response && response.success && response.data && response.data.length > 0) {
      setBanners(response.data);
    }
  }

  const getWarehouseList = async () => {
    setMoreLoading(true);
    let filterParam = '';
    if (filterName) {
      filterParam += `&user_name=${filterName}`;
    }
    if (filterTownship) {
      filterParam += `&township_id=${filterTownship.id}`;
    }
    if (filterState) {
      filterParam += `&state_id=${filterState.id}`;
    }


    let idArray = [];
    if (warehouseList && warehouseList.length > 0) {
      idArray = warehouseList.map(item => item.id);
    }

    const reqData = `${apiUrl.userList}?page=${page}&user_type_id=${1}&user_id_arr=[${idArray}]${filterParam}`;

    const response = await fetchGetByToken(
      reqData,
      token
    );

    if (response?.success) {
      setPkgError(false)

      if (response.data?.length === 0 && page === 1) {
        setError(local.noData)
      } else {
        setError("")
        const newList = page > 1 ? warehouseList?.concat(response.data) : response.data;
        setWarehouseList(newList);

        if(response.nextPageUrl){
          setLoadMore(true);
        }else{
          setLoadMore(false);
        }
      }
    } else {
      setError(response?.message ?? local.noData);
      if (response?.status === 403) {
        setPkgError(true);
      } else {
        setPkgError(false)
      }
    }

    setLoading(false)
    setMoreLoading(false)
    setRefreshing(false)
  }

  const onFilterHandler = () => {
    setShowFilter(true);
  };

  const onSubmitFilter = async (name, state, township) => {
    setShowFilter(false);
    setFilterName(name);
    setFilterState(state)
    setFilterTownship(township)
    setPage(1);
    setWarehouseList([]);
  }

  const chatHandler = () => {
    navigation.navigate('ChatStack')
  }

  const itemHandler = (item) => {
    navigation.navigate("RiceList", {item})
  }

  const profileHandler = (item) => {
    navigation.navigate("RiceList", {item})
    // navigation.navigate('UserDetail', {item, mill : false})
  }

  const handleBuy = () => {
    navigation.navigate('Package');
  }

  const reloadData = async () => {
    setLoading(true)
    getWarehouseList();
  }

  const bannerHandler = async  (item) => {
    if (item?.type === "phone") {
      Linking.openURL(`tel:${item.value}`)
    } else if (item?.type === "website") {
      Linking.openURL(item.value)
    } else if (item?.type === "facebook") {
      Linking.openURL("https://www.facebook.com/profile.php?" + item.value);
    } else if (item?.type === "user") {

      setLoading(true);

      const data = {
        user_id: item.value
      };
  
      const response = await fetchPostByToken(apiUrl.getUserDetail, data, token);

      setLoading(false);
      if (response?.success && response.user) {
        if (response.user.user_type_id === 1) { // Warehouse
          stackNavigation.navigate('WarehouseStack', {
            screen: 'RiceList',
            params: {item : response.user, fromHome : true}
          });
         } else { // Rice Mill
          stackNavigation.navigate('RiceMillStack', {
            screen: 'RiceList',
            params: {item : response.user, fromHome : true}
          });
         }
      } else {
        ToastAndroid.show(response.message ?? local.fail, ToastAndroid.SHORT)
      }
    }
  }

  return <View style={styles.container}>
  <ScrollView
        refreshControl={
          <RefreshControl
              refreshing={ refreshing }
              colors={[ Colors.light_green ]}
              onRefresh={ onRefresh } 
              progressViewOffset={hp(4)} />
        }>
        <Header
          chatHandler={chatHandler}
          notiHandler={()=> navigation.navigate('NotificationScreen')}
          newsHandler={()=> navigation.navigate('KnowledgeStack')} />
          <StatusBar backgroundColor='#f2f2f2' barStyle='dark-content' />

          {banners && banners.length && <Banner 
            home={false}
            data={banners}
            bannerHandler={bannerHandler} />}

          {!loading && <>
            {!!error ? <View style={{marginTop : hp(-10)}}><NoPackage
                text={error}
                pkgError={pkgError}
                buyHandler={handleBuy}
                reloadHandler={reloadData} /></View> : <FlatList
                  style={ styles.list }
                  alwaysBounceVertical={false}
                  showsVerticalScrollIndicator={false}
                  data={warehouseList} 
                  onEndReached={handleLoadMore}
                  onEndReachedThreshold={1}
                  ListFooterComponent={moreLoading && <Loading />}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={({item})=>{
                    return <ShopItem
                      item={ item }
                      itemHandler={() => itemHandler(item)}
                      profileHandler={() => profileHandler(item)} />;
                  }}
                  />}
            
          </>}

          {showFilter ? <FilterModal
            defaultState={ filterState }
            defaultTownship={ filterTownship }
            defaultName={ filterName }
            states={ states }
            allTownships={ townships }
            onClose={()=> setShowFilter(false)}
            onSubmit={onSubmitFilter} /> 
            : null}

          <Loading visible={loading} />
      </ScrollView>
      {!pkgError && <View style={styles.fabContent}>
        <FAB
          icon={({ size, color }) => <Icon name="filter" size={size} color={color} />} 
          onPress={onFilterHandler}
          style={styles.fabIcon}
          color="white"
        />
      </View>}
    </View>
};

export default memo(WarehouseScreen);
