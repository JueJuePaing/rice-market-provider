import React, {
  useState, 
  memo, 
  useRef,
  useEffect,
  useCallback
} from "react";
import {
  View,
  StatusBar,
  BackHandler,
  TextInput,
  ToastAndroid
} from "react-native";
import {
  Button,
  HelperText,
  DefaultTheme
} from "react-native-paper";
import Icon from "react-native-vector-icons/MaterialIcons";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";

import {
  ChangePasswordScreen as styles
} from "assets/style";
import { useLocal } from "../../hook/useLocal";
import { fetchPostByToken } from "utils/fetchData";
import apiUrl from "utils/apiUrl";

import Loading from "components/Loading";
import Header from "components/Header/DetailHeader";

const ChangePasswordScreen = ({ navigation, route }) => {

  const {token, forgetPassword} = route.params;
  const local = useLocal();
  
  const backButtonHandler = useCallback(() => {
    navigation.goBack();
    return true;
  }, []);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, [backButtonHandler]);

  const [
    oldPassword,
    setOldPassword
  ] = useState("");
  const [
    newPassword,
    setNewPassword
  ] = useState("");
  const [
    confirmPassword,
    setConfirmPassword
  ] = useState("");
  const [
    loading, 
    setLoading
  ] = useState(false);
  const [
    showOldPassword, 
    setShowOldPassword
  ] = useState(false);
  const [
    showNewPassword, 
    setShowNewPassword
  ] = useState(false);
  const [
    showConfirmPassword, 
    setShowConfirmPassword
  ] = useState(false);
  const [
    oldPwError, 
    setOldPwError
  ] = useState("");
  const [
    newPwError, 
    setNewPwError
  ] = useState("");
  const [
    confirmPwError, 
    setConfirmPwError
  ] = useState('');
  const [
    error,
    setError
  ] = useState('')

  const _isMounted = useRef(false);

  const theme = {
    ...DefaultTheme,
    colors : {
      ...DefaultTheme.colors,
      primary : "#87ad2b"
    }
  };

  useEffect(() => {
    _isMounted.current = true;

    return function clean() {
      _isMounted.current = false;
    };
  }, []);

  const handleOldPwChange = (pw) => {
    setOldPwError("");
    setOldPassword(pw);
  };

  const handleNewPwChange = (pw) => {
    setNewPwError("");
    setNewPassword(pw);
  };

  const handleConfirmPwChange = (pw) => {
    setConfirmPwError('');
    setConfirmPassword(pw);
  };

  const handleChangePassword = async () => {

    let valid = true;
    if (!oldPassword && !forgetPassword) {
      setOldPwError(local.invalidOldPassword)
      valid = false
    }
    if (!newPassword) {
      setNewPwError(true)
      valid = false
    }
    if (!confirmPassword) {
      setConfirmPwError(local.invalidConfirmPassword)
      valid = false
    } else if (newPassword !== confirmPassword) {
      setConfirmPwError(local.mismatchPassword)
      valid = false
    }

    if (!valid) return

    setLoading(true);

    if (!forgetPassword) {
      // Confirm old password first
      const data = {
        password: oldPassword
      };
  
      const response = await fetchPostByToken(apiUrl.confirmOldPassword, data, token);
  
      if (response?.success) {
        changePassword();
      } else {
        setLoading(false);
        setOldPwError(response?.message ?? local.fail);
      }
    } else {
      changePassword();
    }
  };

  const changePassword = async () => {
    const data = {
      password: newPassword,
      password_confirmation: confirmPassword
    };

    const response = await fetchPostByToken(apiUrl.changePassword, data, token);

    setLoading(false);
    if (response?.success) {
      ToastAndroid.show(response.message ?? local.successful, ToastAndroid.SHORT)
      if (forgetPassword) {
        navigation.replace('Login')
      } else {
        navigation.goBack();
      }
    } else {
      if (response.errors && response.errors.password) {
        setNewPwError(response.errors.password)
      } 
      if (response.errors && response.errors.password_confirmation) {
        setConfirmPwError(response.errors.password_confirmation)
      }
      if (!response.errors || !response.errors.password || !response.errors.password_confirmation) {
        setError(response?.message ?? local.fail);
      }
    }
  }

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor='#f2f2f2' />
      <Header
        title={local.changePassword}
        backHandler={()=> navigation.goBack()} />
      <View style={styles.content}>
        
        {!forgetPassword && <View style={styles.inputContainer}>
          <Icon name="lock" size={hp(2.2)} style={styles.inputIcon} />
          <View style={styles.inputWrapper}>
            <TextInput
              placeholder={local.oldPassword}
              value={oldPassword}
              onChangeText={handleOldPwChange}
              secureTextEntry={!showOldPassword}
              style={styles.input}
              placeholderTextColor='gray'
              underlineColor="transparent"/>
          </View>
          <Icon
            name={!showOldPassword ? "visibility-off" : "visibility"}
            size={hp(2.2)}
            onPress={() => setShowOldPassword(!showOldPassword)}
            style={styles.inputIcon}/>
        </View>}
        {!!oldPwError && (
          <HelperText type="error" style={styles.errorTxt}>
            {oldPwError}
          </HelperText>
        )}

        <View style={styles.inputContainer}>
          <Icon name="lock" size={hp(2.2)} style={styles.inputIcon} />
          <View style={styles.inputWrapper}>
            <TextInput
              placeholder={local.newPassword}
              value={newPassword}
              onChangeText={handleNewPwChange}
              secureTextEntry={!showNewPassword}
              style={styles.input}
              underlineColor="transparent"
              placeholderTextColor='gray' />
          </View>
          <Icon
            name={!showNewPassword ? "visibility-off" : "visibility"}
            size={hp(2.2)}
            onPress={() => setShowNewPassword(!showNewPassword)}
            style={styles.inputIcon}/>
        </View>
        {!!newPwError && (
          <HelperText type="error" style={styles.errorTxt}>
            {local.invalidNewPassword}
          </HelperText>
        )}

        <View style={styles.inputContainer}>
          <Icon name="lock" size={hp(2.2)} style={styles.inputIcon} />
          <View style={styles.inputWrapper}>
            <TextInput
              placeholder={local.confirmPassword}
              value={confirmPassword}
              onChangeText={handleConfirmPwChange}
              secureTextEntry={!showConfirmPassword}
              style={styles.input}
              underlineColor="transparent"
              placeholderTextColor='gray' />
          </View>
          <Icon
            name={!showConfirmPassword ? "visibility-off" : "visibility"}
            size={hp(2.2)}
            onPress={() => setShowConfirmPassword(!showConfirmPassword)}
            style={styles.inputIcon}/>
        </View>
        {!!confirmPwError && (
          <HelperText type="error" style={styles.errorTxt}>
            {confirmPwError}
          </HelperText>
        )}
      
        {!!error && (
          <HelperText type="error" style={styles.errorTxt}>
            {error}
          </HelperText>
        )}
        <Button
          mode="contained"
          theme={theme}
          onPress={handleChangePassword}
          style={styles.button}
          labelStyle={{fontSize: hp(1.8)}}
          disabled={loading}>
          {local.changePassword}
        </Button>
      </View>

      <Loading visible={loading} />
    </View>)
};

export default memo(ChangePasswordScreen);
