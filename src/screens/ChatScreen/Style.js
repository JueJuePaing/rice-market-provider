import { StyleSheet } from "react-native";
import {
    heightPercentageToDP as hp, 
    widthPercentageToDP as wp
  } from "react-native-responsive-screen";

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor: '#f2f2f2'
    },
    noDataContent: {
        flex : 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    noData: {
        fontSize: hp(1.8),
        color: '#8b8f8b'
    },

    // Chat Detail
    detailContainer: {
        backgroundColor: '#fff',
        flex: 1,
        marginTop: hp(.7),
        borderTopLeftRadius: hp(5),
        borderTopRightRadius: hp(7),
        paddingTop: hp(2)
      },
    sending: {
        fontSize: hp(1.4),
        color: '#8b8f8b',
        right: wp(3),
        alignSelf: 'flex-end'
    },
    moreBtn: {
        alignSelf: 'center'
    },
    noMessageContent: {
        alignItems: 'center',
        justifyContent: 'center',
        height: hp(80)
    }
});

export default styles;