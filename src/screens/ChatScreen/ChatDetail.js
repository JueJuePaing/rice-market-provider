import React, {
    useState,
    useContext,
    useEffect,
    useCallback,
    useRef
} from "react";
import {
    View,
    Text,
    ToastAndroid,
    ScrollView,
    RefreshControl,
    PermissionsAndroid,
    DeviceEventEmitter,
    BackHandler,
    Platform
} from "react-native";
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import { Video } from 'react-native-compressor';

import styles from "./Style";
import { useLocal } from "../../hook/useLocal";
import { Context } from "context/Provider";
import apiUrl from "utils/apiUrl";
import { fetchPostByToken, fetchPostImageUpload } from "utils/fetchData";
import Colors from 'assets/colors';

import Header from "components/Header/DetailHeader";
import MessageSelfText from "components/Chat/MessageSelfText";
import MessageUserText from "components/Chat/MessageUserText";
import MessageSelfImage from "components/Chat/MessageSelfImage";
import MessageUserImage from "components/Chat/MessageUserImage";
import MessageSelfVideo from "components/Chat/MessageSelfVideo";
import MessageUserVideo from "components/Chat/MessageUserVideo";
import VideoFullScreen from "components/Chat/VideoFullScreen";
import MessageInput from "components/Chat/MessageInput";
import ImageFullScreen from "components/Chat/ImageFullScreen";
import Loading from "components/Loading";
import ChatModal from "components/ChatModal/ChatModal";

const ChatDetail = ({
    navigation,
    route
}) => {

    const {chat} = route.params;
    
    const local = useLocal();
    const {
        userInfo, 
        token
    } = useContext(Context);
    const userData = userInfo ? JSON.parse(userInfo) : null

    const scrollViewRef = useRef(null);
    
    const [
        videoDetail,
        setVideoDetail
    ] = useState();
    const [
        imgDetail,
        setImgDetail
    ] = useState();
    const [
        message,
        setMessage
    ] = useState("");
    const [
        sending,
        setSending
    ] = useState(false);
    const [
        chatMessages,
        setChatMessages
    ] = useState()
    const [
        refreshing,
        setRefreshing
      ] = useState(false)
    const [
        lastMessageId,
        setLastMessageId
    ] = useState();
    const [
        needToScroll,
        setNeedToScroll
    ] = useState(true);
    const [
        loading,
        setLoading
    ] = useState(true);
    const [
        friendInfo,
        setFriendInfo
    ] = useState();
    const [
        showChatImgModal,
        setShowChatImgModal
    ] = useState(false);
    const [
        showChatVideoModal,
        setShowChatVideoModal
    ] = useState(false);

    const backButtonHandler = useCallback(() => {
        navigation.goBack();
        return true;
      }, []);
    
      useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', backButtonHandler);
    
        return () => {
          BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
        };
      }, [backButtonHandler]);


    const requestCameraPermission = async () => {
        if (Platform.OS === 'android') {
          try {
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.CAMERA,
              {
                title: 'Camera Permission',
                message: 'App needs camera permission',
              },
            );
            // If CAMERA Permission is granted
            return granted === PermissionsAndroid.RESULTS.GRANTED;
          } catch (err) {
            console.warn(err);
            return false;
          }
        } else return true;
      };

    useEffect(() => {
        const eventListener = DeviceEventEmitter.addListener(
            'noti.chat_new_message',
            data => {

                const parsedNotiData = JSON.parse(data.data);

                if (chat && data && data.channel_id+'' === chat.channel_id+'') {
                    setChatMessages(prev => [...prev, parsedNotiData])
                } 
            }
        );
        return () => {
            eventListener.remove();
        };
    }, [chat]);

    useEffect(() => {
        if (token && chat.channel_id) {
            getChatMessages(chat.channel_id);
        } else {
            setLoading(false);
        }
    }, [token, chat])

    const getChatMessages = async (channelId) => {
        const data = {
          limit: 20,
          channel_id: channelId,
          last_message_id: 0
        };
    
        const response = await fetchPostByToken(
          apiUrl.getMessages,
          data,
          token
        );
    

        if (response?.success) {
          if (response.data && response.data.length > 0) {
            const adjustedData = response.data.reverse();
            setChatMessages(adjustedData)
            setLastMessageId(adjustedData[0].id)
          } 
        } else {
          ToastAndroid.show(response?.message ?? local.fail, ToastAndroid.SHORT)
        }
    
        setLoading(false);
      }


    useEffect(() => {
        if (token && chat.friend_id) {
            getFriendDetail()
        }
    }, [token, chat]);

    const getFriendDetail = async () => {
        const data = {
            user_id: chat.friend_id
        };
    
        const response = await fetchPostByToken(apiUrl.getUserDetail, data, token);

        if (response?.success && response.user) {
            setFriendInfo(response.user);
        }
    }

    const handleContentSizeChange = (contentWidth, newContentHeight) => {
        if (needToScroll) {
            scrollViewRef.current.scrollToEnd({ animated: true });
        } else {
            setNeedToScroll(true);
        }
    };

  const onRefresh = useCallback(() => {
    setRefreshing(true);
    setNeedToScroll(false);

    const reloadData = async () => {
      getMoreMessages();
    };

    reloadData();
}, [token, lastMessageId]);


const getMoreMessages = async () => {
    const data = {
      limit: 20,
      channel_id: chat.channel_id,
      last_message_id: lastMessageId
    };

    const response = await fetchPostByToken(
      apiUrl.getMessagesUp,
      data,
      token
    );

    if (response?.success) {
      if (response.data && response.data.length > 0) {
        setLastMessageId(response.data[response.data.length - 1].id)
        setChatMessages([...response.data.reverse(), ...chatMessages]);
      } 
    } else {
      ToastAndroid.show(response?.message ?? local.fail, ToastAndroid.SHORT)
    }

    setRefreshing(false)
  }

    const itemHandler = (item) => {
        if (item.type === "image") {
            setImgDetail(item)
        }
    };

    const playVideoHandler = (item) => {
        setVideoDetail(item);
    }

    

    const videoRecordHandler = async () => {
        const options = {
            mediaType: 'video',
            // videoQuality: 'high', // Set video quality (high, medium, low, 640x480, 1280x720, etc.)
            durationLimit: 30,    // Maximum duration in seconds
          };
      
        let isCameraPermitted = await requestCameraPermission();

        if (isCameraPermitted) {
            launchCamera(options, response => {
                if (response.didCancel) {
                  console.log('User cancelled video recording');
                } else if (response.error) {
                  console.log('Video recording error:', response.error);
                } else {
                    if (response.assets && response.assets.length > 0) {
                        compressVideo(response.assets[0]);
                    }
                    else {
                        ToastAndroid.show(local.fail, ToastAndroid.SHORT);
                    }
                }
              });
        }
    }

    const imageHandler = () => {
        setShowChatImgModal(true)
    }

    const  videoHandler = () => {
        setShowChatVideoModal(true)
    }

    const chooseImgHandler = (val) => {
        setShowChatImgModal(false);
        if (val === 1) {
            takeCameraHandler()
        } else {
            galleryHandler('photo');
        }
    }

    const chooseVideoHandler = (val) => {
        setShowChatVideoModal(false);
        if (val === 1) {
            videoRecordHandler()
        } else {
            galleryHandler('video');
        }
    }

    const takeCameraHandler = async () => {
        let options = {
            mediaType: 'photo',
            maxWidth: 500,
            maxHeight: 500,
            quality: 1,
            saveToPhotos: true,
        };
        let isCameraPermitted = await requestCameraPermission();
    
        if (isCameraPermitted) {
            launchCamera(options, response => {
                if (response.didCancel) {
                  console.log('User cancelled camera');
                } else if (response.error) {
                  console.log('Camera error:', response.error);
                } else {
                    if (response.assets && response.assets.length > 0) {
                        sendMediaHandler(response.assets[0], 'image')
                    }
                    else {
                        ToastAndroid.show(local.fail, ToastAndroid.SHORT);
                    }
                }
              });
        }
    }

    const galleryHandler = (type) => {
        launchImageLibrary({
            selectionLimit : 1,
            mediaType : type
        }, response => {
            if (response?.didCancel !== true) {
              if (response && response.assets && response.assets.length > 0) {
                if (response.assets[0].type.includes('image'))
                    sendMediaHandler(response.assets[0], 'image')
                else if (response.assets[0].duration > 30) 
                    ToastAndroid.show(local.invalidVideoDuration, ToastAndroid.SHORT);
                else 
                    compressVideo(response.assets[0]);
              } 
            }
          });
    }

    const compressVideo = async (file) => {
        setSending(true);
        const result = await Video.compress(
            file.uri,
            {
              compressionMethod: 'auto',
            },
            (progress) => {
                console.log('Compression Progress: ', progress);
            }
          );
        if (result) {
            sendMediaHandler({...file, uri : result}, 'video')
        } else {
            setSending(false);
        }
    }

    const sendMediaHandler = async (media, type) => {

        setSending(true);

        const uploadData = new FormData();
        uploadData.append('type', type);
        uploadData.append('channel_id', chat.channel_id);
        uploadData.append('message', {
            type: media.type,
            uri: media.uri,
            name: media.fileName,
            width: media.width,
            height: media.height
        });

        const response = await fetchPostImageUpload(
            apiUrl.sendMessage,
            uploadData,
            token
        );

        if (response?.success) {
            const latestMessage = {
                id : 0,
                message : '',
                sender_id: userData.id,
                type,
                file_name: media.uri,
                date_time: '1 second ago'
            }
            const data = [...chatMessages, latestMessage]
            setChatMessages(data);
        } else {
            ToastAndroid.show(response?.message ?? local.fail, ToastAndroid.SHORT)
        }

        setSending(false)
    }

    const sendTextHandler = async () => {
        if (message) {
            setSending(true);

            const data = {
                message,
                type: 'text',
                channel_id: chat.channel_id
              };
          
              const response = await fetchPostByToken(
                apiUrl.sendMessage,
                data,
                token
              );
          
       
              if (response?.success) {
                const latestMessage = {
                    id : 0,
                    message,
                    sender_id: userData.id,
                    type: 'text',
                    file_name: '',
                    date_time: '1 second ago'
                }
                let data;
                if (chatMessages) {
                    data = [...chatMessages, latestMessage]
                } else {
                    data = [latestMessage]
                }
                setChatMessages(data);
                setMessage('')
              } else {
                ToastAndroid.show(response?.message ?? local.fail, ToastAndroid.SHORT)
              }

            setSending(false)
        }
    }

    const infoHandler = () => {
        navigation.navigate('ChatInfoScreen', {chat})
    }

    return  <>
        <Header
            noMarquee={true}
            title={chat.friend_name ? chat.friend_name : friendInfo ? friendInfo.name : ''}
            info={true}
            infoHandler={infoHandler}
            backHandler={()=> navigation.goBack()} />
        <View style={styles.detailContainer}>
            <ScrollView
                ref={scrollViewRef}
                showsVerticalScrollIndicator={false}
                onContentSizeChange={handleContentSizeChange}
                refreshControl={
                    <RefreshControl
                        refreshing={ refreshing }
                        colors={[ Colors.light_green ]}
                        onRefresh={ onRefresh } />
                  }>
            {chatMessages ? 
                <>
                {chatMessages.map((item) => {
                    if (item.type === "text" && item.sender_id === userData.id) 
                        return <MessageSelfText 
                            item={item}
                            itemHandler={() => itemHandler(item)} />
                    else if (item.type === "text" && item.sender_id !== userData.id) 
                        return <MessageUserText 
                            profile={chat.profile ? chat.profile : friendInfo ? friendInfo.image : null}
                            item={item}
                            itemHandler={() => itemHandler(item)} />
                    else if (item.type === "image" && item.sender_id === userData.id) 
                        return <MessageSelfImage 
                            item={item}
                            itemHandler={() => itemHandler(item)} />
                    else if (item.type === "image" && item.sender_id !== userData.id) 
                        return <MessageUserImage 
                            profile={chat.profile ? chat.profile : friendInfo ? friendInfo.image : null}
                            item={item}
                            itemHandler={() => itemHandler(item)} />
                    else if (item.type === "video" && item.sender_id === userData.id) 
                        return <MessageSelfVideo
                            item={item}
                            itemHandler={() => itemHandler(item)}
                            playVideoHandler={()=> playVideoHandler(item)} />
                    else if (item.type === "video" && item.sender_id !== userData.id) 
                        return <MessageUserVideo
                            profile={chat.profile ? chat.profile : friendInfo ? friendInfo.image : null}
                            item={item}
                            itemHandler={() => itemHandler(item)}
                            playVideoHandler={()=> playVideoHandler(item)} />
                })}
            </> : !loading ? <View style={styles.noMessageContent}>
                <Text style={styles.noData}>{local.noMessage}</Text>
            </View> : null}
            </ScrollView>

            {sending && <Text style={styles.sending}>{local.sending}</Text>}
            <MessageInput
                sending={sending}
                message={message}
                changeMessage={setMessage}
                cameraHandler={imageHandler}
                sendHandler={sendTextHandler}
                videoRecordHandler={videoHandler} />
        </View>

        {videoDetail && <VideoFullScreen 
            item={videoDetail}
            onClose={()=> setVideoDetail(null)} />}

        {imgDetail && <ImageFullScreen 
            item={imgDetail}
            onClose={()=> setImgDetail(null)} />}

        {showChatImgModal && <ChatModal
            visible={true}
            image={true}
            onClose={()=> setShowChatImgModal(false)}
            chooseImgHandler={chooseImgHandler}
        />}

        {showChatVideoModal && <ChatModal
            visible={true}
            image={false}
            onClose={()=> setShowChatVideoModal(false)}
            chooseVideoHandler={chooseVideoHandler}
        />}

        <Loading visible={loading} />
    </> 
};

export default ChatDetail;
