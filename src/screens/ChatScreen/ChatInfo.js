import React, {
    useState,
    useContext,
    useEffect
} from "react";
import {
    View,
    Text,
    FlatList,
    TouchableOpacity,
    Image
} from "react-native";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from "react-native-vector-icons/AntDesign";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";

import { useLocal } from "hook/useLocal";
import { Context } from "context/Provider";
import apiUrl from "utils/apiUrl";
import { fetchPostByToken } from "utils/fetchData";
import Colors from "assets/colors";
import { ChatInfoScreen as styles } from "assets/style";

import Header from "components/Header/DetailHeader";
import Loading from "components/Loading";
import VideoFullScreen from "components/Chat/VideoFullScreen";
import ImageFullScreen from "components/Chat/ImageFullScreen";

const ChatInfo = ({ navigation, route }) => {

    const {chat} = route.params;
    const local = useLocal();
    const {
        token
    } = useContext(Context);

    const [
        loading,
        setLoading
    ] = useState(true)
    const [
        friendInfo,
        setFriendInfo
    ] = useState();
    const [
        files,
        setFiles
    ] = useState();
    const [
        videoDetail,
        setVideoDetail
    ] = useState();
    const [
        imgDetail,
        setImgDetail
    ] = useState();

    useEffect(() => {
        if (token && chat.friend_id) {
            getFriendDetail()
        } 
    }, [token, chat]);

    useEffect(() => {
        if (token && chat.channel_id) {
            getFiles()
        } else {
            setLoading(false)
        }
    }, [token, chat]);


    const playVideoHandler = (item) => {
        setVideoDetail(item);
    }
    
    const getFriendDetail = async () => {

        const data = {
            user_id: chat.friend_id
        };
    
        const response = await fetchPostByToken(apiUrl.getUserDetail, data, token);
      
        if (response?.success && response.user) {
            setFriendInfo(response.user);
          
        }
    }

    const getFiles = async () => {
      
        const data = {
            channel_id : chat.channel_id,
            last_message_id : 0,
            limit : 50
        };

        const response = await fetchPostByToken(apiUrl.getChatFiles, data, token);

        if (response?.success && response.data && response.data.length > 0) {
   
            setFiles(response.data)
            setLoading(false)
            // let adjustedFiles = response.data;

            // // Use Promise.all to handle asynchronous operations
            // const thumbnailPromises = adjustedFiles.map((item) => {
            //     if (item.type === "video") {
            //         return createThumbnail({
            //             url: item.file_name,
            //             timeStamp: 1000,
            //         })
            //         .then((response) => {
            //             if (response.path) {
            //                 return { ...item, thumbnail: response.path };
            //             } else {
            //                 return item;
            //             }
            //         })
            //         .catch((err) => {
            //             console.log({ err });
            //             return item;
            //         });
            //     } else {
            //         return Promise.resolve(item);
            //     }
            // });

            // Promise.all(thumbnailPromises)
            //     .then((adjustedFilesWithThumbnails) => {
            //         setFiles(adjustedFilesWithThumbnails);
            //         setLoading(false);
            //     })
            //     .catch((error) => {
            //         console.log("Error while creating thumbnails: ", error);
            //         setLoading(false);
            //     });


        } else {
            setLoading(false)
        }
    }

    const renderVideoItem = ({ item }) => {
        if (item.type === "video")
            return (
                <TouchableOpacity 
                    onPress={ () => playVideoHandler(item)}
                    style={styles.mediaItem}>
                    <AntDesign
                        style={{position: 'absolute'}}
                        name={'play'}
                        size={hp(4)}
                        color="#fff" />
                </TouchableOpacity>
            );
        else if (item.type === "image") 
            return (
                <TouchableOpacity 
                    onPress={()=> setImgDetail(item)}
                    style={styles.mediaItem}>
                    <Image 
                        style={styles.mediaFile}
                        source={{ uri : item.file_name }} />
                </TouchableOpacity>
            );
        return null;
      };

  return <View style={styles.container}>
    <Header backHandler={()=> navigation.goBack()} />
    <View style={styles.content}>
        <View style={styles.profileContent}>
            <Image
            style={styles.profile}
            source={{ uri : friendInfo ? friendInfo.image : null }} />
        </View>
        {friendInfo && <Text style={styles.friendName}>
            { friendInfo.name }
        </Text>}
        <View style={styles.row}>
            <FontAwesome
                name="phone"
                size={hp(2.5)}
                color={Colors.dark_orange} />
            <Text style={styles.value}>
                { friendInfo ? friendInfo.phone_number : '' }
            </Text>
        </View>
        <View style={styles.row}>
            <FontAwesome
                name="map-pin"
                size={hp(2.5)}
                color={Colors.dark_orange} />
            <Text style={styles.value}>
                { friendInfo ? friendInfo.address : local.notAvailable }
            </Text>
        </View>
        <View style={styles.mediaContent}>

            {!loading && <>
                {files && files.length > 0 ? <FlatList
                    data={files}
                    renderItem={renderVideoItem}
                    keyExtractor={item => item.id}
                    numColumns={3}
                    columnWrapperStyle={styles.list}
                    /> : <View style={styles.noDataContent}>
                    <Text style={styles.noData}>{local.noFile}</Text>
                </View>}
            </>}

        </View>
    </View>

    <Loading visible={loading} />
    
    {videoDetail && <VideoFullScreen 
            item={videoDetail}
            onClose={()=> setVideoDetail(null)} />}

    {imgDetail && <ImageFullScreen 
            item={imgDetail}
            onClose={()=> setImgDetail(null)} />}
  </View>
};

export default ChatInfo;
