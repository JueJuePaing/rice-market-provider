import React, { 
  useState,
  useContext,
  useEffect,
  useCallback
} from "react";
import {
  View, 
  StatusBar,
  FlatList,
  ToastAndroid,
  BackHandler,
  Linking,
  DeviceEventEmitter
} from "react-native";
import { useNavigation } from '@react-navigation/native';
import { heightPercentageToDP as hp } from "react-native-responsive-screen";

import { Context } from "context/Provider";
import apiUrl from "utils/apiUrl";
import { fetchPostByToken, fetchGetByToken } from "utils/fetchData";
import { useLocal } from "hook/useLocal";

import Header from "components/Header/DetailHeader";
import ChatItem from "components/Chat/ChatItem";
import NoPackage from "components/NoPackage";
import Loading from "components/Loading";
import Banner from "components/Banner";

const ChatScreen = ({ navigation }) => {
  const local = useLocal();
  const stackNavigation = useNavigation();

  const {
    token
  } = useContext(Context);

  const [
    loading,
    setLoading
  ] = useState(true);
  const [
    conversationList,
    setConversationList
  ] = useState();
  const [
    banners,
    setBanners
  ] = useState();

  const backButtonHandler = useCallback(() => {
    navigation.goBack();
    return true;
  }, []);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, [backButtonHandler]);

  useEffect(() => {
    const eventListener = DeviceEventEmitter.addListener(
        'noti.chat_new_message',
        data => {

            const parsedNotiData = JSON.parse(data.data);
            
            const filterConversation = conversationList?.filter(conversation => conversation.channel_id+'' !== data.channel_id+'');
            const updatedConversation = conversationList?.filter(conversation => conversation.channel_id+'' === data.channel_id+'');
            const updatedItem = {
              ...updatedConversation[0],
              last_text : parsedNotiData.type === "text" ? parsedNotiData.message :
                parsedNotiData.type === "image" ? `${data.title} sent an image.` :
                  parsedNotiData.type === "video" ? `${data.title} sent a video.` : "Welcome to Rices Market Provider"
            };
            setConversationList([updatedItem, ...filterConversation]);
        }
    );
    return () => {
        eventListener.remove();
    };
}, [conversationList]);

  useEffect(() => {
    if (token) {
      getConversationList();
      getBannerAds();
    } else {
      setLoading(false)
    }
  }, [token]);


  const getBannerAds = async () => {
    const response = await fetchGetByToken(
      apiUrl.messengerAds,
      token
    );
    if (response && response.success && response.data && response.data.length > 0) {
      setBanners(response.data);
    }
  }

  const getConversationList = async () => {
    setLoading(true)

    const data = {
      limit: 20,
      last_conversation_id: 0
    };

    const response = await fetchPostByToken(
      apiUrl.getConversations,
      data,
      token
    );

    if (response?.success) {
      if (response.data && response.data.length > 0) {
        setConversationList(response.data)
      }
    } else {
      ToastAndroid.show(response?.message ?? local.fail, ToastAndroid.SHORT)
    }

    setLoading(false);
  }

  const chatHandler = (chat) => {
    navigation.navigate('ChatDetailScreen', {chat})
  }

  const bannerHandler = async (item) => {

    if (item?.type === "phone") {
      Linking.openURL(`tel:${item.value}`)
    } else if (item?.type === "website") {
      Linking.openURL(item.value)
    } else if (item?.type === "facebook") {
      Linking.openURL("https://www.facebook.com/profile.php?" + item.value);
    } else if (item?.type === "user") {

      setLoading(true);

      const data = {
        user_id: item.value
      };
  
      const response = await fetchPostByToken(apiUrl.getUserDetail, data, token);

      setLoading(false);
      if (response?.success && response.user) {
        if (response.user.user_type_id === 1) { // Warehouse
          stackNavigation.navigate('WarehouseStack', {
            screen: 'RiceList',
            params: {item : response.user, fromHome : true}
          });
         } else { // Rice Mill
          stackNavigation.navigate('RiceMillStack', {
            screen: 'RiceList',
            params: {item : response.user, fromHome : true}
          });
         }
      } else {
        ToastAndroid.show(response.message ?? local.fail, ToastAndroid.SHORT)
      }
    }
  }

  return <View>
    <StatusBar 
      backgroundColor={"#f2f2f2"} 
      barStyle="dark-content" />
    <Header
      title="Chat"
      noMarquee={true}
      backHandler={() => navigation.goBack()} />

    {banners && banners.length && <Banner 
      home={false}
      data={banners}
      bannerHandler={bannerHandler} />}

    {conversationList ? <FlatList
        alwaysBounceVertical={false}
        style={{
          marginBottom : hp(23)
        }}
        showsVerticalScrollIndicator={false}
        data={conversationList} 
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item})=>{
        return <ChatItem 
            item={item}
            itemHandler={() => chatHandler(item)} />
        }} /> : !loading ? <NoPackage
        text={local.noData}
        reloadHandler={()=> getConversationList()} /> : null}   
    <Loading visible={loading} />
  </View>
};

export default ChatScreen;
