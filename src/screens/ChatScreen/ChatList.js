import React from "react";
import { FlatList } from "react-native";

import Header from "components/Header/DetailHeader";
import ChatItem from "components/Chat/ChatItem";
import { useLocal } from "../../hook/useLocal";
import NoPackage from "components/NoPackage";

const ChatList = ({
    loading,
    conversationList,
    onClose,
    chatHandler,
    reloadChatList
}) => {
    const local = useLocal();

    return  <>
        <Header
            title="Chat"
            backHandler={onClose} />
        {conversationList ? <FlatList
            alwaysBounceVertical={false}
            showsVerticalScrollIndicator={false}
            data={conversationList} 
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item})=>{
            return <ChatItem 
                item={item}
                itemHandler={() => chatHandler(item)} />
            }} /> : !loading ? <NoPackage
            text={local.noData}
            reloadHandler={reloadChatList} /> : null}     
        </> 
};

export default ChatList;
