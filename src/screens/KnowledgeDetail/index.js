import React, { 
  useState,
  useCallback,
  useEffect,
  useRef,
  useContext
} from "react";
import {
  View,
  TouchableOpacity,
  StatusBar,
  Image,
  Text,
  BackHandler
} from "react-native";
import { WebView } from 'react-native-webview';
import Colors from 'assets/colors';
import {
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import Video from 'react-native-video';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Slider from '@react-native-community/slider';
import {UIActivityIndicator} from "react-native-indicators";

import apiUrl from "utils/apiUrl";
import { fetchPostByToken } from "utils/fetchData";
import styles from './Style';
import { Context } from "context/Provider";
import { useLocal } from "hook/useLocal";

import Header from "components/Header/DetailHeader";
import Loading from "components/Loading";

const KnowledgeDetailScreen = ({ navigation, route }) => {

  const {item, knowledge_id} = route.params;
  const local = useLocal();
  const videoRef = useRef(null);
  const {
    token
  } = useContext(Context);

  const [
    loading,
    setLoading
  ] = useState(false);

  const [
    knowledgeData,
    setKnowledgeData
  ] = useState();

  const [
    duration,
    setDuration
  ] = useState(1);
  const [
    videoLoading,
    setVideoLoading
  ] = useState(true)
  const [isPlaying, setIsPlaying] = useState(true);
  const [currentTime, setCurrentTime] = useState(0);

  useEffect(() => {
    if (item) {
      setKnowledgeData(item)
    } else {
      getKnowledgeData(knowledge_id);
    }
  }, [item, knowledge_id])

  const getKnowledgeData = async(id) => {
    setLoading(true);

    const response = await fetchPostByToken(apiUrl.knowledgeDetail, {id}, token);

    if (response.success && response.data) {
      setKnowledgeData(response.data);
    }

    setLoading(false)
  }

  const onLoad = (data) => {
    setVideoLoading(false)
    setDuration(data.duration);
  };

  const backButtonHandler = useCallback(() => {
    navigation.goBack();
    return true;
  }, []);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, [backButtonHandler]);


  const handleProgress = (progress) => {
    setCurrentTime(progress.currentTime);
  };

  const togglePlayPause = () => {
      setIsPlaying(!isPlaying);

  };

  const handleSliderChange = (value) => {
    if (videoRef.current) {
      videoRef.current.seek(value);
      setCurrentTime(value);
    }
  };

  return <View style={styles.container}>
    <StatusBar 
        backgroundColor={"#fff"} 
        barStyle="dark-content" />

        <Header
          noMarquee={true}
          title={local.detail}
          color="#fff"
          noChat={true}
          backHandler={()=> navigation.goBack()} />

        {!loading && knowledgeData ? <View 
          activeOpacity={0.8}
          style={styles.item}>
          {knowledgeData.type === "image" ? <Image
              source={{
                uri : knowledgeData.image
              }}
              style={styles.newsImg} /> : knowledgeData.type === "video" ?  <View style={styles.videoContent}>
              <Video
                ref={videoRef}
                source={{
                  uri : knowledgeData.image
                }}
                controls={false} // enable player controls
                paused={!isPlaying}
                resizeMode="contain" 
                style={styles.video}
                onLoad={onLoad}   
                onEnd={()=> {
                  setIsPlaying(false)
                  setCurrentTime(0)
                  videoRef.current.seek(0);
                }} 
                onProgress={handleProgress} />
              {!videoLoading && <>
                <TouchableOpacity
                  activeOpacity={0.8}
                  style={styles.playButton}
                  onPress={togglePlayPause}>
                  <AntDesign
                    name={!isPlaying ? "play" : "pausecircleo"}
                    size={hp(4.5)}
                    color='lightgray' />
                </TouchableOpacity>
                <Slider
                  style={styles.slider}
                  minimumValue={0}
                  maximumValue={duration ? duration : 1}
                  value={currentTime}
                  onValueChange={handleSliderChange}
                  minimumTrackTintColor="#009688"
                  maximumTrackTintColor="#ccc"
                  thumbTintColor="#009688"
                />
              </>}
        
              {videoLoading && <UIActivityIndicator 
                style={{position: 'absolute'}} 
                color={Colors.light_yellow} size={35} />}</View> : <View style={styles.videoBg} />}
          <Text style={[styles.title, {marginBottom : 10}]}>
              {knowledgeData.title}
          </Text>

          <WebView
              originWhitelist={['*']}
              source={{ html: knowledgeData.description }}
              style={styles.webView} />
        </View> : null}

        <Loading visible={loading} />


      
  </View>

};

export default KnowledgeDetailScreen;
