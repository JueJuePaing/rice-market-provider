import { StyleSheet } from "react-native";
import {
    heightPercentageToDP as hp, 
    widthPercentageToDP as wp
} from "react-native-responsive-screen";
import Colors from 'assets/colors';

const styles = StyleSheet.create({
    container : {
      flex : 1,
      backgroundColor: '#fff'
    },
    categoryContent: {
      marginHorizontal: wp(3),
      paddingBottom: hp(2)
    },
    tab: {
      paddingHorizontal: wp(5),
      borderRadius: hp(3),
      borderWidth: 1,
      borderColor: Colors.light_green,
      zIndex: 2,
      paddingVertical: hp(1.2)
    },
    selectedTab: {
      backgroundColor: Colors.light_green,
      borderWidth: 0
    },
    tabLabel: {
      fontSize: hp(1.8),
      color: Colors.gray
    },
    selectedTabLabel: {
      color: '#fff'
    },
    item: {
      width: wp(94),
      alignSelf: 'center',
      alignItems: 'center',
      height: hp(90),
  },
  newsImg: {
      width: wp(94),
      height: hp(25),
      borderRadius: hp(1),
      resizeMode: 'cover'
  },
  title: {
      fontWeight: 'bold',
      fontSize: hp(1.8),
      color: '#000',
      paddingBottom: hp(.7),
      paddingTop: hp(1),
      alignSelf: 'flex-start'
  },
  webView: {
      width: wp(94)
  },
    list : {
      marginBottom : hp(2)
    },
    video: {
        width: wp(94),
        height: hp(25),
        borderRadius: hp(1),
    },
    videoBg: {
        width: wp(94),
        height: hp(25),
        borderRadius: hp(1),
        backgroundColor: '#a8a5a5'
    },
    videoContent: {
        alignItems: 'center',
        justifyContent: 'center',
        width: wp(94),
        height: hp(25),
        marginTop: hp(2),
        marginBottom: hp(1)
       },
       playButton : {
        position: 'absolute'
      },
      slider: {
        width: wp(100),
      },
  });

export default styles;