import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import Colors from "assets/colors";

const styles = StyleSheet.create({
  container : {
    flex : 1,
    alignItems : "center",
    backgroundColor : "#FFFEFF"
  },
  button : {
    width : wp(50)
  },
  errorTxt : {
    fontSize : 14,
    marginBottom: hp(2),
    marginTop: hp(-2)
  },
  description: {
    fontSize: hp(1.8),
    color: '#000',
    maxWidth: wp(80),
    textAlign: 'center'
  },
  logo : {
    width : wp(80),
    height: hp(30),
    resizeMode : 'contain',
    marginBottom: hp(10)
  },
  passInput: {
    width: wp(80),
    height: hp(15),
    alignSelf: 'center'
  },
  otpDescription: {
    width: wp(80),
    marginTop: hp(-2),
    marginBottom: hp(3),
    color: '#000',
    fontSize: hp(1.7)
  },
  underlineStyleBase: {
    width: wp(12),
    height: wp(13),
    borderWidth: 1,
    borderRadius: hp(1),
    borderColor: '#e1e6f5',
    color: Colors.dark_yellow,
    fontSize: hp(2.4),
  },
  underlineStyleHighLighted: {
    borderColor: Colors.dark_yellow
  },
      nrcRow: {
        flexDirection : "row",
        alignItems : "center",
        marginVertical : 10,
       justifyContent: "space-between",
       width: wp(90),
       marginBottom : hp(2),
       marginTop: hp(2)
    },
    nrcBtn: {
        borderWidth : 1,
        borderColor : "#ddd",
        borderRadius : 5,
        height: 40,
        alignItems: 'center',
        flexDirection : "row",
        alignItems : "center",
        justifyContent: 'space-around'
    },
    stateCodeBtn: {
        width : wp(15), 
    },
    townshipBtn: {
        width : wp(24)
    },
    citizenBtn: {
        width : wp(15)
    },
    nrcInputContainer : {
      alignItems : "center",
      justifyContent: 'center',
      borderWidth : 1,
      borderColor : "#ddd",
      borderRadius : 5
    },
    nrcInputWrapper : {
      height : 40,
      flex : 1,
      borderWidth : 0,
      borderRadius : 5,
      overflow : "hidden",
      width : wp(27)
    },
    input : {
      color : "#000"
    },
    nrcValue : {
      fontSize : hp(1.8),
      color: '#000',
      paddingLeft : 3
    }
});

export default styles;