import React, {
  useState, 
  memo,
  useContext
} from "react";
import {
  View,
  Text,
  TextInput,
  Image,
  ToastAndroid,
  TouchableOpacity
} from "react-native";
import {
  DefaultTheme,
  Button,
  HelperText
} from "react-native-paper";
import OTPInputView from '@twotalltotems/react-native-otp-input';
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import Entypo from 'react-native-vector-icons/Entypo';

import { setItem } from "utils/appStorage";
import styles from "./Style";
import { fetchPost } from "utils/fetchData";
import apiUrl from "utils/apiUrl";
import { Context } from "context/Provider";
import {
  STATE_CODE,
  CITIZEN_CODE,
  TOWNSHIP_LIST
} from 'data/nrc';
import { useLocal } from "hook/useLocal";

import Loading from "components/Loading";
import SelectModal from "components/SelectModal";

const OTPScreen = ({ navigation, route }) => {
  const local = useLocal();

  const {phone, forgetPassword, regOtp, send_otp} = route.params;

  const {
    changeAuth, 
    lang,
    changeUserInfo
  } = useContext(Context);

  const [passCode, setPassCode] = useState('');
  const [error, setError] = useState('');

  const [
    showModal,
    setShowModal
  ] = useState(false);
  const [
    modalData,
    setModalData
  ] = useState();
  const [
    modalTitle,
    setModalTitle
  ] = useState('');
  const [
    loading, 
    setLoading
  ] = useState(false);
  const [
    nrcStateCode,
    setNrcStateCode
  ] = useState(STATE_CODE[0])
  const [
    nrcTownshipCode,
    setNrcTownshipCode
  ] = useState(TOWNSHIP_LIST[0].TownshipCode[0])
  const [
    nrcTownships,
    setNrcTownships
  ] = useState(TOWNSHIP_LIST[0].TownshipCode);
  const [
    nrcCitizenCode,
    setNrcCitizenCode
  ] = useState(CITIZEN_CODE[0])
  const [
    nrcNo,
    setNrcNo
  ] = useState('');
  const [
    nrcNoError,
    setNrcNoError
  ] = useState('');

  const theme = {
    ...DefaultTheme,
    colors : {
      ...DefaultTheme.colors,
      primary : "#87ad2b"
    }
  };

  const validatePhone = async () => {

    setLoading(true);

    const reqData = {
      phone_number : phone,
      otp: passCode
    };
    const id_card = nrcStateCode + '/' + nrcTownshipCode + '(' + nrcCitizenCode + ')' + nrcNo;
    const reqNrcData = {
      phone_number : phone,
      id_card
    }

    const response = await fetchPost(
      forgetPassword ? send_otp ? apiUrl.verifyOTP : apiUrl.checkIdCard : apiUrl.validatePhone,
      forgetPassword ? send_otp ? reqData : reqNrcData : reqData
    );

    setLoading(false)

    if (response?.success) {
      if (forgetPassword) {
        ToastAndroid.show(response.data?.message ?? local.successful, ToastAndroid.SHORT)
        navigation.navigate('ChangePassword', {token : response.data?.token, forgetPassword});
      } else {
        await setItem('@auth', 'true');
        await setItem('@userInfo', JSON.stringify(response.data?.user));

        changeUserInfo(JSON.stringify(response.data?.user))
        changeAuth(true);

        navigation.replace("App", { name : "navigation" });
      }
    } else {
      setError(response?.message ?? response.data ? response.data.message : local.somethingWrong)
    }
    
  }

  const handleNrcNoChange = (no) => {
    setNrcNoError("");
    setNrcNo(no); 
  };

  const handleNRCStateCodeOpen = () => {
    setModalData(STATE_CODE);
    setShowModal(true);
    setModalTitle(local.chooseNRCStateCode);
  }

  const handleNRCTownshipOpen = () => {
    setModalData(nrcTownships);
    setShowModal(true);
    setModalTitle(local.chooseNRCTownshipCode);
  }

  const handleNRCCitizenOpen = () => {
    setModalData(CITIZEN_CODE);
    setShowModal(true);
    setModalTitle(local.chooseNRCCitizenCode);
  }

  const onChooseModalData = (data) => {
    if (modalTitle === local.chooseNRCStateCode) {
      setNrcStateCode(data);
      const state_id = parseInt(data);
      const tsCodes = TOWNSHIP_LIST[state_id-1].TownshipCode;
      setNrcTownshipCode(tsCodes[0])
      setNrcTownships(tsCodes);
    }  else if (modalTitle === local.chooseNRCTownshipCode) {
      setNrcTownshipCode(data);
    } else if (modalTitle === local.chooseNRCCitizenCode) {
      setNrcCitizenCode(data);
    }
    setShowModal(false);
    setModalTitle('');
    setModalData();
  }


  return (
    <View style={styles.container}>
      <View style={styles.logoContainer}>
        <Image
          source={require("assets/images/app_logo.jpeg")}
          style={styles.logo} />
        {/* <Text style={styles.appname}>MM Rice Market Provider</Text> */}
      </View>
      <Text style={styles.description}>
        {send_otp ? lang === 'en' ? `${local.enterSentOTP}${phone}` : `${phone}${local.enterSentOTP}` : 
        forgetPassword ? local.chooseNRC : local.enterOTP}
      </Text>
      
      {(!forgetPassword || (forgetPassword && send_otp)) && <OTPInputView
        style={styles.passInput}
        secureTextEntry={false}
        code={passCode}
        onCodeChanged={code => {
          setPassCode(code);
          setError('')
        }}
        pinCount={6}
        autoFocusOnLoad
        codeInputFieldStyle={styles.underlineStyleBase}
        codeInputHighlightStyle={styles.underlineStyleHighLighted}
        onCodeFilled={code => {
          console.log(`Code is ${code}, you are good to go!`);
        }}
      />}

      {forgetPassword && !send_otp && <>
        <View style={styles.nrcRow}>
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={handleNRCStateCodeOpen}
            style={[styles.nrcBtn, styles.stateCodeBtn]}>
            <Text style={styles.nrcValue}>{nrcStateCode}</Text>
            <Entypo
              name='chevron-small-down'
              size={18}
              color='#5c5c5a'
              style={styles.downBtn} />
          </TouchableOpacity>
          <Text style={styles.nrcValue}>/</Text>
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={handleNRCTownshipOpen}
            style={[styles.nrcBtn, styles.townshipBtn]}>
            <Text style={styles.nrcValue}>{nrcTownshipCode}</Text>
            <Entypo
              name='chevron-small-down'
              size={18}
              color='#5c5c5a'
              style={styles.downBtn} />
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={handleNRCCitizenOpen}
            style={[styles.nrcBtn, styles.citizenBtn]}>
            <Text style={styles.nrcValue}>{nrcCitizenCode}</Text>
            <Entypo
              name='chevron-small-down'
              size={18}
              color='#5c5c5a'
              style={styles.downBtn} />
          </TouchableOpacity>

          <View style={styles.nrcInputContainer}>
            <View style={styles.nrcInputWrapper}>
              <TextInput
                placeholder={local.nrcNo}
                value={nrcNo}
                onChangeText={handleNrcNoChange}
                keyboardType="number-pad"
                autoCapitalize="none"
                style={styles.input}
                maxLength={6}
                placeholderTextColor='darkgray'
                underlineColor="transparent"/>
            </View>
          </View>
        </View>
        {!!nrcNoError && <Text style={styles.errTxt}>
          {nrcNoError}</Text>}
      </>}

      {regOtp && !send_otp && <Text style={styles.otpDescription}>
        {`${local.regOtpDescription}${regOtp}`}
      </Text>}

      {!!error && (
        <HelperText type="error" style={styles.errorTxt}>
          {error}
        </HelperText>
      )}

      <Button
          mode="contained"
          theme={theme}
          // buttonColor={Colors.light_green}
          onPress={validatePhone}
          style={styles.button}
          labelStyle={{fontSize: hp(1.8)}}
          disabled={loading}>
          {local.submit}
        </Button>

      <Loading visible={loading} />

      {showModal && <SelectModal
        title={ modalTitle }
        data={ modalData }
        selectedId={ 
          modalTitle === local.chooseNRCStateCode ? nrcStateCode :
          modalTitle === local.chooseNRCTownshipCode ? nrcTownshipCode :
          modalTitle === local.chooseNRCCitizenCode ? nrcCitizenCode : null }
        onClose={() => {
          setShowModal(false)
          setModalTitle('');
          setModalData();
        }}
        onChoose={onChooseModalData} />}
    </View>)
};

export default memo(OTPScreen);
