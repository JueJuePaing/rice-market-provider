import React, {
  useState, 
  memo, 
  useRef,
  useEffect,
  useContext
} from "react";
import {
  View,
  Text, 
  Image, 
  TouchableOpacity,
  TextInput
} from "react-native";
import {
  Button,
  HelperText,
  DefaultTheme
} from "react-native-paper";
import Icon from "react-native-vector-icons/MaterialIcons";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import DeviceInfo from 'react-native-device-info';
import Pushy from 'pushy-react-native';

import {
  LoginScreen as styles
} from "assets/style";
import { setItem } from "utils/appStorage";
import apiUrl from "utils/apiUrl";
import { fetchPost } from "utils/fetchData";
import { useLocal } from "hook/useLocal";

import Loading from "components/Loading";
import Language from "components/Language";
import { Context } from "context/Provider";

const LoginScreen = ({ navigation }) => {
  const local = useLocal();

  const {
    token,
    lang,
    changeAuth,
    changeUserInfo,
    changeToken,
    changeLang
  } = useContext(Context);

  const [
    phone,
    setPhone
  ] = useState("");
  const [
    password,
    setPassword
  ] = useState("");
  const [
    loading, 
    setLoading
  ] = useState(false);
  const [
    error, 
    setError
  ] = useState("");
  const [
    showPassword, 
    setShowPassword
  ] = useState(false);
  const [
    lanModalOpened,
    setLanModalOpened
  ] = useState(false);
  const [
    phoneError,
    setPhoneError
  ] = useState('');
  const [
    pwError,
    setPwError
  ] = useState('');

  const device_id = DeviceInfo.getDeviceId();

  const _isMounted = useRef(false);

  const theme = {
    ...DefaultTheme,
    colors : {
      ...DefaultTheme.colors,
      primary : "#87ad2b"
    }
  };

  useEffect(() => {
    _isMounted.current = true;

    return function clean() {
      _isMounted.current = false;
    };
  }, []);

  const handlePhoneChange = (phno) => {
    setPhoneError("");
    setError('');
    setPhone(phno);
  };

  const handlePasswordChange = (pw) => {
    setPwError("");
    setError('');
    setPassword(pw);
  };

  const handleLogin = async () => {
    let valid = true;
    if (!phone) {
      setPhoneError(true);
      valid = false;
    }
    if (!password) {
      setPwError(local.invalidPassword);
      valid = false;
    }

    if (!valid) return 

    setLoading(true);
  
    Pushy.register()
      .then(async deviceToken => {

        const data = {
          phone_number: phone,
          password,
          device_id,
          pushy_token: deviceToken
        };

        const response = await fetchPost(apiUrl.login, data);

        setLoading(false);
        if (response?.success) {
          await setItem('@auth', 'true');
          await setItem('@token', response.data?.token);
          await setItem('@userInfo', JSON.stringify(response.data?.user));
    
          changeToken(response.data?.token)
          changeAuth(true);
          changeUserInfo(JSON.stringify(response.data?.user))

          navigation.replace("App", { name : "navigation" });
        } else {
          if (response?.data?.error_code === 1) {
            // Phone no validate
            navigation.navigate('OTP', {phone, token})
            return;
          }
          setError(response?.data?.message ?? 'Something went wrong');
        }
      })
      .catch(err => {
        // Handle registration errors
        console.error('Pushy Register Errors : ' + err);
      });
  };

  const handleForgotPassword = () => {
    navigation.navigate("ForgetPassword");
  };

  const handleRegister = () => {
    navigation.navigate("Register");
  }

  const languageHandler = () => {
    setLanModalOpened(true);
  };

  const chooseLan = (lan) => {
    changeLang(lan);
    setItem('@lang', lan);
    setLanModalOpened(false);
  }

  return (
    <View style={styles.container}>
      <TouchableOpacity 
        style={styles.language}
        activeOpacity={0.8}
        onPress={languageHandler}>
        <SimpleLineIcons
          size={hp(3)}
          color="#000"
          name="globe"
           />
      </TouchableOpacity>
      <View style={styles.logoContainer}>
        <Image
          source={require("assets/images/app_logo.jpeg")}
          style={styles.logo} />
      </View>
      <View style={styles.formContainer}>
        <View style={styles.inputContainer}>
          <Icon name="phone" size={hp(2.2)} style={styles.inputIcon} />
          <View style={styles.inputWrapper}>
            <TextInput
              placeholder={local.phoneNumber}
              value={phone}
              onChangeText={handlePhoneChange}
              keyboardType="number-pad"
              autoCapitalize="none"
              style={styles.input}
              underlineColor="transparent"/>
          </View>
        </View>
        {!!phoneError && <Text style={styles.errTxt}>
          {local.invalidPhoneNumber}
        </Text>}

        <View style={styles.inputContainer}>
          <Icon name="lock" size={hp(2.2)} style={styles.inputIcon} />
          <View style={styles.inputWrapper}>
            <TextInput
              placeholder={local.password}
              value={password}
              onChangeText={handlePasswordChange}
              secureTextEntry={!showPassword}
              style={styles.input}
              underlineColor="transparent"/>
          </View>
          <Icon
            name={!showPassword ? "visibility-off" : "visibility"}
            size={hp(2.2)}
            onPress={() => setShowPassword(!showPassword)}
            style={styles.inputIcon}/>
        </View>
        {!!pwError && <Text style={styles.errTxt}>{ pwError }</Text>}

        <TouchableOpacity 
          style={styles.forgotPasswordBtn} 
          onPress={handleForgotPassword}>
          <Text style={styles.forgotPassword}>
            {local.forgotPassword}
          </Text>
        </TouchableOpacity>
        {!!error && (
          <HelperText type="error" style={styles.errorTxt}>
            {error}
          </HelperText>
        )}
        <Button
          mode="contained"
          theme={theme}
          onPress={handleLogin}
          style={styles.button}
          labelStyle={{fontSize: hp(1.8)}}
          disabled={loading}>
          {local.login}
        </Button>

        <View style={styles.regRow}>
          <Text style={styles.regLabel}>
            {local.noAccount}
          </Text>
          <TouchableOpacity 
            style={styles.forgotPasswordBtn} 
            onPress={handleRegister}>
            <Text style={styles.forgotPassword}>
              {local.register}
            </Text>
          </TouchableOpacity>
        </View>
        
      </View>
      <Loading visible={loading} />
      <Language 
        visible={lanModalOpened}
        lan={ lang }
        languageHandler={chooseLan}
        onClose={()=> setLanModalOpened(false)} />
    </View>)
};

export default memo(LoginScreen);
