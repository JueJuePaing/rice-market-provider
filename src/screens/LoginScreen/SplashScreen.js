import React, { useEffect, useContext } from "react";
import {
  View,
  Image
} from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

import { Context } from "context/Provider";
import { getItem } from "utils/appStorage";

const SplashScreen = ({ navigation }) => {

  const {
    changeLang,
    changeUserInfo,
    changeToken
  } = useContext(Context);

  useEffect(() => {
    const getStorageData = async () => {
      const token = await getItem('@token');
      const auth = await getItem('@auth');
      const info = await getItem('@userInfo');
      const language = await getItem('@lang');
      changeLang(language ? language : 'en');
      if (info) {
        changeUserInfo(info);
      }
      changeToken(token);
      if (auth === 'true') {
        navigation.replace("App");
      } else {
        navigation.replace("Login");
      }
    }

    getStorageData();
}, [navigation]);

  return (
  <View style={{
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }}>
  <Image
    source={require("assets/images/app_logo.jpeg")}
    style={{ 
      width : wp(70),
      height: hp(25),
      resizeMode : 'contain'
    }} />
</View>
  );
};

export default SplashScreen;
