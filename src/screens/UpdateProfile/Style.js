import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import Colors from "assets/colors";

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor : "#f2f2f2",
      },
      content: {
        paddingHorizontal : 20,
        marginBottom: hp(12)
      },
      logoContainer : {
        marginBottom : 0,
        // flexDirection : 'row',
        alignItems: 'center'
      },
      logo : {
        width : wp(80),
        height: hp(30),
        resizeMode : 'cover'
      },
      appname : {
        fontSize: hp(2.3),
        fontWeight: 'bold',
        marginBottom : hp(7),
        marginTop: hp(1),
        color: '#000'
      },
      formContainer : {
        width : "100%"
      },
      uploadBtn: {
        alignSelf: 'center',
        marginVertical : hp(1.5)
      },
      editBtn: {
        position: 'absolute',
        right: wp(-6),
        bottom: hp(-.5)
      },
      profile: {
        width: hp(12),
        height: hp(12),
        borderRadius: hp(1), //12
        resizeMode : 'cover'
      },
      errTxt: {
        alignSelf: 'flex-start',
        color: 'red',
        fontSize: hp(1.6),
        marginTop: -4,
        marginBottom: 5
      },
      errTxt2: {
        alignSelf: 'center',
        color: 'red',
        fontSize: hp(1.6),
        marginTop: -4,
        marginBottom: 5
      },
      innerUpload: {
        width: hp(9.5),
        height: hp(9.5),
        borderRadius: hp(7),
        backgroundColor: 'lightgray',
        alignItems : "center",
        justifyContent: 'center',
      },
      inputContainer : {
        flexDirection : "row",
        alignItems : "center",
        marginVertical : 10,
        borderWidth : 1,
        borderColor : "#ddd",
        borderRadius : 5
      },
      inputWrapper : {
        height : 40,
        flex : 1,
        borderWidth : 0,
        borderRadius : 5,
        overflow : "hidden"
        // paddingBottom: 20,
      },
      selectLabel : {
        paddingHorizontal : 10,
        fontSize : hp(1.8),
        color: '#000'
      },
      input : {
        height : 40,
        marginTop : 0,
        backgroundColor : "#f2f2f2",
        flex : 1,
        paddingHorizontal : 10,
        fontSize : hp(1.8),
        color: '#000'
      },
      inputAreaIcon : {
        marginHorizontal : 10,
        color : "#4A4550",
        marginTop: 10
      },
      inputAreaWrapper : {
        height : 120,
        flex : 1,
        borderWidth : 0,
        borderRadius : 5,
        overflow : "hidden"
        // paddingBottom: 20,
      },
      inputArea : {
        height : 120,
        marginTop : 0,
        backgroundColor : "#f2f2f2",
        flex : 1,
        paddingHorizontal : 10,
        fontSize : hp(1.8),
        color: '#000'
      },
      inputIcon : {
        marginHorizontal : 10,
        color : "#4A4550"
      },
      checkboxContainer : {
        flexDirection : "row",
        alignItems : "center",
        justifyContent : "center",
        marginVertical : 10
      },
      checkbox : {
        backgroundColor : "transparent",
        borderColor : "transparent"
      },
      checkboxLabel : {
        marginLeft : 5,
        fontSize : 16
      },
      button : {
        // backgroundColor: '#DB3B26',
        marginTop : hp(2.5)
      },
      forgotPasswordBtn : {
        alignSelf : "flex-end",
        marginTop: -5
      },
      forgotPassword : {
        fontSize : hp(1.6),
        color : Colors.dark_yellow,
        fontWeight: 'bold'
      },
      errorTxt : {
        fontSize : 14
      },
      language : {
        position : 'absolute',
        top : hp(1),
        right : wp(4)
      },
      backBtn : {
        height : hp(4.3),
        width : hp(4.3),
        alignItems: 'center',
        justifyContent : 'center',
        borderWidth : 3,
        borderColor : 'rgba(171, 194, 118, 0.5)',
        borderRadius: hp(1.5),
        position : "absolute",
        top : hp(1),
        left : wp(4)
      },
      regRow: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        marginTop : hp(2),
        marginBottom: hp(2)
      },
      regLabel: {
        color: 'darkgray',
        fontSize: hp(1.7),
        marginRight: wp(2)
      },
      nrcRow: {
        flexDirection : "row",
        alignItems : "center",
        marginVertical : 10,
       justifyContent: "space-between"
    },
    nrcBtn: {
        borderWidth : 1,
        borderColor : "#ddd",
        borderRadius : 5,
        height: 40,
        alignItems: 'center',
        flexDirection : "row",
        alignItems : "center",
        justifyContent: 'space-around'
    },
    stateCodeBtn: {
        width : wp(15), 
    },
    townshipBtn: {
        width : wp(24)
    },
    citizenBtn: {
        width : wp(15)
    },
    nrcInputContainer : {
      alignItems : "center",
      justifyContent: 'center',
      borderWidth : 1,
      borderColor : "#ddd",
      borderRadius : 5
    },
    nrcInputWrapper : {
      height : 40,
      flex : 1,
      borderWidth : 0,
      borderRadius : 5,
      overflow : "hidden"
      // paddingBottom: 20,
    },
    nrcValue : {
      fontSize : hp(1.8),
      color: '#000',
      paddingLeft : 3
    }
});

export default styles;