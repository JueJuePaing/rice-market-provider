import React, {
  memo,
  useContext,
  useState,
  useEffect,
  useCallback
} from "react";
import {
  View,
  Text,
  Image,
  StatusBar,
  ScrollView,
  TouchableOpacity,
  Alert,
  BackHandler,
  TextInput,
  ToastAndroid
} from "react-native";
import Ionicons from 'react-native-vector-icons/Ionicons';
import Icon from "react-native-vector-icons/MaterialIcons";
import Feather from 'react-native-vector-icons/Feather';
import {
  Button,
  HelperText,
  DefaultTheme
} from "react-native-paper";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { launchImageLibrary } from "react-native-image-picker";
import Entypo from 'react-native-vector-icons/Entypo';

import Colors from 'assets/colors';
import { setItem } from "utils/appStorage";
import apiUrl from "utils/apiUrl";
import { fetchGet, fetchPostImageUpload } from "utils/fetchData";
import styles from './Style';
import { Context } from "context/Provider";
import {useLocal} from 'hook/useLocal';
import {
  STATE_CODE,
  CITIZEN_CODE,
  TOWNSHIP_LIST
} from 'data/nrc';

import SelectModal from "components/SelectModal";
import Loading from "components/Loading";
import Header from "components/Header/DetailHeader";

const UpdateProfileScreen = ({ navigation, route }) => {
  const {userData} = route.params;
  const local = useLocal();

  const {
    token,
    userInfo,
    changeUserInfo
  } = useContext(Context);

  const parsedUserInfo = userInfo ? JSON.parse(userInfo) : null;

  const theme = {
    ...DefaultTheme,
    colors : {
      ...DefaultTheme.colors,
      primary : "#87ad2b"
    }
  };

  const [
    profile,
    setProfile
  ] = useState();
  const [
    name,
    setName
  ] = useState("");
  const [
    address,
    setAddress
  ] = useState('');

  const [
    loading, 
    setLoading
  ] = useState(false);
  const [
    error, 
    setError
  ] = useState("");
  const [
    states,
    setStates
  ] = useState([]);
  const [
    townships,
    setTownships
  ] = useState([]);
  const [
    allTownships,
    setAllTownships
  ] = useState([]);
  const [
    userTypes,
    setUserTypes
  ] = useState([]);
  const [
    showModal,
    setShowModal
  ] = useState(false);
  const [
    modalData,
    setModalData
  ] = useState();
  const [
    modalTitle,
    setModalTitle
  ] = useState('');
  const [
    state,
    setState
  ] = useState();
  const [
    township,
    setTownship
  ] = useState();
  const [
    userType,
    setuserType
  ] = useState();
  const [
    nameError,
    setNameError
  ] = useState('');

  const [
    nrcStateCode,
    setNrcStateCode
  ] = useState(STATE_CODE[0])
  const [
    nrcTownshipCode,
    setNrcTownshipCode
  ] = useState(TOWNSHIP_LIST[0].TownshipCode[0])
  const [
    nrcTownships,
    setNrcTownships
  ] = useState(TOWNSHIP_LIST[0].TownshipCode);
  const [
    nrcCitizenCode,
    setNrcCitizenCode
  ] = useState(CITIZEN_CODE[0])
  const [
    nrcNo,
    setNrcNo
  ] = useState('');

  const [
    addressError,
    setAddressError
  ] = useState('');
  const [
    statesError,
    setStatesError
  ] = useState('');
  const [
    townshipError,
    setTownshipError
  ] = useState('');
  const [
    userTypeError,
    setUserTypeError
  ] = useState('');
  const [
    nrcNoError,
    setNrcNoError
  ] = useState('');

  const backButtonHandler = useCallback(() => {
    navigation.goBack();
    return true;
  }, []);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, [backButtonHandler]);

  useEffect(()=> {
    if (userData) {
      setProfile({uri: userData.image})
      setName(userData.name)
      setAddress(userData.address)
      const filterState = states.filter(state => state.id === userData.states_and_division_id);
      setState(filterState[0]);
      const tsState = allTownships.filter(ts => ts.id === userData.township_id);
      setTownship(tsState[0]);
      const filterTownships = allTownships.filter(ts => ts.state_id === userData.states_and_division_id);
      setTownships(filterTownships)
      const typeState = userTypes.filter(type => type.id === userData.user_type_id);
      setuserType(typeState[0]);

      const id_card_parts = userData.id_card?.split(/[\/()]/);

      if (id_card_parts && id_card_parts.length > 0) {
        setNrcStateCode(id_card_parts[0]);
        setNrcTownshipCode(id_card_parts[1]);
        setNrcCitizenCode(id_card_parts[2]);
        setNrcNo(id_card_parts[3]);

        const state_id = parseInt(id_card_parts[0]);
        const tsCodes = TOWNSHIP_LIST[state_id-1].TownshipCode;
        setNrcTownships(tsCodes);
      }
    }
  }, [userData, states, allTownships, userTypes])

  useEffect(() => {
    const getStatesAndDivision = async () => {
      const response = await fetchGet(apiUrl.statesAndTownships);
      if (response?.success) {
        setStates(response.states);
        setAllTownships(response.townships)
      }
    }
    getStatesAndDivision();
  }, []);

  useEffect(() => {
    const getUserTypes = async () => {
      const response = await fetchGet(apiUrl.userTypes);
      if (response?.success) {
        setUserTypes(response.data);
      }
    }
    getUserTypes();
  }, []);

  const handleNrcNoChange = (no) => {
    setNrcNoError("");
    setNrcNo(no); 
  };

  const handleAddressChange = (val) => {
    setAddressError("");
    setAddress(val);
  };

  const handleNameChange = (txt) => {
    setNameError("");
    setName(txt);
  };

  const handleNRCStateCodeOpen = () => {
    setModalData(STATE_CODE);
    setShowModal(true);
    setModalTitle(local.chooseNRCStateCode);
  }

  const handleNRCTownshipOpen = () => {
    setModalData(nrcTownships);
    setShowModal(true);
    setModalTitle(local.chooseNRCTownshipCode);
  }

  const handleNRCCitizenOpen = () => {
    setModalData(CITIZEN_CODE);
    setShowModal(true);
    setModalTitle(local.chooseNRCCitizenCode);
  }

  const handleSubmit = async () => {
    let valid = true;
    if (!name) {
      setNameError(true);
      valid = false;
    }
    if (!address) {
      setAddressError(local.invalidAddress);
      valid = false
    }
    if (!state) {
      setStatesError(true);
      valid = false;
    }
    if (!township) {
      setTownshipError(true);
      valid = false;
    }
    if (!userType) {
      setUserTypeError(true);
      valid = false;
    }
    if (!nrcNo) {
      setNrcNoError(local.enterNrcNo)
      valid = false;
    } else if (nrcNo.length < 6) {
      setNrcNoError(local.invalidNrcNo);
      valid = false;
    }

    if (!valid) return 

    setLoading(true);

    const id_card = nrcStateCode + '/' + nrcTownshipCode + '(' + nrcCitizenCode + ')' + nrcNo;

    const uploadData = new FormData();
    uploadData.append('name', name);
    uploadData.append('address', address);
    uploadData.append('state_id', state.id);
    uploadData.append('township_id', township.id);
    uploadData.append('user_type_id', userType.id);
    uploadData.append('id_card', id_card);
    if (profile.type) {
      uploadData.append('image', {
        type: profile.type,
        uri: profile.uri,
        name: profile.fileName,
        width: profile.width,
        height: profile.height
      });
    }

    const response = await fetchPostImageUpload(apiUrl.updateProfile, uploadData, token);

    setLoading(false);

    if (response?.success) {
      //success
      ToastAndroid.show(response.message ?? local.successful, ToastAndroid.SHORT);
      const data = {
        ...parsedUserInfo,
        name,
        address,
        image : profile.uri,
        states_and_division_id : state.id,
        township_id : township.id,
        user_type_id : userType.id,
        id_card
      }
      await setItem('@userInfo', JSON.stringify(data));
      changeUserInfo(JSON.stringify(data))
      navigation.goBack();
    } else {
      setError(response.message ?? local.somethingWrong)
    }
  };


  const handleUpload = async () => {
    launchImageLibrary({
      selectionLimit : 1,
      mediaType : "photo"
  }, response => {
      if (response?.didCancel !== true) {
        if (response && response.assets && response.assets.length > 0) {
          setProfile(response.assets[0]);
        } 
      }
    });
  }

  const handleStatesOpen = () => {
    setModalData(states);
    setShowModal(true);
    setModalTitle(local.chooseStates);
  }

  const handleTownshipsOpen = () => {
    setModalData(townships);
    setShowModal(true);
    setModalTitle(local.chooseTownship);
  }

  const handleUserTypeOpen = () => {
    setModalData(userTypes);
    setShowModal(true);
    setModalTitle(local.chooseUserType);
  }

  const onChooseModalData = (data) => {
    if (modalTitle === local.chooseTownship) {
      setTownship(data);
      setTownshipError(false);
    } else if (modalTitle === local.chooseUserType) {
      setuserType(data);
      setUserTypeError(false)
    } else if (modalTitle == local.chooseStates) {
      setState(data);
      setTownship();
      setStatesError(false)
      // get related townships 
      const filterTownships = allTownships.filter(township => township.state_id == data.id+"");
      setTownships(filterTownships)
    } else if (modalTitle === local.chooseNRCStateCode) {
      setNrcStateCode(data);
      const state_id = parseInt(data);
      const tsCodes = TOWNSHIP_LIST[state_id-1].TownshipCode;
      setNrcTownshipCode(tsCodes[0])
      setNrcTownships(tsCodes);
    }  else if (modalTitle === local.chooseNRCTownshipCode) {
      setNrcTownshipCode(data);
    } else if (modalTitle === local.chooseNRCCitizenCode) {
      setNrcCitizenCode(data);
    }
    setShowModal(false);
    setModalTitle('');
    setModalData();
  }


  return (
    <>
    <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
      <StatusBar backgroundColor='#f2f2f2' />
      <Header
        title={local.updateProfile}
        backHandler={()=> navigation.goBack()}/>
      
      <View style={styles.content}>
        <View style={styles.formContainer}>
          <TouchableOpacity
            style={styles.uploadBtn}
            onPress={handleUpload}>
              {
                profile ? <Image
                  source={{ uri : profile.uri }}
                  style={styles.profile} /> : 
                <Ionicons
                  name='person-circle-outline'
                  size={hp(13)}
                  color='darkgray' />
              }
            <TouchableOpacity onPress={handleUpload}>
              <Feather
                name='edit'
                color={Colors.light_green}
                size={hp(2.4)}
                style={styles.editBtn} />
            </TouchableOpacity>
          </TouchableOpacity>
        </View>

        <View style={styles.inputContainer}>
          <Icon name="person" size={hp(2.2)} style={styles.inputIcon} />
          <View style={styles.inputWrapper}>
            <TextInput
              placeholder={local.name}
              value={name}
              onChangeText={handleNameChange}
              autoCapitalize="none"
              // error={nameError}
              style={styles.input}
              placeholderTextColor='darkgray'
              underlineColor="transparent"/>
          </View>
        </View>
        {!!nameError && <Text style={styles.errTxt}>{local.invalidName}</Text>}

        <View style={styles.nrcRow}>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={handleNRCStateCodeOpen}
          style={[styles.nrcBtn, styles.stateCodeBtn]}>
          <Text style={styles.nrcValue}>{nrcStateCode}</Text>
          <Entypo
            name='chevron-small-down'
            size={18}
            color='#5c5c5a'
            style={styles.downBtn} />
        </TouchableOpacity>
        <Text style={styles.nrcValue}>/</Text>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={handleNRCTownshipOpen}
          style={[styles.nrcBtn, styles.townshipBtn]}>
          <Text style={styles.nrcValue}>{nrcTownshipCode}</Text>
          <Entypo
            name='chevron-small-down'
            size={18}
            color='#5c5c5a'
            style={styles.downBtn} />
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={handleNRCCitizenOpen}
          style={[styles.nrcBtn, styles.citizenBtn]}>
          <Text style={styles.nrcValue}>{nrcCitizenCode}</Text>
          <Entypo
            name='chevron-small-down'
            size={18}
            color='#5c5c5a'
            style={styles.downBtn} />
        </TouchableOpacity>

        <View style={styles.nrcInputContainer}>
          <View style={styles.nrcInputWrapper}>
            <TextInput
              placeholder={local.nrcNo}
              value={nrcNo}
              onChangeText={handleNrcNoChange}
              keyboardType="number-pad"
              autoCapitalize="none"
              style={styles.input}
              maxLength={6}
              placeholderTextColor='darkgray'
              underlineColor="transparent"/>
          </View>
        </View>
      </View>
      {!!nrcNoError && <Text style={styles.errTxt}>
          {nrcNoError}</Text>}
  
        <View style={[styles.inputContainer, {alignItems: 'flex-start'}]}>
          {/* <Icon name="map" size={hp(2.2)} style={styles.inputAreaIcon} /> */}
          <View style={styles.inputAreaWrapper}>
            <TextInput
              placeholder={local.address}
              value={address}
              multiline={true}
              numberOfLines={3}
              onChangeText={handleAddressChange}
              style={styles.inputArea}
              textAlignVertical="top"
              underlineColor="transparent"/>
          </View>
        </View>
        {addressError !== '' && <Text style={styles.errTxt}>{addressError}</Text>}

        <TouchableOpacity 
          onPress={ handleStatesOpen }
          style={[styles.inputContainer, {paddingHorizontal : 10,}]}>
          <View style={[styles.inputWrapper, {justifyContent: 'center'}]}>
            <Text style={styles.selectLabel}>
              {state ? state.name : local.chooseStates}
            </Text>
          </View>
          <Ionicons
              name="caret-down-circle"
              color={"#5f5a63"}
              size={hp(2.4)}
            />
        </TouchableOpacity>
        {!!statesError && <Text style={styles.errTxt}></Text>}

        <TouchableOpacity 
          onPress={ handleTownshipsOpen }
          disabled={ !state }
          style={[styles.inputContainer, {paddingHorizontal : 10,}]}>
          <View style={[styles.inputWrapper, {justifyContent: 'center'}]}>
            <Text style={[styles.selectLabel, {color: state ? '#000' : 'gray'}]}>
              {township ? township.name : local.chooseTownship}
            </Text>
          </View>
          <Ionicons

              name="caret-down-circle"
              color={"#5f5a63"}
              size={hp(2.4)}
            />
        </TouchableOpacity>
        {!!townshipError && <Text style={styles.errTxt}>{local.invalidTownship}</Text>}


        <TouchableOpacity 
          onPress={ handleUserTypeOpen }
          style={[styles.inputContainer, {paddingHorizontal : 10,}]}>
          <View style={[styles.inputWrapper, {justifyContent: 'center'}]}>
            <Text style={[styles.selectLabel]}>
              {userType ? userType.name : local.chooseUserType}
            </Text>
          </View>
          <Ionicons
              name="caret-down-circle"
              color={"#5f5a63"}
              size={hp(2.4)}
            />
        </TouchableOpacity>
        {!!userTypeError && <Text style={styles.errTxt}>{local.invalidUserType}</Text>}

        {!!error && (
          <HelperText type="error" style={styles.errorTxt}>
            {error}
          </HelperText>
        )}


        <Button
          mode="contained"
          theme={theme}
          onPress={handleSubmit}
          style={styles.button}
          labelStyle={{fontSize: hp(1.8)}}
          disabled={loading}>
          {local.submit}
        </Button>

      </View>

      </ScrollView>

      <Loading visible={loading} />

      {showModal && <SelectModal
          title={ modalTitle }
          data={ modalData }
          selectedId={ modalTitle === local.chooseTownship ? township?.id : modalTitle === local.chooseUserType ? userType?.id :
            modalTitle === local.chooseStates ? state?.id :
            modalTitle === local.chooseNRCStateCode ? nrcStateCode :
            modalTitle === local.chooseNRCTownshipCode ? nrcTownshipCode :
            modalTitle === local.chooseNRCCitizenCode ? nrcCitizenCode : null }
          onClose={() => {
            setShowModal(false)
            setModalTitle('');
            setModalData();
          }}
          onChoose={onChooseModalData} />}
      
      </>
  )
};

export default memo(UpdateProfileScreen);
