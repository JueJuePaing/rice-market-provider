import React, {
  memo,
  useContext,
  useState,
  useEffect,
  useCallback
} from "react";
import {
  View,
  Text,
  FlatList,
  StatusBar,
  BackHandler
} from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import moment from "moment-timezone";

import Colors from "assets/colors";
import apiUrl from "utils/apiUrl";
import { fetchPostByToken } from "utils/fetchData";
import styles from './Style';
import { Context } from "context/Provider";
import {useLocal} from 'hook/useLocal';

import Loading from "components/Loading";
import Header from "components/Header/DetailHeader";
import NoPackage from "components/NoPackage";

const PackageHistory = ({ navigation }) => {
  const local = useLocal();

  const {
    token
  } = useContext(Context);

  const [
    loading, 
    setLoading
  ] = useState(false);
  const [
    error, 
    setError
  ] = useState("");
  const [
    packageList,
    setPackageList
  ] = useState([]);

  const [page, setPage] = useState(1);
  const [loadMore, setLoadMore] = useState(false);
  const [moreLoading, setMoreLoading] = useState(false);

  useEffect(() => {
    if (page === 1) {
      setLoading(true)
    }
      getPackageList(page)
  }, [page, token]);


  const reloadData = async () => {
    setLoading(true);
    getPackageList(1);
  }


const getPackageList = async (pageNo) => {

  setMoreLoading(true)

  const response = await fetchPostByToken(
    `${apiUrl.packageHistory}?page=${pageNo}`,
    {},
    token
  );

  if (response?.success) {

    
    if (response.package?.length === 0 && pageNo === 1) {
      setError(local.noData)
    } else {
      setError("")

      const newList = pageNo > 1 ? packageList?.concat(response.package) : response.package;
      setPackageList(newList);

      if(response.nextPageUrl){
        setLoadMore(true);
      }else{
        setLoadMore(false);
      }
    }
  } else {
    setError(response?.message ? response?.message : response.data?.message ? response.data.message : local.noData);
  
  }
  setMoreLoading(false)
  setLoading(false)
}


const handleLoadMore = () => {
  if(loadMore){
      setPage(page + 1)
  }
}

  const backButtonHandler = useCallback(() => {
    navigation.goBack();
    return true;
  }, []);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, [backButtonHandler]);

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor='#fff' />
      <Header
        color='#fff'
        title={local.packageHistory}
        backHandler={()=> navigation.goBack()}/>
      
      <View style={styles.content}>
      
      {!loading && <>
        {!!error ? <View style={{marginTop : hp(-10)}}>
            <NoPackage
              text={error}
              pkgError={false}
              reloadHandler={reloadData} />
        </View> : <FlatList
          alwaysBounceVertical={false}
          showsVerticalScrollIndicator={false}
          data={packageList} 
          keyExtractor={(item, index) => index.toString()}

          onEndReached={handleLoadMore}
          onEndReachedThreshold={1}
          ListFooterComponent={moreLoading && <Loading />}

          renderItem={({item, index})=>{
            const formattedDate = moment(item.start_date, "YYYY-MM-DD HH:mm:ss").format("D MMM YYYY") + ' ~ ' + moment(item.end_date, "YYYY-MM-DD HH:mm:ss").format("D MMM YYYY");
            return <View style={styles.item}>
              <View style={styles.row}>
                <Text style={styles.name}>{item.package_name}</Text>
                <View style={styles.statusRow}>
                  <View style={[styles.statusBtn, {
                    backgroundColor : 
                      item.status === "active" ? Colors.light_green : 
                      item.status === "pending" ? "#e8c741" : "#c7c7c3"
                  }]}>
                    <Text style={[styles.status, {color: 
                      item.status === "active" || item.status === "pending" ? '#fff' : "#454542"
                      }]}>{item.status}</Text>
                  </View>
                </View>
              </View>
              <View style={styles.middle}>
                
                <Text style={styles.date}>{formattedDate}</Text>
              </View>
              

            </View>
          }}/>
        }
        </>}

      </View>
        


      <Loading visible={loading} />

      </View>
  )
};

export default memo(PackageHistory);
