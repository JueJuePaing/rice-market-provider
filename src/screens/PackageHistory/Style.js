import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import Colors from "assets/colors";

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor : "#fff",
      },
      content: {
        marginVertical: hp(1)
      },
      item: {
        alignSelf: 'center',
        width: wp(90),
        paddingHorizontal: wp(2),
        paddingVertical: hp(1.7),
        // flexDirection: 'row',
        // justifyContent: 'space-between',
        // alignItems: 'center',
        backgroundColor: '#edede8',
        borderRadius: hp(1),
        marginBottom: hp(1.5)
      },
      indexContent: {
        width: wp(10),
        backgroundColor: '#f5f5ce',
        alignItems: 'center',
        justifyContent: 'center',
        height: wp(10),
        borderRadius: wp(10),

      },
      index: {
        fontSize: hp(1.8),
        color: '#000'
      },
      name: {
        fontSize: hp(1.8),
        color: '#000',
        fontWeight: '600'
      },
      date: {
        paddingLeft: wp(4),
        fontSize: hp(1.6),
        color: '#2e2e2d',
      },
      activeBtn: {
        width: wp(3),
        height: wp(3),
        borderRadius: wp(3),
        backgroundColor : Colors.light_green
      },
      row : {
        paddingLeft: wp(3),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingRight: wp(1)
      },
      statusRow : {
        flexDirection: 'row',
        alignItems: 'center'
      },
      statusBtn : {
        paddingHorizontal: wp(2),
        paddingVertical: hp(.7),
        borderRadius: hp(2)
      },
      status: {
        textTransform: 'capitalize',
        fontSize: hp(1.4)
      }
});

export default styles;