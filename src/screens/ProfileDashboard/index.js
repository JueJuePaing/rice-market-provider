import React, {
  useState, 
  memo, 
  useContext,
  useEffect,
  useCallback
} from "react";
import {
  View,
  Text,
  TouchableOpacity,
  ToastAndroid,
  StatusBar,
  BackHandler
} from "react-native";
import moment from "moment-timezone";
import Ionicons from "react-native-vector-icons/Ionicons";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

import {
  ProfileDashboardScreen as styles
} from "assets/style";
import { getDatesWithRange } from "utils/date-time";
import { Context } from "context/Provider";
import apiUrl from "utils/apiUrl";
import { fetchGetByToken, fetchPostByToken } from "utils/fetchData";
import Colors from "assets/colors";
import { useLocal } from "hook/useLocal";

import Loading from "components/Loading";
import Header from "components/Header/DetailHeader";
import RiceChart from "components/Charts/RiceChart";
import DateRangePicker from "components/DatePicker";
import SelectModal from "components/SelectModal";

const ProfileDashboardScreen = ({ navigation }) => {

  const {
    token
  } = useContext(Context);
  const local = useLocal();

  const [
    loading, 
    setLoading
  ] = useState(false);
  const [
    dateLabels,
    setDateLabels
  ] = useState([]);
  const [
    startDate,
    setStartDate
  ] = useState("");
  const [
    endDate,
    setEndDate
  ] = useState("");
  const [
    datePicker,
    setDatePicker
  ] = useState(false);
  const [
    riceModalOpened,
    setRiceModalOpened
  ] = useState(false);
  const [
    riceType,
    setRiceType
  ] = useState();
  const [
    lowChecked,
    setLowChecked
  ] = useState(true);
  const [
    mediumChecked,
    setMediumChecked
  ] = useState(true);
  const [
    highChecked,
    setHighChecked
  ] = useState(true);
  const [
    error,
    setError
  ] = useState('');
  const [
    pkgError,
    setPkgError
  ] = useState(false)
  const [
    riceTypes,
    setRiceTypes
  ] = useState();
  const [
    lowData,
    setLowData
  ] = useState()
  const [
    mediumData,
    setMediumData
  ] = useState()
  const [
    highData,
    setHighData
  ] = useState()

  useEffect(() => {
    if (token) {
      getRiceTypes();
    } else {
      setLoading(false)
    }
  }, [token]);

  const getRiceTypes = async () => {
    const response = await fetchGetByToken(
      apiUrl.getRiceTypes,
      token
    );

    if (response?.success) {
      setPkgError(false)
      if (response.data && response.data.length > 0) {
        setRiceTypes(response.data);
        setError('')
      } else {
        setError(local.noData)
      }
    } else {
      setError(response?.message ?? local.noData);
      if (response.status === 403) {
        setPkgError(true);
      } else {
        setPkgError(false)
      }
    }
    setLoading(false)
  }

  const backButtonHandler = useCallback(() => {
    // DeviceEventEmitter.emit('tabIndex', 5);
    navigation.goBack();
    return true;
  }, []);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, [backButtonHandler]);

  useEffect(() => {
    if (riceType && startDate && endDate) {
      getDetailData()
    }
  }, [riceType, startDate, endDate])

  const getDetailData = async () => {
    setLoading(true)
    const data = {
      start_date : startDate,
      end_date : endDate,
      rice_type_id : riceType.id
    };

    const response = await fetchPostByToken(
      apiUrl.priceDetailGraph,
      data,
      token
    );

    setLoading(false);
    if (response?.success) {
      if (response.data?.low) {
        setLowData(response.data.low)
      }
      if (response.data?.medium) {
        setMediumData(response.data.medium)
      }
      if (response.data?.high) {
        setHighData(response.data.high)
      }
    } else {
      
      ToastAndroid.show(response?.message ?? local.fail, ToastAndroid.SHORT)
    }
  }

  const onChoose = (rice) => {
    setRiceModalOpened(false);
    setRiceType(rice)
  }

  useEffect(() => {
    const startdate = new Date();
    startdate.setDate(startdate.getDate() - 6);

    const end = new Date();
    end.setDate(end.getDate());

    const year = startdate.getFullYear();
    const month = String(startdate.getMonth() + 1).padStart(2, "0");
    const day = String(startdate.getDate()).padStart(2, "0");
    const formattedStartdate = `${year}-${month}-${day}`;

    const endYear = end.getFullYear();
    const endMonth = String(end.getMonth() + 1).padStart(2, "0");
    const endDay = String(end.getDate()).padStart(2, "0");
    const formattedEnddate = `${endYear}-${endMonth}-${endDay}`;

    setStartDate(formattedStartdate);
    setEndDate(formattedEnddate);
  }, []);

  useEffect(() => {
    if (!!startDate && !!endDate) {
      const dateArr = getDatesWithRange(startDate, endDate);
      const labels = [];
      for (let i = 0; i < dateArr.length; i++) {
        // just for label
        const dateLabel = moment(dateArr[i])
          .format("DD/MM/YYYY");
        labels.push(dateLabel);
      }

      setDateLabels(labels);
    }
  }, [startDate, endDate]);

  const onSubmitDate = (start, end) => {
    setDatePicker(false);
    setStartDate(start);
    setEndDate(end);
  }

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor='#f2f2f2' />
      <Header
        title={local.dashboard}
        backHandler={()=> navigation.goBack()} />

        <View style={styles.row}>
          <TouchableOpacity 
            activeOpacity={0.8}
            onPress={()=> setRiceModalOpened(true)}
            style={styles.selectWrapper}>
              <Text style={[styles.dateRangeTxt, {paddingLeft: wp(3)}]}>
                {riceType ? riceType.name : local.chooseRiceType}
              </Text>
            <Ionicons
              name="caret-down-circle-outline"
              size={hp(2.3)}
              color={"#fff"} />
          </TouchableOpacity>
          <TouchableOpacity 
            activeOpacity={0.8}
            onPress={()=> setDatePicker(true)}
            style={styles.selectWrapper}>
              <Text style={styles.dateRangeTxt}>
                {startDate} ~ {endDate}
              </Text>
            <Ionicons
              name="caret-down-circle-outline"
              size={hp(2.3)}
              color={"#fff"} />
          </TouchableOpacity>
        </View>

        <View style={styles.legendRow}>
          <View style={styles.circleRow}>
            {/* <View style={styles.circle} /> */}
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.checkbox}
              onPress={()=> setLowChecked(prev => !prev)}>
              <Ionicons
                name={lowChecked ? 'md-checkbox' : 'md-checkbox-outline'}
                size={hp(2.5)}
                color={'#dbc672'} />
            </TouchableOpacity>
            <Text style={styles.label}>{local.low}</Text>
          </View>
          <View style={styles.circleRow}>
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.checkbox}
              onPress={()=> setMediumChecked(prev => !prev)}>
              <Ionicons
                name={mediumChecked ? 'md-checkbox' : 'md-checkbox-outline'}
                size={hp(2.5)}
                color={'#d9875b'} />
            </TouchableOpacity>
            <Text style={styles.label}>{local.medium}</Text>
          </View>
          <View style={styles.circleRow}>
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.checkbox}
              onPress={()=> setHighChecked(prev => !prev)}>
              <Ionicons
                name={highChecked ? 'md-checkbox' : 'md-checkbox-outline'}
                size={hp(2.5)}
                color={Colors.light_green} />
            </TouchableOpacity>
            <Text style={styles.label}>{local.high}</Text>
            </View>
        </View>

        {dateLabels?.length > 0 && (!loading || lowData) && <RiceChart 
          dateLabels={dateLabels}
          low={lowChecked}
          medium={mediumChecked}
          high={highChecked}
          lowData={lowData}
          mediumData={mediumData}
          highData={highData} /> }
  
        <DateRangePicker 
          visible={datePicker}
          onClose={()=> setDatePicker(false)}
          onSubmit={onSubmitDate} />

        {riceModalOpened && <SelectModal
            title={local.chooseRiceType}
            data={riceTypes}
            selectedId={riceType ? riceType.id : null}
            onClose={() => setRiceModalOpened(false)}
            onChoose={onChoose} />}
      
      <Loading visible={loading} />
    </View>)
};

export default memo(ProfileDashboardScreen);
