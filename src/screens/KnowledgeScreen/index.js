import React, { 
  useState,
  useEffect,
  useContext,
  useCallback
} from "react";
import {
  View, 
  FlatList,
  StatusBar,
  BackHandler,
  RefreshControl,
  ScrollView
} from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";

import {useLocal} from 'hook/useLocal';
import { NewsScreen as styles } from "assets/style";
import { Context } from "context/Provider";
import apiUrl from "utils/apiUrl";
import { fetchGetByToken } from "utils/fetchData";
import Colors from "assets/colors";

import Header from "components/Header/DetailHeader";
import NewsItem from "components/NewsItem";
import NoPackage from "components/NoPackage";
import Loading from "components/Loading";

const KnowledgeScreen = ({ navigation }) => {

  const {
    token
  } = useContext(Context);

  const local = useLocal();

  const [
    refreshing,
    setRefreshing
  ] = useState(false)
  const [
    loading,
    setLoading
  ] = useState(false);
  const [
    news,
    setNews
  ] = useState([]);
  const [
    error, 
    setError
  ] = useState("");

  const [page, setPage] = useState(1);
  const [loadMore, setLoadMore] = useState(false);
  const [moreLoading, setMoreLoading] = useState(false);
 
  const backButtonHandler = useCallback(() => {
    navigation.goBack();
    return true;
  }, []);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, [backButtonHandler]);


  const onRefresh = useCallback(() => {
    setRefreshing(true);

    const reloadData = async () => {
      setPage(1);
      setLoading(true);
      getKnowledges(1);
    };

    reloadData();
}, [token]);

  useEffect(() => {
    getKnowledges(page);
  }, [page, token]);

  const handleLoadMore = () => {
    if(loadMore){
        setPage(page + 1)
    }
  }


  const getKnowledges = async (pageNo) => {
    setMoreLoading(true)
    
    const response = await fetchGetByToken(
      `${apiUrl.knowledgeList}?page=${pageNo}`,
      token
    );

    if (response?.success) {
      if (response.data && response.data.length === 0  && pageNo === 1) {
        setError(local.noData)
      } else {
        setError("")

        const newList = pageNo > 1 ? news?.concat(response.data) : response.data;
        setNews(newList);

        if(response.nextPageUrl){
          setLoadMore(true);
        }else{
          setLoadMore(false);
        }
      }


    } else {
      setError(response?.message ?? local.noData);

    }
    setLoading(false)
    setRefreshing(false);
    setMoreLoading(false)
  }

  const itemHandler = (item) => {
    navigation.navigate('KnowledgeDetail', {item, knowledge_id : null});
  }

  const reloadData = async () => {
    setLoading(true);
    getKnowledges(1);
  }

  return <View>
    <StatusBar 
      backgroundColor={"#f2f2f2"} 
      barStyle="dark-content" />

      <ScrollView 
            refreshControl={
              <RefreshControl
                  refreshing={ refreshing }
                  colors={[ Colors.light_green ]}
                  onRefresh={ onRefresh } 
                  progressViewOffset={hp(4)} />
            }
            showsVerticalScrollIndicator={false}
            >

        <Header
            title={local.knowledges}
            color="#f2f2f2"
            noMarquee={true}
            noChat={true}
            backHandler={()=> navigation.goBack()} />
        
        {!loading && <>
          {!!error ? <NoPackage
            text={error}
            reloadHandler={reloadData} /> : 
            <FlatList
              style={ styles.list }
              onEndReached={handleLoadMore}
              onEndReachedThreshold={1}
              alwaysBounceVertical={false}
              showsVerticalScrollIndicator={false}
              data={news} 
              keyExtractor={(item) => item.id.toString()}
              renderItem={({item})=>{
                return <NewsItem
                  item={ item }
                  categoryName="Knowledge"
                  itemHandler={itemHandler} />;
              }}
              ListFooterComponent={moreLoading && <Loading visible={true} />} /> 
              }
        </>}
      </ScrollView>

      <Loading visible={loading} />
    </View>
};

export default KnowledgeScreen;
