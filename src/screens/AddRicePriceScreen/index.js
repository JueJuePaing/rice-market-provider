import React, {
  useState,
  useEffect,
  useCallback,
  useContext
} from "react";
import {
  ScrollView,
  View, 
  BackHandler,
  TouchableOpacity,
  TextInput,
  ToastAndroid,
  StatusBar,
  Text} from "react-native";
import { Button } from "react-native-paper";
import Ionicons from "react-native-vector-icons/Ionicons";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import moment from "moment-timezone";

import {useLocal} from '../../hook/useLocal';
import { Context } from "context/Provider";
import apiUrl from "utils/apiUrl";
import { fetchPostByToken } from "utils/fetchData";
import {
  EditRicePriceModal as styles
} from "assets/style";
import Colors from "assets/colors";

import Loading from "components/Loading";
import Header from "components/Header/DetailHeader";
import DateTimePicker from "components/DatePicker/DateTimePicker";

const AddRicePriceScreen = ({navigation, route}) => {

  const {riceTypes, item} = route.params;
  const local = useLocal();

  const {
    token
  } = useContext(Context);

  const [
    typeError,
    setTypeError
  ] = useState(false);
  const [
    lowError,
    setLowError
  ] = useState(false);
  const [
    mediumError,
    setMediumError
  ] = useState(false);
  const [
    highError,
    setHighError
  ] = useState(false);
  const [
    unitError,
    setUnitError
  ] = useState(false);
  const [
    lowPrice,
    setLowPrice
  ] = useState();
  const [
    mediumPrice,
    setMediumPrice
  ] = useState();
  const [
    highPrice,
    setHighPrice
  ] = useState();
  const [
    unit,
    setUnit
  ] = useState('');
  const [
    type,
    setType
  ] = useState();
  const [
    editingType,
    setEditingType
  ] = useState(false);
  const [
    loading,
    setLoading
  ] = useState(false);
  const [
    date,
    setDate
  ] = useState(new Date());
  const [
    showDate,
    setShowDate
  ] = useState(false)

  const backButtonHandler = useCallback(() => {
    navigation.goBack();
    return true;
  }, []);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, [backButtonHandler]);

  useEffect(()=> {
    if (item) {
      setLowPrice(item?.price_low+"")
      setMediumPrice(item?.price_medium+"")
      setHighPrice(item?.price_high+"")
      setUnit(item?.unit);
      const filterType = riceTypes.find(riceType => riceType.id === item.rice_type_id);
      setType(filterType)
      setDate(moment(item.date).toDate())
    }
  }, [item, riceTypes])

  const onLowPriceChange = (txt) => {
    setLowError(false);
    setLowPrice(txt);
  };

  const onMediumPriceChange = (txt) => {
    setMediumError(false);
    setMediumPrice(txt);
  };

  const onHighPriceChange = (txt) => {
    setHighError(false);
    setHighPrice(txt);
  };

  const onUnitChange = (txt) => {
    setUnitError(false);
    setUnit(txt);
  };

  const handleSubmit = async () => {
    let valid = true;
    if (!type) {
      valid = false;
      setTypeError(true);
    }
    if (!lowPrice) {
      valid = false;
      setLowError(true);
    }
    if (!mediumPrice) {
      valid = false;
      setMediumError(true);
    }
    if (!highPrice) {
      valid = false;
      setHighError(true);
    }
    if (!unit) {
      valid = false;
      setUnitError(true);
    }
    if (!valid) return;

    setLoading(true);
    let response;
 
    if (item) { // Edit 
      const data = {
        id : item.id,
        rice_type_id : type.id,
        price_low : lowPrice,
        price_medium : mediumPrice,
        price_high : highPrice,
        unit,
        date_time: localFormattedDate
      };
  
      response = await fetchPostByToken(
        apiUrl.updatePrice,
        data,
        token
      );
    } else { // Add new rice price
      const data = {
        rice_type_id : type.id,
        price_low : lowPrice,
        price_medium : mediumPrice,
        price_high : highPrice,
        unit,
        date_time: localFormattedDate
      };
  
      response = await fetchPostByToken(
        apiUrl.newRicePrice,
        data,
        token
      );
    }

    setLoading(false);
    if (response?.success) {
      ToastAndroid.show(response?.message ?? local.successful, ToastAndroid.SHORT)
      navigation.goBack();
    } else {
      ToastAndroid.show(response?.message ?? local.fail, ToastAndroid.SHORT)
    }
  }

  const typeHandler = (chosenType) => {
    setType(chosenType)
    setEditingType(false)
    setTypeError(false)
  }

  const localFormattedDate = moment(date).format("YYYY-MM-DD HH:mm:ss");

  return ( <ScrollView 
    showsVerticalScrollIndicator={false}
    style={styles.container}>
    <StatusBar backgroundColor='#f2f2f2' />
    <Header
      title={item ? local.editRicePrice : local.addRicePrice}
      backHandler={()=> navigation.goBack()}/>

          <View>
            <TouchableOpacity 
              activeOpacity={0.8}
              onPress={()=> setEditingType(!editingType)}
              style={styles.selectWrapper}>
                <Text style={{
                  fontSize: hp(1.8),
                  color: type ? "#000" : "darkgray"
                }}>
                  {type ? type.name : local.chooseRiceType }
                </Text>
              <Ionicons
                name={editingType ? "caret-up-circle" : "caret-down-circle"}
                size={hp(2.8)}
                color={Colors.light_green} />
            </TouchableOpacity>
            {!!typeError && <Text style={styles.errTxt}>
                {local.invalidRiceType}
            </Text>}
            {editingType && 
                <ScrollView 
                  showsVerticalScrollIndicator={false}
                  style={styles.filterList}>
                  {riceTypes && riceTypes.length > 0 ? <>
                    {riceTypes?.map((riceType, index) => {
                      return <TouchableOpacity
                        onPress={ () => typeHandler(riceType) }>
                        <Text style={[styles.riceName, {paddingBottom : index === riceTypes.length -1 ? hp(4) : hp(2.5)}]}>{riceType.name}</Text>
                      </TouchableOpacity>
                    })} 
                  </> : null}
                </ScrollView>}
          </View>
 

          <Text style={[styles.label, {color: '#000'}]}>
            {local.low}
          </Text>
          <TextInput
            placeholder={local.enterHere}
            value={lowPrice}
            onChangeText={onLowPriceChange}
            keyboardType="numeric"
            autoCapitalize="none"
            style={[styles.input, {borderColor: 'lightgray'}]}
            underlineColor="transparent" />
          {!!lowError && <Text style={styles.errTxt}>
            {local.invalidLow}
          </Text>}

          <Text style={[styles.label, {color: '#000'}]}>
            {local.medium}
          </Text>
          <TextInput
            placeholder={local.enterHere}
            value={mediumPrice}
            onChangeText={onMediumPriceChange}
            keyboardType="numeric"
            autoCapitalize="none"
            style={[styles.input, {borderColor: 'lightgray'}]}
            underlineColor="transparent" />
          {!!mediumError && <Text style={styles.errTxt}>
            {local.invalidMedium}
          </Text>}

          <Text style={[styles.label, {color: '#000'}]}>
            {local.high}
          </Text>
          <TextInput
            placeholder={local.enterHere}
            value={highPrice}
            onChangeText={onHighPriceChange}
            keyboardType="numeric"
            autoCapitalize="none"
            style={[styles.input, {borderColor: 'lightgray'}]}
            underlineColor="transparent" />
          {!!highError && <Text style={styles.errTxt}>
            {local.invalidHigh}
          </Text>}

          <Text style={[styles.label, {color: '#000'}]}>
            {local.unit}
          </Text>
          <TextInput
            placeholder={local.enterHere}
            value={unit}
            onChangeText={onUnitChange}
            keyboardType="default"
            autoCapitalize="none"
            style={[styles.input, styles.textAreaInput, {borderColor: 'lightgray'}]}
            underlineColor="transparent"
            multiline={true}
            numberOfLines={2}
            textAlignVertical="top" />
          {!!unitError && <Text style={styles.errTxt}>
                  {local.invalidUnit}
          </Text>}

          <View style={styles.dateRow}>
            <View style={styles.dateBlock}>
              <Text style={[styles.label, {color: '#000'}]}>
                {local.date}
              </Text>
              <TouchableOpacity
                activeOpacity={0.8}
                style={styles.dateBtn}
                onPress={ () => setShowDate(true) }>
                <Ionicons
                  name="calendar"
                  size={hp(2.6)}
                  color={Colors.light_green} />
                <Text style={styles.dateValue}>
                  {date ? localFormattedDate : local.chooseDate}
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <Button
            mode="contained"
            onPress={handleSubmit}
            style={styles.button}
            labelStyle={{fontSize: hp(1.8)}}>
            {local.submit}
          </Button>

      {showDate && <View style={styles.datePicker}>
        <DateTimePicker
          currentDate={date}
          onClose={()=> setShowDate(false)}
          onSubmit={(val) => {
            setShowDate(false)
            setDate(val)
          }} />
        </View>}
       
      <Loading visible={loading} />
      </ScrollView>
     
  );
};

export default AddRicePriceScreen;
