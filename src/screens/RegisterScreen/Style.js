
import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import Colors from "assets/colors";

const styles = StyleSheet.create({
    container : {
      flex : 1,
     //  alignItems : "center",
      // justifyContent : "center",
      paddingHorizontal : 20,
      backgroundColor : "#FFFEFF",
      // paddingTop: hp(5)
    },
    logoContainer : {
      marginBottom : 0,
      // flexDirection : 'row',
      alignItems: 'center'
    },
    logo : {
      width : wp(80),
      height: hp(30),
      resizeMode : 'cover'
    },
    appname : {
      fontSize: hp(2.3),
      fontWeight: 'bold',
      marginBottom : hp(7),
      marginTop: hp(1),
      color: '#000'
    },
    formContainer : {
      width : "100%"
    },
    uploadBtn: {
      alignSelf: 'center',
      marginBottom : hp(1),
      marginTop: hp(5)
    },
    profile: {
      width: hp(12),
      height: hp(12),
      borderRadius: hp(12),
      resizeMode : 'contain'
    },
    errTxt: {
      alignSelf: 'flex-start',
      color: 'red',
      fontSize: hp(1.6),
      marginTop: -4,
      marginBottom: 5
    },
    errTxt2: {
      alignSelf: 'center',
      color: 'red',
      fontSize: hp(1.6),
      marginTop: -4,
      marginBottom: 5
    },
    innerUpload: {
      width: hp(9.5),
      height: hp(9.5),
      borderRadius: hp(7),
      backgroundColor: 'lightgray',
      alignItems : "center",
      justifyContent: 'center',
    },
    inputContainer : {
      flexDirection : "row",
      alignItems : "center",
      marginVertical : 10,
      borderWidth : 1,
      borderColor : "#ddd",
      borderRadius : 5
    },
    inputWrapper : {
      height : 40,
      flex : 1,
      borderWidth : 0,
      borderRadius : 5,
      overflow : "hidden"
      // paddingBottom: 20,
    },
    selectLabel : {
      paddingHorizontal : 10,
      fontSize : hp(1.8),
      color: '#000'
    },
    input : {
      height : 40,
      marginTop : 0,
      backgroundColor : "#FFFEFF",
      flex : 1,
      paddingHorizontal : 10,
      fontSize : hp(1.8),
      color: '#000'
    },
    inputAreaIcon : {
      marginHorizontal : 10,
      color : "#4A4550",
      marginTop: 10
    },
    inputAreaWrapper : {
      height : 120,
      flex : 1,
      borderWidth : 0,
      borderRadius : 5,
      overflow : "hidden"
      // paddingBottom: 20,
    },
    inputArea : {
      height : 120,
      marginTop : 0,
      backgroundColor : "#FFFEFF",
      flex : 1,
      paddingHorizontal : 10,
      fontSize : hp(1.8),
      color: '#000'
    },
    inputIcon : {
      marginHorizontal : 10,
      color : "#4A4550"
    },
    checkboxContainer : {
      flexDirection : "row",
      alignItems : "center",
      justifyContent : "center",
      marginVertical : 10
    },
    checkbox : {
      backgroundColor : "transparent",
      borderColor : "transparent"
    },
    checkboxLabel : {
      marginLeft : 5,
      fontSize : 16
    },
    button : {
      // backgroundColor: '#DB3B26',
      marginTop : hp(2.5)
    },
    forgotPasswordBtn : {
      alignSelf : "flex-end",
      marginTop: -5
    },
    forgotPassword : {
      fontSize : hp(1.6),
      color : Colors.dark_yellow,
      fontWeight: 'bold'
    },
    errorTxt : {
      fontSize : 14
    },
    language : {
      position : 'absolute',
      top : hp(1),
      right : wp(4)
    },
    backBtn : {
      height : hp(4.3),
      width : hp(4.3),
      alignItems: 'center',
      justifyContent : 'center',
      borderWidth : 3,
      borderColor : 'rgba(171, 194, 118, 0.5)',
      borderRadius: hp(1.5),
      position : "absolute",
      top : hp(1),
      left : wp(4)
    },
    regRow: {
      flexDirection: 'row',
      alignItems: 'center',
      alignSelf: 'center',
      marginTop : hp(2),
      marginBottom: hp(2)
    },
    regLabel: {
      color: 'darkgray',
      fontSize: hp(1.7),
      marginRight: wp(2)
    },
    nrcRow: {
        flexDirection : "row",
        alignItems : "center",
        marginVertical : 10,
       justifyContent: "space-between"
    },
    nrcBtn: {
        borderWidth : 1,
        borderColor : "#ddd",
        borderRadius : 5,
        height: 40,
        alignItems: 'center',
        flexDirection : "row",
        alignItems : "center",
        justifyContent: 'space-around'
    },
    stateCodeBtn: {
        width : wp(13), 
    },
    townshipBtn: {
        width : wp(28)
    },
    citizenBtn: {
        width : wp(13)
    },
    nrcInputContainer : {
      alignItems : "center",
      justifyContent: 'center',
      borderWidth : 1,
      borderColor : "#ddd",
      borderRadius : 5
    },
    nrcInputWrapper : {
      height : 40,
      flex : 1,
      borderWidth : 0,
      borderRadius : 5,
      overflow : "hidden"
      // paddingBottom: 20,
    },
    nrcValue : {
      fontSize : hp(1.8),
      color: '#000',
      paddingLeft : 3
    },
    termsRow : {
      flexDirection: 'row',
      // alignItems: 'center',
      marginTop: hp(1.5)
    },
    check : {
      width: wp(5),
      height: wp(5),
      borderRadius : 5,
      borderWidth : 1,
      borderColor : '#87ad2b',
      alignItems: 'center',
      justifyContent: 'center',
      marginRight : wp(2)
    },
    agree2 : {
      color: '#000',
      fontSize: hp(1.8),
      marginLeft: wp(7)
    },
    termsRow2: {
      flexDirection: 'row',
      flexWrap: 'wrap', // Allow text to wrap to the next line
      alignItems: 'flex-start', // Align text to the left side
    },
    agree: {
      fontSize: hp(1.8),
      color: '#000',
    },
    termsBtn: {

    },
    terms: {
      color: '#87ad2b',
      fontSize: hp(1.75),
      fontWeight: '500'
    }
  });

export default styles;