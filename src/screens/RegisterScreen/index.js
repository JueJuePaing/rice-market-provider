import React, {
  useState, 
  memo, 
  useRef,
  useEffect,
  useContext
} from "react";
import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  Image,
  ToastAndroid,
  TextInput
} from "react-native";
import {
  Button,
  HelperText,
  DefaultTheme
} from "react-native-paper";
import Icon from "react-native-vector-icons/MaterialIcons";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import DeviceInfo from 'react-native-device-info';
import Pushy from 'pushy-react-native';
import { launchImageLibrary } from "react-native-image-picker";

import { setItem } from "utils/appStorage";
import apiUrl from "utils/apiUrl";
import { fetchGet, fetchPostImageUpload } from "utils/fetchData";
import {
  STATE_CODE,
  CITIZEN_CODE,
  TOWNSHIP_LIST
} from 'data/nrc';
import styles from "./Style";
import { useLocal } from "hook/useLocal";

import Loading from "components/Loading";
import Language from "components/Language";
import { Context } from "context/Provider";
import SelectModal from "components/SelectModal";

const RegisterScreen = ({ navigation }) => {

  const local = useLocal();

  const [
    phone,
    setPhone
  ] = useState("");
  const [
    profile,
    setProfile
  ] = useState();
  const [
    name,
    setName
  ] = useState("");
  const [
    password,
    setPassword
  ] = useState("");
  const [
    confirmPassword,
    setConfirmPassword
  ] = useState('');
  const [
    address,
    setAddress
  ] = useState('');

  const [
    loading, 
    setLoading
  ] = useState(false);
  const [
    error, 
    setError
  ] = useState("");
  const [
    showPassword, 
    setShowPassword
  ] = useState(false);
  const [
    showConfirmPassword,
    setShowConfirmPassword
  ] = useState(false);
  const [
    lanModalOpened,
    setLanModalOpened
  ] = useState(false);
  const [
    states,
    setStates
  ] = useState([]);
  const [
    townships,
    setTownships
  ] = useState([]);
  const [
    allTownships,
    setAllTownships
  ] = useState([]);
  const [
    userTypes,
    setUserTypes
  ] = useState([]);
  const [
    showModal,
    setShowModal
  ] = useState(false);
  const [
    modalData,
    setModalData
  ] = useState();
  const [
    modalTitle,
    setModalTitle
  ] = useState('');
  const [
    state,
    setState
  ] = useState();
  const [
    township,
    setTownship
  ] = useState();
  const [
    userType,
    setuserType
  ] = useState();
  const [
    nrcStateCode,
    setNrcStateCode
  ] = useState(STATE_CODE[0])
  const [
    nrcTownshipCode,
    setNrcTownshipCode
  ] = useState(TOWNSHIP_LIST[0].TownshipCode[0])
  const [
    nrcTownships,
    setNrcTownships
  ] = useState(TOWNSHIP_LIST[0].TownshipCode);
  const [
    nrcCitizenCode,
    setNrcCitizenCode
  ] = useState(CITIZEN_CODE[0])
  const [
    nrcNo,
    setNrcNo
  ] = useState('');
  const [
    phoneError,
    setPhoneError
  ] = useState('');
  const [
    nameError,
    setNameError
  ] = useState('');
  const [
    imgError,
    setImgError
  ] = useState('');
  const [
    pwError,
    setPwError
  ] = useState('');
  const [
    confirmPwError,
    setConfirmPwError
  ] = useState('');
  const [
    addressError,
    setAddressError
  ] = useState('');
  const [
    statesError,
    setStatesError
  ] = useState('');
  const [
    townshipError,
    setTownshipError
  ] = useState('');
  const [
    userTypeError,
    setUserTypeError
  ] = useState('');
  const [
    nrcNoError,
    setNrcNoError
  ] = useState('');
  const [
    termsChecked,
    setTermsChecked
  ] = useState(false);
  const [
    termsError,
    setTermsError
  ] = useState(false);

  const {
    lang,
    changeToken,
    changeLang
  } = useContext(Context);

  const _isMounted = useRef(false);

  const theme = {
    ...DefaultTheme,
    colors : {
      ...DefaultTheme.colors,
      primary : "#87ad2b"
    }
  };

  const device_id = DeviceInfo.getDeviceId();


  useEffect(() => {
    const getStatesAndDivision = async () => {
      const response = await fetchGet(apiUrl.statesAndTownships);
      if (response?.success) {
        setStates(response.states);
        setAllTownships(response.townships)
      }
    }
    getStatesAndDivision();
  }, []);

  useEffect(() => {
    const getUserTypes = async () => {
      const response = await fetchGet(apiUrl.userTypes);
      if (response?.success) {
        setUserTypes(response.data);
      }
    }
    getUserTypes();
  }, []);

  useEffect(() => {
    _isMounted.current = true;

    return function clean() {
      _isMounted.current = false;
    };
  }, []);

  const handlePhoneChange = (phno) => {
    setPhoneError("");
    setPhone(phno);
    
  };

  const handleNrcNoChange = (no) => {
    setNrcNoError("");
    setNrcNo(no);
    
  };

  const handleConfirmPasswordChange = (pw) => {
    setConfirmPwError("");
    setConfirmPassword(pw);
  };

  const handleAddressChange = (pw) => {
    setAddressError("");
    setAddress(pw);
  };

  const handleNameChange = (txt) => {
    setNameError("");
    setName(txt);
  };

  const handlePasswordChange = (pw) => {
    setPwError("");
    setPassword(pw);
  };

  const handleBack = () => {
    navigation.goBack();
  }

  const handleRegister = async () => {
    let valid = true;
    if (!name) {
      setNameError(true);
      valid = false;
    }
    if (!profile) {
      setImgError(local.invalidUploadImage);
      valid = false;
    }
    if (!phone) {
      setPhoneError(true);
      valid = false;
    }
    if (!address) {
      setAddressError(local.invalidAddress);
      valid = false
    }
    if (!password) {
      setPwError(local.invalidPassword);
      valid = false;
    }
    if (!confirmPassword) {
      setConfirmPwError(local.invalidConfirmPassword)
      valid = false;
    } else if (password !== confirmPassword) {
      setConfirmPwError(local.mismatchPassword);
      valid = false
    }
    if (!nrcNo) {
      setNrcNoError(local.enterNrcNo)
      valid = false;
    } else if (nrcNo.length < 6) {
      setNrcNoError(local.invalidNrcNo);
      valid = false;
    }
    if (!state) {
      setStatesError(true);
      valid = false;
    }
    if (!township) {
      setTownshipError(true);
      valid = false;
    }
    if (!userType) {
      setUserTypeError(true);
      valid = false;
    }
    if (!termsChecked) {
      setTermsError(true);
      valid = false;
    }

    if (!valid) return 

    Pushy.register()
    .then(async deviceToken => {
      setLoading(true);

      const id_card = nrcStateCode + '/' + nrcTownshipCode + '(' + nrcCitizenCode + ')' + nrcNo;

      const uploadData = new FormData();
      uploadData.append('name', name);
      uploadData.append('address', address);
      uploadData.append('state_id', state.id);
      uploadData.append('township_id', township.id);
      uploadData.append('user_type_id', userType.id);
      uploadData.append('phone_number', phone);
      uploadData.append('password', password);
      uploadData.append('device_id', device_id);
      uploadData.append('pushy_token', deviceToken);
      uploadData.append('id_card', id_card);
      uploadData.append('image', {
        type: profile.type,
        uri: profile.uri,
        name: profile.fileName,
        width: profile.width,
        height: profile.height
      });

      const response = await fetchPostImageUpload(apiUrl.register, uploadData);
  
      setLoading(false);

      console.log("reg response >> ", response);
  
      if (response?.success) {
        //success
        if (response?.token) {
          await setItem('@token', response.token);
          changeToken(response.token)
          ToastAndroid.show(response.message, ToastAndroid.SHORT);

          setProfile();
          setName();
          setPhone();
          setPassword();
          setConfirmPassword();
          setAddress();
          setState();
          setTownship();
          setuserType();
          setNrcNo('');

          navigation.navigate('OTP', {phone, regOtp : response.otp, send_otp : response.send_otp})
        } else {
          setError(response.message ?? local.somethingWrong)
        }
      } else {
        if (response?.errors?.password) {
          setPwError(response?.errors.password[0]);
        }
        if (response?.errors?.phone_number) {
          setPhoneError(response?.errors.phone_number[0]);
        }
        if (response?.errors?.image) {
          setImgError(response?.errors.image[0]);
        }
        ToastAndroid.show(response.message ? response.message : local.fail, ToastAndroid.SHORT);
      }
    })
    .catch(err => {
      // Handle registration errors
      console.error('Pushy Register Errors : ' + err);
    });

    
  };

  const languageHandler = () => {
    setLanModalOpened(true);
  };

  const chooseLan = (lan) => {
    changeLang(lan);
    setItem('@lang', lan);
    setLanModalOpened(false);
  }

  

  const handleUpload = async () => {
    launchImageLibrary({
      selectionLimit : 1,
      mediaType : "photo"
  }, response => {
      if (response?.didCancel !== true) {
        if (response && response.assets && response.assets.length > 0) {
          setImgError('');
          setProfile(response.assets[0]);
        } 
      }
    });
  }

  const handleStatesOpen = () => {
    setModalData(states);
    setShowModal(true);
    setModalTitle(local.chooseStates);
  }

  const handleTownshipsOpen = () => {
    setModalData(townships);
    setShowModal(true);
    setModalTitle(local.chooseTownship);
  }

  const handleUserTypeOpen = () => {
    setModalData(userTypes);
    setShowModal(true);
    setModalTitle(local.chooseUserType);
  }

  const handleNRCStateCodeOpen = () => {
    setModalData(STATE_CODE);
    setShowModal(true);
    setModalTitle(local.chooseNRCStateCode);
  }

  const handleNRCTownshipOpen = () => {
    setModalData(nrcTownships);
    setShowModal(true);
    setModalTitle(local.chooseNRCTownshipCode);
  }

  const handleNRCCitizenOpen = () => {
    setModalData(CITIZEN_CODE);
    setShowModal(true);
    setModalTitle(local.chooseNRCCitizenCode);
  }

  const onChooseModalData = (data) => {
    if (modalTitle === local.chooseTownship) {
      setTownship(data);
      setTownshipError(false);
    } else if (modalTitle === local.chooseUserType) {
      setuserType(data);
      setUserTypeError(false)
    } else if (modalTitle === local.chooseStates) {
      setState(data);
      setStatesError(false)
      setTownship();
      // get related townships 
      const filterTownships = allTownships.filter(township => township.state_id == data.id+"");
      setTownships(filterTownships)
    } else if (modalTitle === local.chooseNRCStateCode) {
      setNrcStateCode(data);
      const state_id = parseInt(data);
      const tsCodes = TOWNSHIP_LIST[state_id-1].TownshipCode;
      setNrcTownshipCode(tsCodes[0])
      setNrcTownships(tsCodes);
    }  else if (modalTitle === local.chooseNRCTownshipCode) {
      setNrcTownshipCode(data);
    } else if (modalTitle === local.chooseNRCCitizenCode) {
      setNrcCitizenCode(data);
    }
    setShowModal(false);
    setModalTitle('');
    setModalData();
  }

  return (
    <>
    <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
      <TouchableOpacity 
        style={styles.language}
        activeOpacity={0.8}
        onPress={languageHandler}>
        <SimpleLineIcons
          size={hp(3)}
          color="#000"
          name="globe"
          />
      </TouchableOpacity>


      <View style={styles.formContainer}>
        <TouchableOpacity
          style={styles.uploadBtn}
          onPress={handleUpload}>
            {
              profile ? <Image
                source={{ uri : profile.uri }}
                style={styles.profile} /> : 
              <Ionicons
                name='person-circle-outline'
                size={hp(13)}
                color='darkgray' />
            }
        </TouchableOpacity>
      </View>
      {!!imgError && <Text style={styles.errTxt2}>{ imgError }</Text>}

      <View style={styles.inputContainer}>
        <Icon name="person" size={hp(2.2)} style={styles.inputIcon} />
        <View style={styles.inputWrapper}>
          <TextInput
            placeholder={local.name}
            value={name}
            onChangeText={handleNameChange}
            autoCapitalize="none"
            // error={nameError}
            style={styles.input}
            placeholderTextColor='darkgray'
            underlineColor="transparent"/>
        </View>
      </View>
      {!!nameError && <Text style={styles.errTxt}>
        {local.invalidName}</Text>}


      <View style={styles.inputContainer}>
        <Icon name="phone" size={hp(2.2)} style={styles.inputIcon} />
        <View style={styles.inputWrapper}>
          <TextInput
            placeholder={local.phoneNumber}
            value={phone}
            onChangeText={handlePhoneChange}
            keyboardType="number-pad"
            autoCapitalize="none"
            style={styles.input}
            placeholderTextColor='darkgray'
            underlineColor="transparent"/>
        </View>
      </View>
      {!!phoneError && <Text style={styles.errTxt}>
        {local.invalidPhoneNumber}</Text>}

      <View style={styles.nrcRow}>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={handleNRCStateCodeOpen}
          style={[styles.nrcBtn, styles.stateCodeBtn]}>
          <Text style={styles.nrcValue}>{nrcStateCode}</Text>
          <Entypo
            name='chevron-small-down'
            size={18}
            color='#5c5c5a'
            style={styles.downBtn} />
        </TouchableOpacity>
        <Text style={styles.nrcValue}>/</Text>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={handleNRCTownshipOpen}
          style={[styles.nrcBtn, styles.townshipBtn]}>
          <Text style={styles.nrcValue}>{nrcTownshipCode}</Text>
          <Entypo
            name='chevron-small-down'
            size={18}
            color='#5c5c5a'
            style={styles.downBtn} />
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={handleNRCCitizenOpen}
          style={[styles.nrcBtn, styles.citizenBtn]}>
          <Text style={styles.nrcValue}>{nrcCitizenCode}</Text>
          <Entypo
            name='chevron-small-down'
            size={18}
            color='#5c5c5a'
            style={styles.downBtn} />
        </TouchableOpacity>

        <View style={styles.nrcInputContainer}>
          <View style={styles.nrcInputWrapper}>
            <TextInput
              placeholder={local.nrcNo}
              value={nrcNo}
              onChangeText={handleNrcNoChange}
              keyboardType="number-pad"
              autoCapitalize="none"
              style={styles.input}
              maxLength={6}
              placeholderTextColor='darkgray'
              underlineColor="transparent"/>
          </View>
        </View>
      </View>
      {!!nrcNoError && <Text style={styles.errTxt}>
          {nrcNoError}</Text>}

      <View style={styles.inputContainer}>
        <Icon name="lock" size={hp(2.2)} style={styles.inputIcon} />
        <View style={styles.inputWrapper}>
          <TextInput
            placeholder={local.password}
            value={password}
            onChangeText={handlePasswordChange}
            secureTextEntry={!showPassword}
            style={styles.input}
            placeholderTextColor='darkgray'
            underlineColor="transparent"/>
        </View>
        <Icon
          name={!showPassword ? "visibility-off" : "visibility"}
          size={hp(2.2)}
          onPress={() => setShowPassword(!showPassword)}
          style={styles.inputIcon}/>
      </View>
      {!!pwError && <Text style={styles.errTxt}>{ pwError }</Text>}

      <View style={styles.inputContainer}>
        <Icon name="lock" size={hp(2.2)} style={styles.inputIcon} />
        <View style={styles.inputWrapper}>
          <TextInput
            placeholder={local.confirmPassword}
            value={confirmPassword}
            onChangeText={handleConfirmPasswordChange}
            secureTextEntry={!showConfirmPassword}
            style={styles.input}
            placeholderTextColor='darkgray'
            underlineColor="transparent"/>
        </View>
        <Icon
          name={!showConfirmPassword ? "visibility-off" : "visibility"}
          size={hp(2.2)}
          onPress={() => setShowConfirmPassword(!showConfirmPassword)}
          style={styles.inputIcon}/>
      </View>
      {confirmPwError !== '' && <Text style={styles.errTxt}>{confirmPwError}</Text>}

      <View style={[styles.inputContainer, {alignItems: 'flex-start'}]}>
        {/* <Icon name="map" size={hp(2.2)} style={styles.inputAreaIcon} /> */}
        <View style={styles.inputAreaWrapper}>
          <TextInput
            placeholder={local.address}
            value={address}
            multiline={true}
            numberOfLines={3}
            placeholderTextColor='darkgray'
            onChangeText={handleAddressChange}
            style={styles.inputArea}
            textAlignVertical="top"
            underlineColor="transparent"/>
        </View>
      </View>
      {addressError !== '' && <Text style={styles.errTxt}>{addressError}</Text>}

      <TouchableOpacity 
        onPress={ handleStatesOpen }
        style={[styles.inputContainer, {paddingHorizontal : 10,}]}>
        <View style={[styles.inputWrapper, {justifyContent: 'center'}]}>
          <Text style={styles.selectLabel}>
            {state ? state.name : local.chooseStates}
          </Text>
        </View>
        <Ionicons

            name="caret-down-circle"
            color={"#5f5a63"}
            size={hp(2.4)}
          />
      </TouchableOpacity>
      {!!statesError && <Text style={styles.errTxt}>
        {local.invalidStates}
      </Text>}

      <TouchableOpacity 
        onPress={ handleTownshipsOpen }
        disabled={ !state }
        style={[styles.inputContainer, {paddingHorizontal : 10,}]}>
        <View style={[styles.inputWrapper, {justifyContent: 'center'}]}>
          <Text style={[styles.selectLabel, {color: state ? '#000' : 'gray'}]}>
            {township ? township.name : local.chooseTownship}
          </Text>
        </View>
        <Ionicons

            name="caret-down-circle"
            color={"#5f5a63"}
            size={hp(2.4)}
          />
      </TouchableOpacity>
      {!!townshipError && <Text style={styles.errTxt}>{local.invalidTownship}</Text>}


      <TouchableOpacity 
        onPress={ handleUserTypeOpen }
        style={[styles.inputContainer, {paddingHorizontal : 10,}]}>
        <View style={[styles.inputWrapper, {justifyContent: 'center'}]}>
          <Text style={[styles.selectLabel]}>
            {userType ? userType.name : local.chooseUserType}
          </Text>
        </View>
        <Ionicons
            name="caret-down-circle"
            color={"#5f5a63"}
            size={hp(2.4)}
          />
      </TouchableOpacity>
      {!!userTypeError && <Text style={styles.errTxt}>{local.invalidUserType}</Text>}

      {!!error && (
        <HelperText type="error" style={styles.errorTxt}>
          {error}
        </HelperText>
      )}

      <View style={styles.termsRow}>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={()=> {
            if (!termsChecked) {
              setTermsError(false)
            }
            setTermsChecked(!termsChecked)
          }}
          style={styles.check}>
          {termsChecked && <Feather
            name='check'
            size={wp(4)}
            color='#87ad2b' />}
        </TouchableOpacity>
        {lang === 'en' ? <>
          <Text style={styles.agree}>{local.agree}</Text>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.termsBtn}
            onPress={()=> navigation.navigate('TermsAndConditions')}>
            <Text style={styles.terms}>{local.termsAndConditions}</Text>
          </TouchableOpacity>
        </> : <View style={styles.termsRow2}>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.termsBtn}
            onPress={()=> navigation.navigate('TermsAndConditions')}>
            <Text style={styles.terms}>{local.termsAndConditions}</Text>
          </TouchableOpacity>
          <Text style={styles.agree}>{local.agree}</Text>

        </View>}
      </View>
      {lang === "mm" && <Text style={styles.agree2}>{local.agree2}</Text>}

      {termsError && <Text style={[styles.errTxt, {marginTop: hp(1)}]}>{local.termsError}</Text>}


      <Button
        mode="contained"
        theme={theme}
        onPress={handleRegister}
        style={styles.button}
        labelStyle={{fontSize: hp(1.8)}}
        disabled={loading}>
        {local.register}
      </Button>

      <View style={styles.regRow}>
        <Text style={styles.regLabel}>
          {local.haveAnAccount}
        </Text>
        <TouchableOpacity 
          style={styles.forgotPasswordBtn} 
          onPress={handleBack}>
          <Text style={styles.forgotPassword}>
            {local.login}
          </Text>
        </TouchableOpacity>
      </View>

      </ScrollView>

      <Loading visible={loading} />
      <Language 
        visible={lanModalOpened}
        lan={ lang }
        languageHandler={chooseLan}
        onClose={()=> setLanModalOpened(false)} />

      {showModal && <SelectModal
          title={ modalTitle }
          data={ modalData }
          selectedId={ modalTitle === local.chooseTownship ? township?.id : modalTitle === local.chooseUserType ? userType?.id : modalTitle === local.chooseStates ? state?.id : 
            modalTitle === local.chooseNRCStateCode ? nrcStateCode :
            modalTitle === local.chooseNRCTownshipCode ? nrcTownshipCode :
            modalTitle === local.chooseNRCCitizenCode ? nrcCitizenCode : null
          }
          onClose={() => {
            setShowModal(false)
            setModalTitle('');
            setModalData();
          }}
          onChoose={onChooseModalData} />}
      
      </>)
};

export default memo(RegisterScreen);