import React, {
  useState, 
  memo,
  useEffect,
  useCallback,
  useContext
} from "react";
import {
  View,
  StatusBar,
  TouchableOpacity,
  BackHandler,
  ToastAndroid,
  Alert
} from "react-native";
import {SwipeListView} from 'react-native-swipe-list-view';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import FeatherIcon from "react-native-vector-icons/Feather";

import {
  RiceTypeConfigurationScreen as styles
} from "assets/style";
import { Context } from "context/Provider";
import apiUrl from "utils/apiUrl";
import { fetchGetByToken, fetchPostByToken } from "utils/fetchData";
import Colors from "assets/colors";
import {useLocal} from 'hook/useLocal';

import Loading from "components/Loading";
import Header from "components/Header/DetailHeader";
import RiceType from "components/RiceType";
import AddRiceTypeModal from "components/AddRiceTypeModal";
import NoPackage from "components/NoPackage";

const RiceTypeConfigurationScreen = ({ navigation }) => {

  const {
    token,
    userInfo
  } = useContext(Context);

  const local = useLocal();
  const parsedInfo = userInfo ? JSON.parse(userInfo) : null;

  const [
    addModalOpened,
    setAddModalOpened
  ] = useState(false);
  const [
    loading, 
    setLoading
  ] = useState(true);
  const [
    riceTypes,
    setRiceTypes
  ] = useState([]);
  const [
    editingRiceType,
    setEditingRiceType
  ] = useState();
  const [
    error, 
    setError
  ] = useState("");
  const [
    pkgError,
    setPkgError
  ] = useState(false)

  const backButtonHandler = useCallback(() => {
    navigation.goBack();
    return true;
  }, []);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, [backButtonHandler]);

  useEffect(() => {
    if (token) {
      getRiceTypes();
    } else {
      setLoading(false)
    }
  }, [token]);

  const getRiceTypes = async () => {
    const response = await fetchGetByToken(
      apiUrl.getRiceTypes,
      token
    );

    if (response?.success) {
      setPkgError(false)
      if (response.data && response.data.length > 0) {
        setRiceTypes(response.data);
        setError('')
      } else {
        setError(local.noData)
      }
    } else {
      setError(response?.message ?? local.noData);
      if (response.status === 403) {
        setPkgError(true);
      } else {
        setPkgError(false)
      }
    }
    setLoading(false)
  }

  const swipeEditHandler = (item) => {
    setEditingRiceType(item);
    setAddModalOpened(true);
  }

  const _renderHiddenItem = ({item}) => {
    return (
      <View style={styles.hiddenRow}>
        <TouchableOpacity
          style={[styles.slideContainer, {marginRight: wp(2)}]}
          onPress={() => swipeEditHandler(item)}>
          <FeatherIcon 
            name='edit'
            size={hp(3)}
            color={Colors.light_green} 
            />
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.slideContainer]}
          onPress={() => swipeDeletHandler(item)}>
          <Icon 
            name='delete'
            size={hp(4)}
            color="#d14e30" 
            />
        </TouchableOpacity>
      </View>
    );
  };

  const swipeDeletHandler = (item) => {
    Alert.alert(
      local.confirmation,
      local.confirmDelete,
      [
        { text: local.cancel, onPress: console.log("do nothing"), style: 'cancel' },
        { text: local.confirm, onPress: () => deleteRiceType(item.id) },
      ],
      { cancelable: false } // Prevent dismissing the alert by tapping outside or pressing the back button
    );
  }

  const deleteRiceType = async (id) => {
    setLoading(true)
    const response = await fetchPostByToken(
      `${apiUrl.deleteRiceType}?id=${id}`,
      {},
      token
    );

    setLoading(false)
    if (response?.success) {
      setLoading(true)
      getRiceTypes();
    } else {
      if (response?.message) ToastAndroid.show(response.message, ToastAndroid.SHORT);
    }
  }

  const addHandler = () => {
    setAddModalOpened(true);
  }

  const onSubmitName = async (name) => { // description
    setAddModalOpened(false);

    setLoading(true)

    let response;

    if (editingRiceType) {
      response = await fetchPostByToken(
        apiUrl.editRiceType,
        {
          id: editingRiceType.id,
          name
        },
        token
      );
    } else {
      response = await fetchPostByToken(
        apiUrl.newRiceType,
        {
          name
        },
        token
      );
    }

    setEditingRiceType();
    if (response?.success) {
      setLoading(true)
      getRiceTypes();
    } else {
      setLoading(false)
      if (response?.message) ToastAndroid.show(response.message, ToastAndroid.SHORT);
    }
  }

  const handleBuy = () => {
    navigation.navigate('Package');
  }

  const reloadData = async () => {
    setLoading(true);
    getRiceTypes();
  }

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor='#f2f2f2' />
      <Header
        title={local.riceTypeConfiguration}
        add={!pkgError && (!parsedInfo || parsedInfo.user_type_id !== 3)}
        addHandler={addHandler} 
        backHandler={()=> navigation.goBack()} />
      
      {!loading && <>
        {!!error ? <NoPackage
          text={error}
          buyHandler={handleBuy}
          pkgError={pkgError}
          reloadHandler={reloadData} /> : 
          <SwipeListView
            disableRightSwipe
            disableLeftSwipe={!parsedInfo || parsedInfo.user_type_id === 3}
            data={riceTypes}
            keyExtractor={(item, index) => item.id+""}
            renderItem={({item, index})=>{
              return <RiceType item={ item } />;
            }}
            renderHiddenItem={_renderHiddenItem}
            rightOpenValue={-wp(22)}
            showsVerticalScrollIndicator={false}
            style={styles.list}
          />}
      </>}
      
      <Loading visible={loading} />

      {addModalOpened && <AddRiceTypeModal 
        editingRiceType={ editingRiceType }
        onClose={()=> {
          setAddModalOpened(false)
          setEditingRiceType()
        }}
        onSubmit={onSubmitName}
      />}

    </View>)
};

export default memo(RiceTypeConfigurationScreen);
