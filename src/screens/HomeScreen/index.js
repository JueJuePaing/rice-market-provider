import React, {
  memo,
  useContext,
  useEffect,
  useState,
  useCallback
} from "react";
import {
  View,
  StatusBar,
  TouchableOpacity,
  RefreshControl,
  BackHandler,
  Linking,
  ToastAndroid,
  Text,
  ScrollView,
  DeviceEventEmitter
} from "react-native";
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import { heightPercentageToDP as hp } from "react-native-responsive-screen";

import style from "./Style";
import { Context } from "context/Provider";
import apiUrl from "utils/apiUrl";
import { fetchGetByToken, fetchPostByToken } from "utils/fetchData";
import Colors from "assets/colors";
import {useLocal} from 'hook/useLocal';

import Loading from "components/Loading";
import Banner from "components/Banner";
import Header from "components/Header";
import HomeShopItem from "components/HomeShopItem";
import AdsSlider from "components/AdsSlider";
import NoPackage from "components/NoPackage";

const HomeScreen = ({ navigation }) => {

  const {
    token
  } = useContext(Context);

  const local = useLocal();
  const stackNavigation = useNavigation();

  // console.log("Token >> ", token);

  const [
    refreshing,
    setRefreshing
  ] = useState(false)
  const [
    loading, 
    setLoading
  ] = useState(true);
  const [
    banners,
    setBanners
  ] = useState([]);
  const [
    footerBanners,
    setFooterBanners
  ] = useState([]);
  const [
    warehouseList,
    setWarehouseList
  ] = useState([]);
  const [
    riceMillList,
    setRiceMillList
  ] = useState([]);
  const [
    pkgError,
    setPkgError
  ] = useState(false);
  const [
    error,
    setError
  ] = useState()

  useEffect(()=> {
    const eventEmitter = DeviceEventEmitter.addListener(
        'noti.new_exchange_station_price',
        val => {
          stackNavigation.navigate('ExchangeStationStack', {
            screen: 'CommodityDepot',
            params: {defaultDepotId : val}
          });
        },
      );
      return () => eventEmitter;
}, [])

useEffect(()=> {
  const eventEmitter = DeviceEventEmitter.addListener(
      'noti.new_system_notification',
      val => {
        console.log("noti.new_system_notification >> ", val)

        const parsed = JSON.parse(val);
        stackNavigation.navigate('SystemNotification', {item: parsed});
      },
    );
    return () => eventEmitter;
}, [])

useEffect(() => {
  const eventListener = DeviceEventEmitter.addListener(
    'noti.new_knowledge',
    val => {
      console.log("noti.new_knowledge >> ", val)

      const parsed = JSON.parse(val);

      stackNavigation.navigate('KnowledgeStack', {
        screen: 'KnowledgeDetail',
        params: {item: parsed, knowledge_id : null}
      });

    },
  );
  return () => {
    eventListener.remove();
  };
}, []);

  useEffect(() => {
    const eventListener = DeviceEventEmitter.addListener(
      'noti.new_message',
      data => {
        stackNavigation.navigate('ChatStack', {
          screen: 'ChatDetailScreen',
          params: {chat : data}
        });
      },
    );
    return () => {
      eventListener.remove();
    };
  }, []);

  useEffect(()=> {
    const eventEmitter = DeviceEventEmitter.addListener(
        'package_change',
        val => {
          console.log("package_change >> ", val);
          if (token) {
            setLoading(true);
            getData();
          }
        },
      );
      return () => eventEmitter;
  }, [token])

  useEffect(() => {
    if (token) {
      getData();
    } else {
      setLoading(false)
    }
  }, [token]);

  const backButtonHandler = useCallback(() => {
    BackHandler.exitApp();
    return true;
  }, []);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, [backButtonHandler]);

  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', backButtonHandler);

      return () => {
        BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
      };
    }, [backButtonHandler])
  );

  const onRefresh = useCallback(() => {
    setRefreshing(true);

    const reloadData = async () => {
      getData();
    };

    reloadData();
}, [token]);

  const getData = async () => {
    await getBannerAds();
    await getFooterBannerAds();
    await getWarehouseList();
    await getRiceMillList();
  }

  const getWarehouseList = async () => {

    const response = await fetchGetByToken(
      `${apiUrl.userList}?user_type_id=${1}&user_id_arr=[]`,
      token
    );

    if (response?.success) {
      setPkgError(false)
      if (response.data?.length > 0) {
        setWarehouseList(response.data)
      }
    } else {
      if (response?.status === 403) {
        setPkgError(true);
        setError(response?.message ?? local.noData);
      } else {
        setPkgError(false)
      }
    }
  }

  const getRiceMillList = async () => {

    const response = await fetchGetByToken(
      `${apiUrl.userList}?user_type_id=${2}&user_id_arr=[]`,
      token
    );

    if (response?.success) {
      setPkgError(false)
      if (response.data?.length > 0) {
        setRiceMillList(response.data)
      }
    } else {
      if (response?.status === 403) {
        setPkgError(true);
        setError(response?.message ?? local.noData);
      } else {
        setPkgError(false)
      }
    }
    setLoading(false)
    setRefreshing(false)
  }

  const getBannerAds = async () => {
    const response = await fetchGetByToken(
      apiUrl.homeBannerAds,
      token
    );
    if (response?.success) {
      setBanners(response.data);
    }
  }

  const getFooterBannerAds = async () => {
    const response = await fetchGetByToken(
      apiUrl.homeFooterAds,
      token
    );
    if (response?.success) {
      setFooterBanners(response.data);
    }
  }

  const chatHandler = () => {
    navigation.navigate('ChatStack')
  }

  const newsHandler = () => {
    navigation.navigate('KnowledgeStack')
  }

  const notiHandler = () => {
    navigation.navigate('NotificationScreen')
  }

  const viewAllWarehouseHandler = () => {
    stackNavigation.navigate('WarehouseStack', {
      screen : 'WarehouseSc'
    });
  }

  const viewAllRiceMillHandler = () => {
    stackNavigation.navigate('RiceMillStack', {
      screen : 'RiceMill'
    });
  }

  const bannerHandler = async (item) => {
    if (item?.type === "phone") {
      Linking.openURL(`tel:${item.value}`)
    } else if (item?.type === "website") {
      Linking.openURL(item.value)
    } else if (item?.type === "facebook") {
      Linking.openURL("https://www.facebook.com/profile.php?" + item.value);
    } else if (item?.type === "user") {
      setLoading(true);

      const data = {
        user_id: item.value
      };
  
      const response = await fetchPostByToken(apiUrl.getUserDetail, data, token);

      setLoading(false);
      if (response?.success && response.user) {
        itemHandler(response.user);
      } else {
        ToastAndroid.show(response.message ?? local.fail, ToastAndroid.SHORT)
      }
    }
  }

  const handleBuy = () => {
    navigation.navigate('Package');
  }

  const reloadData = async () => {
    setLoading(true)
    getData();
  }

  const itemHandler = (item) => {
   if (item.user_type_id === 1) { // Warehouse
    stackNavigation.navigate('WarehouseStack', {
      screen: 'RiceList',
      params: {item, fromHome : true}
    });
   } else { // Rice Mill
    stackNavigation.navigate('RiceMillStack', {
      screen: 'RiceList',
      params: {item, fromHome : true}
    });
   }
  }

  return <View style={ style.container }>
    <ScrollView 
      refreshControl={
        <RefreshControl
            refreshing={ refreshing }
            colors={[ Colors.light_green ]}
            onRefresh={ onRefresh } 
            progressViewOffset={hp(4)} />
      }
      showsVerticalScrollIndicator={false}
      >
        <Header 
          chatHandler={chatHandler}
          notiHandler={notiHandler}
          newsHandler={newsHandler} /> 
        <StatusBar backgroundColor='#f2f2f2' barStyle='dark-content' />
        
        {banners && banners.length > 0 && <Banner 
          home={true}
          data={banners}
          bannerHandler={bannerHandler} />}

        <View style={style.mainContent}>

          {
            !pkgError && <>
            <View style={style.titleRow}>
              <Text style={style.title}>
                {local.warehouses}
              </Text>
              <TouchableOpacity
                activeOpacity={0.8}
                style={style.viewAllBtn}
                onPress={() => viewAllWarehouseHandler()}>
                <Text style={style.viewAllTxt}>
                  {local.viewAll}
                </Text>
              </TouchableOpacity>
            </View>
            {warehouseList && warehouseList.length > 0 ? <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              {warehouseList.map((warehouse, index) => {
                return <HomeShopItem 
                  key={index} 
                  item={warehouse}
                  itemHandler={()=> itemHandler(warehouse)} />
              })}
            </ScrollView> : <View style={style.noDataContent}>
              <Text style={style.noData}>{local.noData}</Text>
            </View>}

            <View style={style.titleRow}>
              <Text style={style.title}>
                {local.riceMills}
              </Text>
              <TouchableOpacity
                activeOpacity={0.8}
                style={style.viewAllBtn}
                onPress={()=> viewAllRiceMillHandler()}>
                <Text style={style.viewAllTxt}>
                  {local.viewAll}
                </Text>
              </TouchableOpacity>
            </View>
            {riceMillList && riceMillList.length > 0 ? <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              {riceMillList.map((riceMill, index) => {
                return <HomeShopItem 
                  mill={true}
                  key={index} 
                  item={riceMill}
                  itemHandler={()=> itemHandler(riceMill)} />
              })}
            </ScrollView> : <View style={style.noDataContent}>
              <Text style={style.noData}>{local.noData}</Text>
            </View>}
          </>}
{/* 
          <View style={{
            width:300,
            height: hp(22),
            backgroundColor: 'red'
          }} /> */}

          {footerBanners && footerBanners.length > 0 && <AdsSlider
            data={footerBanners}
            dataHandler={bannerHandler} />}

          {pkgError && <NoPackage
              home={true}
              text={error}
              pkgError={pkgError}
              buyHandler={handleBuy}
              reloadHandler={reloadData} />}

        </View>
        <Loading visible={loading} />
      </ScrollView>
    </View>
};

export default memo(HomeScreen);
