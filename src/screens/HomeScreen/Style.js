import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor: '#f2f2f2',
        marginBottom: hp(2)
      },
      mainContent : {
        marginHorizontal: wp(4)
      },
      titleRow : {
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: hp(2),
        paddingBottom: hp(2),
        justifyContent: 'space-between'
      },
      title : {
        fontWeight: 'bold',
        fontSize: hp(2),
        color: '#000'
      },
      viewAllBtn: {
    
      },
      viewAllTxt: {
        fontSize: hp(1.6),
        color: 'darkgray'
      },
      noDataContent: {
        height: hp(15),
        alignItems: 'center',
        justifyContent: 'center',
        width: wp(94),
        backgroundColor: '#e8ebe8'
      },
      noData: {
        fontSize: hp(1.8),
        color: '#8b8f8b'
      }
});

export default styles;