import React, {
  useState, 
  memo,
  useEffect,
  useContext,
  useCallback
} from "react";
import {
  View,
  StatusBar,
  TouchableOpacity,
  Text,
  BackHandler,
  ToastAndroid
} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import {SwipeListView} from 'react-native-swipe-list-view';

import styles from "./Style";
import apiUrl from "utils/apiUrl";
import { fetchPostByToken } from "utils/fetchData";
import { Context } from "context/Provider";
import {useLocal} from '../../hook/useLocal';

import Loading from "components/Loading";
import NoPackage from "components/NoPackage";
import Header from "components/Header/DetailHeader";
import Rating from "components/Rating";

const ReviewsScreen = ({ navigation, route }) => {

  const {user} = route.params;
  const {token} = useContext(Context);
  const local = useLocal();

  const [
    loading, 
    setLoading
  ] = useState(true);
  const [
    itemHeights, 
    setItemHeights
  ] = useState([]);
  const [
    reviews,
    setReviews
  ] = useState();
  const [
    error,
    setError
  ] = useState("")
  const [
    pkgError,
    setPkgError
  ] = useState(false)

  useEffect(() => {
    if (token) {
      getReviewList()
    } else {
      setLoading(false)
    }
  }, [token])

  const getReviewList = async () => {
    const data = {
      user_id : user?.id
    };

    const response = await fetchPostByToken(
      apiUrl.getReviews,
      data,
      token
    );

    setLoading(false);
    if (response?.success) {
      setPkgError(false)
      if (response.data && response.data.length === 0) {
        setError(local.noData)
      } else {
        setReviews(response.data);
        setError("")
      }
      setLoading(false)
    } else {
      setError(response?.message ?? local.noData);
      setLoading(false)
      if (response?.status === 403) {
        setPkgError(true);
      } else {
        setPkgError(false)
      }
    }
  }

  const backButtonHandler = useCallback(() => {
    navigation.goBack();
    return true;
  }, []);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, [backButtonHandler]);
  
  const reviewHandler = (item) => {}
  
  const swipeDeletHandler = async (item) => {

    setLoading(true)
    const response = await fetchPostByToken(
      `${apiUrl.deleteReview}`,
      {id: item.id},
      token
    );
    setLoading(false)
    if (response?.success) {
      const filteredReviews = reviews.filter((review, index) => {
        if (review.id !== item.id) {
          return true; // Keep the item in the array
        } else {
          itemHeights.splice(index, 1); // Remove the item at the specified index
          setItemHeights(itemHeights);
        }
      });
      
      if (!filteredReviews || filteredReviews.length === 0) {
        setError("NO DATA")
      } else {
        setReviews(filteredReviews);
      }
    } else {
      if (response?.message) ToastAndroid.show(response.message, ToastAndroid.SHORT);
    }

   
  }

  const _renderItem = ({index, item}) => {

    const onLayout = (e) => {
      const newHeights = [...itemHeights];
      newHeights[index] = e.nativeEvent.layout.height;
      setItemHeights(newHeights);
    };
  
    return (
      <TouchableOpacity
        onLayout={onLayout}
        activeOpacity={0.98}
        style={styles.reviewItem}
        key={item.id}
        onPress={() => reviewHandler(item)}>
        <View style={[styles.row, {
          marginBottom: !!item.review ? hp(1.5) : hp(.5)
        }]}>
          <Text style={styles.name}>{item.reviewer}</Text>
          <Rating rating={item.rating ? parseInt(item.rating) : 0} size={"medium"} />
        </View>
        {!!item.message && <Text style={styles.review}>{item.message}</Text>}
        <Text style={styles.time}>{item.time}</Text>
      </TouchableOpacity>
    );
  };

  const _renderHiddenItem = ({index, item}) => {
    return (
      <TouchableOpacity
        style={[styles.slideContainer, {height: itemHeights[index]}]}
        onPress={() => swipeDeletHandler(item)}>
        <Icon 
          name='delete'
          size={hp(4)}
          color="#d14e30" 
          />
      </TouchableOpacity>
    );
  };

  const handleBuy = () => {
    navigation.navigate('Package');
  }

  const reloadData = async () => {
    setLoading(true)
    getReviewList();
  }

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor='#f2f2f2' />
      <Header
        title={local.reviews}
        backHandler={()=> navigation.goBack()} />

      {!loading && <>
        {!!error ? <NoPackage
          pkgError={pkgError}
          text={error}
          buyHandler={handleBuy}
          reloadHandler={reloadData} /> : <SwipeListView
            disableRightSwipe
            data={reviews}
            keyExtractor={(item, index) => item.id}
            renderItem={_renderItem}
            renderHiddenItem={_renderHiddenItem}
            rightOpenValue={-wp(14)}
            showsVerticalScrollIndicator={false}
            style={styles.listContainer}
          />
        }
          </>}

      <Loading visible={loading} />
    </View>)
};

export default memo(ReviewsScreen);
