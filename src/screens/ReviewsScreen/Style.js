import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2'
  },
  listContainer: {
    width: wp(94),
    alignSelf: 'center',
    marginBottom: hp(2)
  },
  reviewItem: {
    backgroundColor: '#fff',
    width: wp(94),
    paddingHorizontal: wp(4),
    paddingTop: hp(2),
    marginBottom: hp(1.5),
    borderRadius: hp(2)
  },
  row: {
    flexDirection : 'row',
    alignItems: 'center',
    marginBottom: hp(1.5),
    justifyContent: 'space-between'
  },
  name: {
    color: '#000',
    fontSize: hp(1.8),
    fontWeight: 'bold'
  },
  review: {
    color: 'gray',
    fontSize: hp(1.65),
  },
  time: {
    alignSelf: 'flex-end',
    color: '#a5a8a2',
    fontSize: hp(1.4),
    paddingBottom: hp(1)
  },
  slideContainer: {
    borderRadius: hp(2),
    justifyContent: 'center',
    alignItems: 'flex-end',
    borderColor: '#DDD',
    marginRight: wp(2)
  },
});

export default styles;